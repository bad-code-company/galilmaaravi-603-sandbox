#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <string>
#include <string_view>
#include <tuple>
#include <windows.h>

namespace sandbox {
	class window {
	public:
		explicit window(std::string_view window_name, const WNDPROC wndproc, const size_t width = 1280u, const size_t height = 720u);
		~window();

		[[nodiscard]] HWND hwnd() const;

		[[nodiscard]] bool minimized() const;

		[[nodiscard]] std::tuple<LONG, LONG> size() const;
		[[nodiscard]] std::tuple<LONG, LONG> pos() const;

		[[nodiscard]] std::tuple<LONG, LONG> client_size() const;
		[[nodiscard]] std::tuple<LONG, LONG> client_pos() const;

		void show() const;

		explicit operator bool() const;

	private:
		std::string window_name_;
		/// Windows properties
		WNDCLASSEXA wc_;
		HWND		hwnd_;
	};
} // namespace sandbox

#endif // WINDOW_HPP