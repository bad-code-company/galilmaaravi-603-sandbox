#include "app.hpp"
#include <cstring>
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imfilebrowser.h>
#include <imgui_notify.h>
#include "dynamic_scan.hpp"
#include "history.hpp"
#include "log.hpp"
#include "static_scan.hpp"

using namespace std::string_literals;
using namespace sandbox;

std::unique_ptr<window> app::wnd_;
std::unique_ptr<d3d11>	app::d3d11_;

void app::run() {
	log::info("App is starting...");

	// Create window and D3D11
	log::info("Creating window...");
	wnd_	  = std::make_unique<window>(app_name, app::window_proc, app_width, app_height);
	auto icon = LoadImage(nullptr, "sandbox.ico", IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_LOADFROMFILE);
	SendMessage(wnd_->hwnd(), WM_SETICON, ICON_SMALL, (LPARAM)icon);
	SendMessage(wnd_->hwnd(), WM_SETICON, ICON_BIG, (LPARAM)icon);

	log::info("Creating D3D Device...");
	d3d11_ = std::make_unique<d3d11>(wnd_->hwnd());
	wnd_->show();

	ImGui::CreateContext();

	auto &io	   = ImGui::GetIO();
	io.IniFilename = nullptr;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;	  // Enable Multi-Viewport / Platform Windows

	ImGui::StyleColorsDark();

	// Setup backends
	log::info("Setting up ImGui Backends...");
	ImGui_ImplWin32_Init(wnd_->hwnd());
	ImGui_ImplDX11_Init(d3d11_->device, d3d11_->device_context);

	while (true) {
		static auto close = false;
		MSG			msg;
		while (PeekMessage(&msg, nullptr, 0u, 0u, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
				close = true;
		}
		if (close)
			break;

		// Start the Dear ImGui frame
		ImGui_ImplDX11_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();

		gui::render();

		// Rendering
		ImGui::Render();
		const static ImVec4 clear_color(0.45f, 0.55f, 0.60f, 1.00f);
		const static float	clear_color_with_alpha[] = { clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w };
		d3d11_->device_context->OMSetRenderTargets(1u, &d3d11_->render_target_view, nullptr);
		d3d11_->device_context->ClearRenderTargetView(d3d11_->render_target_view, clear_color_with_alpha);
		ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

		// Update and Render additional Platform Windows
		if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
			ImGui::UpdatePlatformWindows();
			ImGui::RenderPlatformWindowsDefault();
		}

		d3d11_->swap_chain->Present(1, 0); // Present with vsync
	}

	log::info("Saving history...");
	history::instance().add();
	dynamic_scan::instance().clear();
	static_scan::instance().clear();
	log::info("Destroying ImGui...");
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
	log::info("Finishing app...");
}

void app::save_history() {
	history::instance().add();
	dynamic_scan::instance().clear();
	static_scan::instance().clear();
}

void app::gui::render() {
	if (ImGui::Begin(app_name, nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoTitleBar)) {
		if (!wnd_->minimized()) {
			// Set ImGui window size and pos as OS window
			auto [width, height] = wnd_->client_size();
			auto [x, y]			 = wnd_->client_pos();
			ImGui::SetWindowSize(app_name, { static_cast<float>(width), static_cast<float>(height) });
			ImGui::SetWindowPos(app_name, { static_cast<float>(x), static_cast<float>(y) });

			if (ImGui::BeginTabBar("Categories##1")) {
				if (ImGui::BeginTabItem("Scan##1")) {
					scan_tab();
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("Results##1")) {
					results_tab();
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("History##1")) {
					history_tab();
					ImGui::EndTabItem();
				}
				ImGui::EndTabBar();
			}
		}

		// Render toasts on top of everything, at the end of your code!
		// You should push style vars here
		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 5.f);													   // Round borders
		ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(43.f / 255.f, 43.f / 255.f, 43.f / 255.f, 100.f / 255.f)); // Background color
		ImGui::RenderNotifications();																			   // <-- Here we render all notifications
		ImGui::PopStyleVar(1);																					   // Don't forget to Pop()
		ImGui::PopStyleColor(1);

		ImGui::End();
	}
}

void app::gui::scan_tab() {
	static ImGui::FileBrowser file_browser;
	static bool				  exe_file_browser = false;

	const auto child_width	= ImGui::GetWindowWidth() - ImGui::GetStyle().ItemSpacing.x * 2.f;
	const auto child_height = (ImGui::GetWindowHeight() - ImGui::GetCursorPosY()) / 3.f - ImGui::GetStyle().ItemSpacing.y * 1.33f;

	if (ImGui::BeginChild("Guest OS Info#1", { child_width, child_height }, true, 0)) {
		// Buttons
		auto select_vmx_button_width = child_width / 1.5f - ImGui::GetStyle().ItemSpacing.x;
		if (ImGui::Button("Select Guest OS VMX File##1", { select_vmx_button_width, 0.f })) {
			exe_file_browser = false;
			file_browser.SetTypeFilters({ ".vmx" });
			file_browser.Open();
		}
		ImGui::SameLine();
		auto clear_button_width = child_width / 3.f - ImGui::GetStyle().ItemSpacing.x;
		if (ImGui::Button("Clear##1", { clear_button_width, 0.f })) {
			dynamic_scan::instance().os_info().vmx_path.clear();
			strcpy_s(dynamic_scan::instance().os_info().username, dynamic_scan::os_info::username_max_size, "");
			strcpy_s(dynamic_scan::instance().os_info().password, dynamic_scan::os_info::password_max_size, "");
		}

		if (!dynamic_scan::instance().os_info().vmx_path.empty()) {
			ImGui::Text("Selected VMX: %s.", dynamic_scan::instance().os_info().vmx_path.stem().string().c_str());
		}

		auto				  username_buf	= dynamic_scan::instance().os_info().username;
		static constexpr auto username_size = dynamic_scan::os_info::username_max_size;
		ImGui::InputText("Guest OS username##1", username_buf, username_size);

		auto				  password_buf	= dynamic_scan::instance().os_info().password;
		static constexpr auto password_size = dynamic_scan::os_info::password_max_size;
		ImGui::InputText("Guest OS password##1", password_buf, password_size);

		ImGui::EndChild();
	}

	if (ImGui::BeginChild("File operations##1", { child_width, child_height }, true, 0)) {
		// Buttons
		auto upload_button_width = child_width / 1.5f - ImGui::GetStyle().ItemSpacing.x;
		if (ImGui::Button("Select Executable File##1", { upload_button_width, 0.f })) {
			exe_file_browser = true;
			file_browser.SetTypeFilters({ ".exe" });
			file_browser.Open();
		}
		ImGui::SameLine();
		auto clear_button_width = child_width / 3.f - ImGui::GetStyle().ItemSpacing.x;
		if (ImGui::Button("Clear##2", { clear_button_width, 0.f })) {
			exe::instance().clear();
		}

		if (exe::instance()) {
			// File description
			ImGui::Text("File name: %s.", exe::instance().name().data());
			ImGui::Text("File size: %s.", exe::instance().size().data());
			// Time related
			ImGui::Text("File creation time: %s.", exe::instance().creation_time().data());
			ImGui::Text("File last access time: %s.", exe::instance().last_access_time().data());
			ImGui::Text("File last write time: %s.", exe::instance().last_write_time().data());
		} else {
			ImGui::Text("File is not selected.");
		}
		ImGui::EndChild();
	}

	if (ImGui::BeginChild("Scan type##1", { child_width, child_height }, true, 0)) {
		// Single basic_scan buttons (SSB) size
		auto ssb_width	= child_width / 2.f - ImGui::GetStyle().ItemSpacing.x * 1.5f;
		auto ssb_height = child_height / 3.f - ImGui::GetStyle().ItemSpacing.y * 2.45f;
		if (ImGui::Button("Static scan##1", { ssb_width, ssb_height })) {
			if (exe::instance()) {
				static_scan::instance().run();
			} else {
				ImGui::InsertNotification({ ImGuiToastType_Error, 3000, "File is not selected." });
			}
		}
		ImGui::SameLine();
		if (ImGui::Button("Dynamic scan##1", { ssb_width, ssb_height })) {
			if (exe::instance()) {
				dynamic_scan::instance().run();
			} else {
				ImGui::InsertNotification({ ImGuiToastType_Error, 3000, "File is not selected." });
			}
		}

		// Hybrid basic_scan button (HSB) size
		auto hsb_width	= child_width - ImGui::GetStyle().ItemSpacing.x * 2.f;
		auto hsb_height = child_height / 1.5f - ImGui::GetStyle().ItemSpacing.y * 2.45f;
		if (ImGui::Button("Hybrid scan##1", { hsb_width, hsb_height })) {
			if (exe::instance()) {
				dynamic_scan::instance().run();
				static_scan::instance().run();
			} else {
				ImGui::InsertNotification({ ImGuiToastType_Error, 3000, "File is not selected." });
			}
		}

		ImGui::EndChild();
	}

	// Display file browser
	file_browser.Display();
	if (file_browser.HasSelected()) {
		if (exe_file_browser) {
			auto selected = file_browser.GetSelected();
			if (selected != exe::instance().path()) {
				save_history();
				exe::instance().set_path(selected);
			}
		} else {
			dynamic_scan::instance().os_info().vmx_path = file_browser.GetSelected();
		}

		file_browser.ClearSelected();
	}

	auto is_scan_active = dynamic_scan::instance().status() == basic_scan::status::run || static_scan::instance().status() == basic_scan::status::run;
	if (is_scan_active) {
		ImGui::OpenPopup("Scanning...##1");
		if (ImGui::BeginPopupModal("Scanning...##1", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoSavedSettings)) {
			ImGui::Text("Now scanning is being performed");
			ImGui::EndPopup();
		}
	}
}

void app::gui::results_tab() {
	const auto child_width	= ImGui::GetWindowWidth() - ImGui::GetStyle().ItemSpacing.x * 2.f;
	const auto child_height = ImGui::GetWindowHeight() - ImGui::GetCursorPosY() - ImGui::GetStyle().ItemSpacing.y * 2.f;

	if (ImGui::BeginChild("Results##1", { child_width, child_height }, true, 0)) {
		const auto clear_button_width	  = child_width - ImGui::GetStyle().ItemSpacing.x * 2.f;
		const auto cursor_y_before_button = ImGui::GetCursorPosY();
		if (ImGui::Button("Clear results##1", { clear_button_width, 0.f })) {
			save_history();
		}
		const auto cursor_y_after_button = ImGui::GetCursorPosY();
		const auto button_height		 = cursor_y_after_button - cursor_y_before_button - ImGui::GetStyle().ItemSpacing.y * 2.f;

		const auto inner_child_width  = child_width - ImGui::GetStyle().ItemSpacing.x * 2.f;
		const auto inner_child_height = child_height / 2.f - button_height - ImGui::CalcTextSize("Dynamic Scan Results:").y - ImGui::CalcTextSize("Static Scan Results:").y - ImGui::GetStyle().ItemSpacing.y * 1.5f;

		ImGui::Text("Dynamic Scan Results:");
		if (ImGui::BeginChild("Dynamic Scan Results##1", { inner_child_width, inner_child_height }, true, 0)) {
			if (dynamic_scan::instance().status() == basic_scan::status::success) {
				ImGui::Text("%s", dynamic_scan::instance().results().c_str());
			} else {
				ImGui::Text("Missing");
			}
			ImGui::EndChild();
		}

		ImGui::NewLine();

		ImGui::Text("Static Scan Results:");
		if (ImGui::BeginChild("Static Scan Results##1", { inner_child_width, inner_child_height }, true, 0)) {
			if (static_scan::instance().status() == basic_scan::status::success) {
				ImGui::Text("%s", static_scan::instance().results().c_str());
			} else {
				ImGui::Text("Missing");
			}
			ImGui::EndChild();
		}

		ImGui::EndChild();
	}
}

void app::gui::history_tab() {
	const auto child_width	= ImGui::GetWindowWidth() - ImGui::GetStyle().ItemSpacing.x * 2.f;
	const auto child_height = ImGui::GetWindowHeight() - ImGui::GetCursorPosY() - ImGui::GetStyle().ItemSpacing.y * 2.f;

	if (ImGui::BeginChild("History##1", { child_width, child_height }, true, 0)) {
		const auto button_width = child_width - ImGui::GetStyle().ItemSpacing.x * 2.f;
		if (ImGui::Button("Clear history##1", { button_width, 0.f })) {
			history::instance().clear();
		}

		ImGui::Separator();

		auto hist = history::instance().get();
		for (auto i = 0u; !hist.empty(); ++i) {
			try {
				auto json = hist.front();

				auto file_path			   = json["file_path"].get<std::string>();
				auto dynamic_scanned	   = json["dynamic_scanned"].get<bool>();
				auto dynamic_scan_result   = json["dynamic_scan_result"].get<std::string>();
				auto dynamic_scan_log_path = json["dynamic_scan_log_path"].get<std::filesystem::path>();
				auto static_scanned		   = json["static_scanned"].get<bool>();
				auto static_scan_result	   = json["static_scan_result"].get<std::string>();

				static constexpr auto results_intend = 25.f;

				ImGui::Text("File path: %s", file_path.c_str());
				ImGui::Text("Dynamic scan %s performed", dynamic_scanned ? "was" : "wasn't");
				if (dynamic_scanned) {
					auto cursor_x = ImGui::GetCursorPosX();

					ImGui::Text("Results:");
					ImGui::SetCursorPosX(cursor_x + results_intend);
					ImGui::Text("%s", dynamic_scan_result.c_str());

					ImGui::Text("Log path:");
					ImGui::SetCursorPosX(cursor_x + results_intend);
					ImGui::Text("%s", dynamic_scan_log_path.string().c_str());

					ImGui::SetCursorPosX(cursor_x);
				}
				ImGui::NewLine();
				ImGui::Text("Static scan %s performed", static_scanned ? "was" : "wasn't");
				if (static_scanned) {
					auto cursor_x = ImGui::GetCursorPosX();

					ImGui::Text("Results:");
					ImGui::SetCursorPosX(cursor_x + results_intend);
					ImGui::Text("%s", static_scan_result.c_str());

					ImGui::SetCursorPosX(cursor_x);
				}

				if (ImGui::Button(("Remove entry##" + std::to_string(i)).c_str(), { button_width, 0.f })) {
					history::instance().remove(std::move(json));
				}

				ImGui::Separator();
				hist.pop();
			} catch (const std::exception &e) {
				log::error("{}", e.what());
			}
		}

		ImGui::EndChild();
	}
}

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT WINAPI app::window_proc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	if (ImGui_ImplWin32_WndProcHandler(hwnd, msg, wParam, lParam))
		return true;

	switch (msg) {
	case WM_SIZE:
		if (d3d11_->device != nullptr && wParam != SIZE_MINIMIZED) {
			d3d11_->cleanup_render_target();
			d3d11_->swap_chain->ResizeBuffers(0, LOWORD(lParam), HIWORD(lParam), DXGI_FORMAT_UNKNOWN, 0);
			d3d11_->create_render_target();
		}
		return 0;
	case WM_SYSCOMMAND:
		// Disable ALT application menu
		if ((wParam & 0xfff0) == SC_KEYMENU)
			return 0;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	case WM_DPICHANGED:
		if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_DpiEnableScaleViewports) {
			auto suggested_rect = reinterpret_cast<RECT *>(lParam);
			SetWindowPos(hwnd, nullptr, suggested_rect->left, suggested_rect->top, suggested_rect->right - suggested_rect->left, suggested_rect->bottom - suggested_rect->top, SWP_NOZORDER | SWP_NOACTIVATE);
		}
		break;
	default:
		break;
	}
	return DefWindowProc(hwnd, msg, wParam, lParam);
}