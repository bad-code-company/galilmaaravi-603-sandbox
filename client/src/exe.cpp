#include "exe.hpp"
#include <sstream>

using namespace sandbox;

exe &exe::instance() {
	static exe inst;
	return inst;
}

void exe::set_path(const std::filesystem::path &path) {
	if (path.empty())
		return clear();

	path_ = path;

	if (!path.has_extension() || path.extension() != ".exe")
		throw std::runtime_error("Failed to match exe path.");
	name_ = path.filename().string();

	// Calculate size
	static constexpr auto		 size_suffixes_len				  = 5u;
	static constexpr char const *size_suffixes[size_suffixes_len] = { "B", "KB", "MB", "GB", "TB" };

	auto size = std::filesystem::file_size(path_);
	int	 i	  = 0;
	for (; size / 1024 > 0 && i < size_suffixes_len; ++i, size /= 1024) {}
	size_ = std::to_string(size) + size_suffixes[i];

	// Time related
	WIN32_FILE_ATTRIBUTE_DATA file_attrib;
	if (GetFileAttributesExA(path.string().c_str(), GetFileExInfoStandard, &file_attrib)) {
		creation_time_	  = filetime_to_string(file_attrib.ftCreationTime);
		last_access_time_ = filetime_to_string(file_attrib.ftLastAccessTime);
		last_write_time_  = filetime_to_string(file_attrib.ftLastWriteTime);
	}
}

void exe::clear() {
	path_.clear();
	name_.clear();
	size_.clear();
	creation_time_.clear();
	last_access_time_.clear();
	last_write_time_.clear();
}

std::filesystem::path exe::path() const {
	return path_;
}

std::string_view exe::name() const {
	return name_;
}

std::string_view exe::size() const {
	return size_;
}

std::string_view exe::creation_time() const {
	return creation_time_;
}

std::string_view exe::last_access_time() const {
	return last_access_time_;
}

std::string_view exe::last_write_time() const {
	return last_write_time_;
}

exe::operator bool() const {
	return !path_.empty();
}

std::string exe::filetime_to_string(const FILETIME &filetime) {
	SYSTEMTIME systime;
	FileTimeToSystemTime(std::addressof(filetime), std::addressof(systime));

	std::ostringstream date;
	date << std::setfill('0');
	date << std::setw(2) << systime.wDay << "/" << std::setw(2) << systime.wMonth << "/" << std::setw(4) << systime.wYear;
	date << " ";
	date << std::setw(2) << systime.wHour << ":" << std::setw(2) << systime.wMinute << ":" << std::setw(2) << systime.wSecond;

	return date.str();
}