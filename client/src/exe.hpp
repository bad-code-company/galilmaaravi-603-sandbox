#ifndef EXE_HPP
#define EXE_HPP

#include <filesystem>
#include <string>
#include <windows.h>

namespace sandbox {
	class exe {
		explicit exe() = default;

	public:
		exe(const exe &)			= delete;
		exe &operator=(const exe &) = delete;

		static exe &instance();

		[[nodiscard]] std::filesystem::path path() const;
		void								set_path(const std::filesystem::path &path);
		void								clear();

		[[nodiscard]] std::string_view name() const;
		[[nodiscard]] std::string_view size() const;

		[[nodiscard]] std::string_view creation_time() const;
		[[nodiscard]] std::string_view last_access_time() const;
		[[nodiscard]] std::string_view last_write_time() const;

		explicit operator bool() const;

	private:
		std::filesystem::path path_;

		std::string name_;
		std::string size_;
		/// Time related
		std::string creation_time_;
		std::string last_access_time_;
		std::string last_write_time_;

		/**
		 * @brief Converts File Time struct to formatted time (DD/MM/YY HH:MM:SS)
		 * @param filetime File Time struct
		 * @return Formatted time (DD/MM/YY HH:MM:SS)
		 */
		static std::string filetime_to_string(const FILETIME &filetime);
	};
} // namespace sandbox

#endif // EXE_HPP
