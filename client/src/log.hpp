#ifndef LOG_HPP
#define LOG_HPP

/// Thanks for SR_team for this logger code
/// Credits: https://gitlab.com/prime-hack/samp/plugins/BulletTrace/-/blob/master/BulletTrace/Log.cpp

#include <ostream>
#include <iostream>
#include <string_view>
#include <fmt/core.h>

namespace sandbox {
	namespace log {
		namespace details {
			static constexpr auto file_name = "sandbox.log";

			/**
			 * @brief Opens a log file and returns its stream
			 * @return Log file stream
			 */
			std::ostream &stream();

			/**
			 * @brief Adds info message to log
			 * @tparam Fmt Format type
			 * @tparam Args Args type
			 * @param prefix Prefix of the log line
			 * @param fmt Format value
			 * @param args Args value
			 */
			template<typename Fmt, typename... Args>
			inline void print(std::string_view prefix, const Fmt &fmt, Args &&...args) {
				stream() << prefix << fmt::format(fmt::runtime(fmt), std::forward<Args>(args)...) << std::endl;
#ifdef PRINT_CONSOLE_LOG
				std::cout << prefix << fmt::format(fmt::runtime(fmt), std::forward<Args>(args)...) << std::endl;
#endif // PRINT_CONSOLE_LOG
			}
		} // namespace details

		/**
		 * @brief Adds info message to log
		 * @tparam Fmt Format type
		 * @tparam Args Args type
		 * @param fmt Format value
		 * @param args Args value
		 */
		template<typename Fmt, typename... Args>
		void info(const Fmt &fmt, Args &&...args) {
			details::print("[INFO] ", fmt, std::forward<Args>(args)...);
		}

		/**
		 * @brief Adds error message to log
		 * @tparam Fmt Format type
		 * @tparam Args Args type
		 * @param fmt Format value
		 * @param args Args value
		 */
		template<typename Fmt, typename... Args>
		void error(const Fmt &fmt, Args &&...args) {
			details::print("[ERROR] ", fmt, std::forward<Args>(args)...);
		}
	}; // namespace log
} // namespace sandbox

#endif // LOG_HPP
