#ifndef APP_H
#define APP_H

#include <filesystem>
#include <memory>
#include <windows.h>
#include "d3d11.hpp"
#include "exe.hpp"
#include "window.hpp"

namespace sandbox {
	class app {
		static constexpr auto app_name	 = "Sandbox App";
		static constexpr auto app_width	 = 425u;
		static constexpr auto app_height = 450u;

	public:
		static void run();

	private:
		static std::unique_ptr<window> wnd_;
		static std::unique_ptr<d3d11>  d3d11_;

		static inline void save_history();

		/// The code of GUI itself
		class gui {
		public:
			static inline void render();

		private:
			static inline void scan_tab();
			static inline void results_tab();
			static inline void history_tab();
		};

		/// Processes window messages
		static LRESULT WINAPI window_proc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	};
} // namespace sandbox

#endif // APP_H