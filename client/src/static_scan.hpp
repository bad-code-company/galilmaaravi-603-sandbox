#ifndef STATIC_SCAN_HPP
#define STATIC_SCAN_HPP

#include "basic_scan.hpp"

namespace sandbox {
	class static_scan : public basic_scan {
		/// Private constructor for singleton
		explicit static_scan() = default;

	public:
		/// Delete copy constructor and copy assign operator
		static_scan(const static_scan &)			= delete;
		static_scan &operator=(const static_scan &) = delete;

		/**
		 * @return Instance of the scanning class
		 */
		static static_scan &instance();

		/**
		 * @brief Runs the scan and saves the results to the class
		 */
		void run() override;
		/**
		 * @return Scanning results
		 */
		[[nodiscard]] std::string results() const override;

	private:
		bool yara_success_ = true;
		bool vt_success_   = true;

		bool  yara_detected_ = false;
		float vt_percentage_ = 0.f; /// VirusTotal
	};
} // namespace sandbox

#endif // STATIC_SCAN_HPP
