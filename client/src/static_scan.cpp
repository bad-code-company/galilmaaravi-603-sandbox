#include "static_scan.hpp"
#include <iomanip>
#include <sstream>
#include <thread>
#include <yarapp/yarapp.hpp>
#include <virustotal/virustotal.hpp>
#include "exe.hpp"
#include "log.hpp"

using namespace sandbox;

static_scan &static_scan::instance() {
	static static_scan inst;
	return inst;
}

void static_scan::run() {
	std::thread([this]() -> void {
		log::info("Starting Static Analysis on file \"{}\"", exe::instance().name());
		status_ = status::run;

		yara_success_ = true;
		try {
			// YARA Rules detection
			log::info("Running YARA Rules scanner...");
			yarapp yara_scanner;
			yara_scanner.add_rules_dir(std::filesystem::current_path() / "static_scan" / "rules");
			yara_detected_ = yara_scanner.scan_file(exe::instance().path());
		} catch (const std::exception &e) {
			log::error("Static Scan - YARA Failed: {}", e.what());
			yara_success_ = false;
		}

		try {
			// VirusTotal detection
			log::info("Running VirusTotal scanner...");
			auto [positive, total] = virustotal::scan_file(exe::instance().path());
			vt_percentage_		   = static_cast<float>(positive) / static_cast<float>(total) * 100.f;
		} catch (const std::exception &e) {
			log::error("Static Scan - VT Failed: {}", e.what());
			vt_success_ = false;
		}

		status_ = yara_success_ || vt_success_ ? status::success : status::wait;
		log::info("Static Analysis ended.");
	}).detach();
}

std::string static_scan::results() const {
	if (status_ == status::success) {
		std::ostringstream oss;
		if (yara_success_)
			oss << "YARA Rules Detection: " << (yara_detected_ ? "DETECTED" : "UNDETECTED");
		if (yara_success_ && vt_success_)
			oss << std::endl;
		if (vt_success_)
			oss << "VirusTotal Detection: " << std::fixed << std::setprecision(2) << vt_percentage_ << "%";
		return oss.str();
	}
	return "";
}