#include "log.hpp"
#include <fstream>
#include <string>
#include <windows.h>

using namespace sandbox;

std::ostream &log::details::stream() {
	static std::ofstream out;
	if (out.is_open())
		return out;

	out.open(details::file_name, std::ios::out);

	if (!out.is_open()) {
		static bool warn_access = false;
		if (!warn_access) {
			warn_access = true;
			MessageBoxA(nullptr, "Can't write log file", file_name, 0);
		}
	}
	return out;
}