#include "window.hpp"
#include <stdexcept>

using namespace sandbox;

window::window(std::string_view window_name, const WNDPROC wndproc, const size_t width, const size_t height)
	: window_name_(window_name) {
	// set window class properties and register it
	wc_ = { sizeof(WNDCLASSEXA), CS_CLASSDC, wndproc, 0, 0, GetModuleHandleA(nullptr), nullptr, nullptr, nullptr, nullptr, window_name_.c_str(), nullptr };
	RegisterClassExA(&wc_);

	static constexpr auto x = 200;
	static constexpr auto y = 200;

	// create window
	hwnd_ = CreateWindowA(wc_.lpszClassName, window_name_.c_str(), WS_OVERLAPPEDWINDOW, x, y, width, height, nullptr, nullptr, wc_.hInstance, nullptr);
	if (!hwnd_) {
		throw std::runtime_error("Can't create window.");
	}
}

window::~window() {
	if (hwnd_)
		DestroyWindow(hwnd_);
	UnregisterClassA(wc_.lpszClassName, wc_.hInstance);
}

HWND window::hwnd() const {
	return hwnd_;
}

bool window::minimized() const {
	return IsIconic(hwnd_);
}

std::tuple<LONG, LONG> window::size() const {
	RECT rect;
	if (GetWindowRect(hwnd_, &rect)) {
		auto width	= rect.right - rect.left;
		auto height = rect.bottom - rect.top;
		return { width, height };
	}
	return std::make_tuple<LONG, LONG>(-1, -1);
}

std::tuple<LONG, LONG> window::pos() const {
	RECT rect;
	if (GetWindowRect(hwnd_, &rect)) {
		return { rect.left, rect.top };
	}
	return std::make_tuple<LONG, LONG>(-1, -1);
}

std::tuple<LONG, LONG> window::client_size() const {
	RECT rect;
	if (GetClientRect(hwnd_, &rect)) {
		auto width	= rect.right - rect.left;
		auto height = rect.bottom - rect.top;
		return { width, height };
	}
	return std::make_tuple<LONG, LONG>(-1, -1);
}

std::tuple<LONG, LONG> window::client_pos() const {
	RECT rect;
	if (GetClientRect(hwnd_, &rect)) {
		ClientToScreen(hwnd_, reinterpret_cast<LPPOINT>(&rect.left));
		ClientToScreen(hwnd_, reinterpret_cast<LPPOINT>(&rect.right));
		return { rect.left, rect.top };
	}
	return std::make_tuple<LONG, LONG>(-1, -1);
}

void window::show() const {
	ShowWindow(hwnd_, SW_SHOWDEFAULT);
	UpdateWindow(hwnd_);
}

window::operator bool() const {
	return hwnd_ != nullptr;
}