#include "app.hpp"
#include "log.hpp"

int APIENTRY WinMain(HINSTANCE, HINSTANCE, PSTR, int) {
	try {
		sandbox::app::run();
	} catch (const std::exception &e) {
		sandbox::log::error("{}", e.what());
		return 1;
	}
	return 0;
}
