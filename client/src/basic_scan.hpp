#ifndef SCAN_HPP
#define SCAN_HPP

#include <filesystem>
#include <string>

namespace sandbox {
	class basic_scan {
	public:
		enum class status { wait,
							run,
							success };

		/**
		 * @brief Runs the scan and saves the results to the class
		 */
		virtual void run() = 0;

		/**
		 * @return Current state
		 */
		[[nodiscard]] virtual status status() const {
			return status_;
		}

		/**
		 * @return Scanning results
		 */
		[[nodiscard]] virtual std::string results() const = 0;

		/**
		 * @brief Clears the results
		 */
		virtual void clear() {
			status_ = status::wait;
		}

	protected:
		enum class status status_ = status::wait;
	};
} // namespace sandbox

#endif // SCAN_HPP
