#ifndef D311_HPP
#define D311_HPP

#include <d3d11.h>

namespace sandbox {
	class d3d11 {
	public:
		/**
		 * @brief Initializes D3D11 stuff: creates d3d11 device and render target view
		 * @param hwnd Window handle
		 */
		explicit d3d11(const HWND hwnd);
		/**
		 * @brief Cleanups D3D11 stuff
		 */
		~d3d11();

		/// D3D11 stuff pointers
		ID3D11Device			 *device			   = nullptr;
		ID3D11DeviceContext	*device_context	   = nullptr;
		IDXGISwapChain		   *swap_chain		   = nullptr;
		ID3D11RenderTargetView *render_target_view = nullptr;

		void create_render_target();
		void cleanup_render_target();

	private:
		/// Window handle
		HWND hwnd_ = nullptr;

		void create_device();
		void cleanup_device();

		/**
		 * @brief Releases field and sets its value to nullptr
		 * @tparam T Field type
		 * @param field Reference to a field ptr
		 */
		template<typename T>
		static inline void release_field(T *&field);
	};
} // namespace sandbox

#endif // D311_HPP