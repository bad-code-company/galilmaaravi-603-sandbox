#ifndef HISTORY_HPP
#define HISTORY_HPP

#include <cstdint>
#include <filesystem>
#include <queue>
#include <nlohmann/json.hpp>

namespace sandbox {
	class history {
		/// Private constructor for singleton
		explicit history();

	public:
		/// Delete copy constructor and copy assign operator
		history(const history &)			= delete;
		history &operator=(const history &) = delete;

		/**
		 * @return Instance of the history class
		 */
		static history &instance();

		/**
		 * @brief Loads existing history from JSON file
		 */
		void load();

		/**
		 * @brief Adds current results to JSON file
		 */
		void add();
		/**
		 * @brief Removes entry from the history
		 * @param json Entry
		 */
		void remove(nlohmann::json &&json);
		/**
		 * @brief Clears history, removes all entries
		 */
		void clear();

		/**
		 * @return Scanning history
		 */
		[[nodiscard]] std::queue<nlohmann::json> get() const;

	private:
		static constexpr auto file_name = "history.json";

		std::deque<nlohmann::json> data_;

		/**
		 * @brief Saves current container data to history file
		 */
		void save() const;
	};
} // namespace sandbox

#endif // HISTORY_HPP
