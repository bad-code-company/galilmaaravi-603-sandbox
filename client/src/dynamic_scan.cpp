#include "dynamic_scan.hpp"
#include <chrono>
#include <ctime>
#include <sstream>
#include <thread>
#include <log_analyzer/log_analyzer.hpp>
#include <vm/vmware_vm.hpp>
#include "exe.hpp"
#include "log.hpp"

using namespace std::string_literals;
using namespace sandbox;

dynamic_scan &dynamic_scan::instance() {
	static dynamic_scan inst;
	return inst;
}

struct dynamic_scan::os_info &dynamic_scan::os_info() {
	return os_info_;
}

void dynamic_scan::run() {
	std::thread([this]() -> void {
		if (!os_info_)
			return;

		log::info("Starting Dynamic Analysis on file \"{}\"", exe::instance().name());
		status_ = status::run;

		vmware_vm vm(os_info_.vmx_path);
		VixHandle snapshot = VIX_INVALID_HANDLE;

		try {
			log::info("Running virtual machine...");
			vm.connect();
			vm.boot();

			// Save snapshot
			log::info("Saving snapshot...");
			snapshot = vm.create_snapshot("scan_start", true);

			log::info("Logging into guest OS...");
			vm.login(os_info_.username, os_info_.password, true);

			const auto guest_log_file = std::filesystem::path(R"(C:\sandbox.log)");
			const auto guest_dir	  = std::filesystem::path(R"(C:\sandbox)");

			// Delete old log file and old directory
			log::info("Before scan deleting old log file and old directory...");
			vm.remove_file(guest_log_file);
			vm.remove_directory(guest_dir);

			// Create sandbox dir
			log::info("Creating sandbox directories on guest OS...");
			const auto guest_scan_dir = guest_dir / "scan";
			vm.create_directory(guest_dir);
			vm.create_directory(guest_scan_dir);
			log::info("Transferring dynamic scan files...");
			const auto dynamic_scan_dir		= std::filesystem::current_path() / "dynamic_scan";
			const auto dynamic_scan_bin_dir = dynamic_scan_dir / "bin";
			vm.transfer_file_to_guest(dynamic_scan_bin_dir, R"(C:\sandbox)");
			log::info("Transferring scan file...");
			// Get file name and guest file path
			const auto guest_file_path = guest_scan_dir / exe::instance().name().data();
			// Transfer a file to check, and necessary stuff to guest
			vm.transfer_file_to_guest(exe::instance().path(), guest_file_path);

			// Run Shark (turn off Patch Guard)
			const auto start_shark_cmd = (guest_scan_dir / R"(Shark\Sea.exe)").string();
			log::info("Starting Shark (CMD: {})...", start_shark_cmd);
			vm.run_cmd(start_shark_cmd);

			// Run runner
			const auto start_runner_cmd = (guest_dir / R"(sandbox_runner.exe )").string() + guest_file_path.string();
			log::info("Starting runner (CMD: {})...", start_runner_cmd);
			vm.run_cmd(start_runner_cmd);

			// After runner is closed, copy the log file
			const auto log_file_name = exe::instance().name().data() + "_"s + current_date() + ".log";
			log::info("Transferring log file (filename: {}) of the program to the host OS...", log_file_name);
			const auto logs_folder = std::filesystem::current_path() / "logs";
			log_path_			   = logs_folder / log_file_name;
			std::filesystem::create_directories(logs_folder);
			const auto host_log_file = logs_folder / log_file_name;
			vm.transfer_file_to_host(guest_log_file, host_log_file);

			// Delete old log file and old directory
			log::info("After scan deleting old log file and old directory...");
			vm.remove_file(guest_log_file);
			vm.remove_directory(guest_dir);

			log::info("Logging out from guest OS...");
			vm.logout();

			// Restore snapshot
			log::info("Restoring and removing snapshot...");
			vm.revert_to_snapshot(snapshot);
			vm.remove_snapshot(snapshot);

			log::info("Shutting down guest OS...");
			vm.shutdown();
			vm.disconnect();

			log::info("Running log analyzer on log file...");
			try {
				log_analyzer la(dynamic_scan_dir / "lua", host_log_file);
				la.run();
				results_ = la.results();
			} catch (const std::exception &e) {
				results_ = "Dynamic scan has completed, but log analyzer failed.\n"
						   "Check log file manually.";
			}

			status_ = status::success;
		} catch (const std::exception &e) {
			log::error("Dynamic Scan Failed: {}", e.what());

			vm.logout();
			if (snapshot != VIX_INVALID_HANDLE) {
				vm.revert_to_snapshot(snapshot);
				vm.remove_snapshot(snapshot);
			}
			vm.shutdown();
			vm.disconnect();

			status_ = status::wait;
			return;
		}

		status_ = status::success;
		log::info("Dynamic Analysis ended.");
	}).detach();
}

std::string dynamic_scan::results() const {
	if (status_ == status::success) {
		return results_;
	}
	return "";
}

std::filesystem::path dynamic_scan::log_path() const {
	return status_ == status::success ? log_path_ : "";
}

std::string dynamic_scan::current_date() {
	auto now	  = std::chrono::system_clock::now();
	auto now_time = std::chrono::system_clock::to_time_t(now);

	std::ostringstream oss;
	oss << std::put_time(std::localtime(&now_time), "%Y-%m-%d_%H-%M-%S");
	return oss.str();
}