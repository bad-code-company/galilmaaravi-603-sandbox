#include <d3d11.hpp>
#include <cstring>
#include <stdexcept>
#include <iostream>

using namespace sandbox;

d3d11::d3d11(const HWND hwnd)
	: hwnd_(hwnd) {
	create_device();
	create_render_target();
}

d3d11::~d3d11() {
	cleanup_render_target();
	cleanup_device();
}

void d3d11::create_device() {
	// Setup swap chain description
	DXGI_SWAP_CHAIN_DESC sp_desc;
	std::memset(&sp_desc, 0, sizeof(DXGI_SWAP_CHAIN_DESC));
	sp_desc.BufferCount = 2u;

	sp_desc.BufferDesc.Width				   = 0u;
	sp_desc.BufferDesc.Height				   = 0u;
	sp_desc.BufferDesc.Format				   = DXGI_FORMAT_R8G8B8A8_UNORM;
	sp_desc.BufferDesc.RefreshRate.Numerator   = 60u;
	sp_desc.BufferDesc.RefreshRate.Denominator = 1u;

	sp_desc.Flags		 = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	sp_desc.BufferUsage	 = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sp_desc.OutputWindow = hwnd_;

	sp_desc.SampleDesc.Count   = 1u;
	sp_desc.SampleDesc.Quality = 0u;

	sp_desc.Windowed   = TRUE;
	sp_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	// Setup creation flags
	auto create_flags = 0u;
#ifdef _DEBUG
	create_flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif // _DEBUG

	// Create D3D11 stuff itself
	if (S_OK != D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, create_flags, nullptr, 0, D3D11_SDK_VERSION, &sp_desc, &swap_chain, &device, nullptr, &device_context)) {
		throw std::runtime_error("Can't create D3D11 device.");
	}
}

void d3d11::create_render_target() {
	ID3D11Texture2D *back_buffer = nullptr;
	swap_chain->GetBuffer(0u, IID_PPV_ARGS(&back_buffer));
	device->CreateRenderTargetView(back_buffer, nullptr, &render_target_view);
	back_buffer->Release();
}

void d3d11::cleanup_device() {
	cleanup_render_target();

	release_field<IDXGISwapChain>(swap_chain);
	release_field<ID3D11DeviceContext>(device_context);
	release_field<ID3D11Device>(device);
}

void d3d11::cleanup_render_target() {
	release_field<ID3D11RenderTargetView>(render_target_view);
}

template<typename T>
void d3d11::release_field(T *&field) {
	if (field) {
		field->Release();
		field = nullptr;
	}
}