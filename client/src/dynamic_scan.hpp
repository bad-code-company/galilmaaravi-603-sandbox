#ifndef DYNAMIC_SCAN_HPP
#define DYNAMIC_SCAN_HPP

#include "basic_scan.hpp"

namespace sandbox {
	class dynamic_scan : public basic_scan {
	public:
		struct os_info {
			static constexpr auto username_max_size = 0x100;
			static constexpr auto password_max_size = 0x80;

			std::filesystem::path vmx_path;
			char				  username[username_max_size] = { 0 };
			char				  password[password_max_size] = { 0 };

			explicit operator bool() const {
				return !vmx_path.empty() && username[0] && password[0];
			}
		};

	private:
		/// Private constructor for singleton
		explicit dynamic_scan() = default;

	public:
		/// Delete copy constructor and copy assign operator
		dynamic_scan(const dynamic_scan &)			  = delete;
		dynamic_scan &operator=(const dynamic_scan &) = delete;

		/**
		 * @return Instance of the scanning class
		 */
		static dynamic_scan &instance();

		struct os_info &os_info();

		/**
		 * @brief Runs the scan and saves the results to the class
		 */
		void run() override;
		/**
		 * @return Scanning results
		 */
		[[nodiscard]] std::string results() const override;

		[[nodiscard]] std::filesystem::path log_path() const;

	private:
		struct os_info os_info_;

		std::filesystem::path log_path_;
		std::string			  results_;

		static std::string current_date();
	};
} // namespace sandbox

#endif // DYNAMIC_SCAN_HPP
