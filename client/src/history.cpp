#include "history.hpp"
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <string>
#include <streambuf>
#include "dynamic_scan.hpp"
#include "exe.hpp"
#include "log.hpp"
#include "static_scan.hpp"

using namespace std::string_literals;
using namespace sandbox;

history::history() {
	load();

	add();
	add();
}

history &history::instance() {
	static history inst;
	return inst;
}

void history::load() {
	std::ifstream file(file_name, std::ios::in);
	if (file.is_open()) {
		std::string file_content(std::istreambuf_iterator<char>(file), {});
		if (!file_content.empty()) {
			try {
				auto history_json = nlohmann::json::parse(file_content);
				data_			  = history_json["history"].get<std::deque<nlohmann::json>>();
			} catch (const std::exception &e) {
				log::error("Can't parse history json: {}", e.what());
			}
		}
		file.close();
	}
}

void history::add() {
	// Check that at least one scan was performed
	if (dynamic_scan::instance().status() != basic_scan::status::success && static_scan::instance().status() != basic_scan::status::success)
		return;
	// Add data to history
	nlohmann::json scan_json;
	scan_json["file_path"]			   = exe::instance().path().string();
	scan_json["dynamic_scanned"]	   = dynamic_scan::instance().status() == basic_scan::status::success;
	scan_json["dynamic_scan_result"]   = dynamic_scan::instance().results();
	scan_json["dynamic_scan_log_path"] = dynamic_scan::instance().log_path().string();
	scan_json["static_scanned"]		   = static_scan::instance().status() == basic_scan::status::success;
	scan_json["static_scan_result"]	   = static_scan::instance().results();
	data_.push_back(std::move(scan_json));
	// Write json to file
	save();
}

void history::remove(nlohmann::json &&json) {
	auto it = std::find(data_.begin(), data_.end(), json);
	if (it != data_.end())
		data_.erase(it);
	save();
}

void history::clear() {
	data_.clear();
	save();
}

std::queue<nlohmann::json> history::get() const {
	return std::queue<nlohmann::json>(data_);
}

void history::save() const {
	std::ofstream file(file_name, std::ios::out);
	if (!file.is_open()) {
		log::error("Can't save history");
		return;
	}
	nlohmann::json history_json;
	history_json["history"] = data_;
	file << history_json.dump(4);
	file.close();
}