#include "yarapp.hpp"
#include <regex>

sandbox::yarapp::yarapp() {
	yr_initialize();
}

sandbox::yarapp::~yarapp() {
	yr_finalize();
}

bool sandbox::yarapp::scan_file(const std::filesystem::path &file_path) const {
	const auto rules = compile_rules(); // get compiled rules

	static constexpr auto callback = [](YR_SCAN_CONTEXT *context, int message, void *message_data, void *user_data) {
		if (message == CALLBACK_MSG_RULE_MATCHING)
			*reinterpret_cast<bool *>(user_data) = true;
		return 0;
	};					   // create callback for scanning
	bool is_match = false; // variable that provided to callback

	// basic_scan file
	yr_rules_scan_file(rules.get_rules(), file_path.string().c_str(), 0, callback, &is_match, 0);

	return is_match;
}

sandbox::yarapp::rules sandbox::yarapp::compile_rules() const {
	// create compiler
	YR_COMPILER *compiler = nullptr;
	yr_compiler_create(&compiler);

	// function to add rules file
	auto add_rules_file = [compiler](const std::filesystem::path &file_path) {
		FILE *rules_file = std::fopen(file_path.string().c_str(), "r");
		yr_compiler_add_file(compiler, rules_file, nullptr, nullptr);
		std::fclose(rules_file);
	};

	// go through rules in vector and add it to compiler
	for (auto &&rules_component : rules_components_) {
		// check which type of variable is current component
		if (std::get_if<std::filesystem::path>(&rules_component)) {
			add_rules_file(std::get<std::filesystem::path>(rules_component));
		} else if (std::get_if<std::string>(&rules_component)) {
			yr_compiler_add_string(compiler, std::get<std::string>(rules_component).data(), nullptr);
		} else {
			yr_compiler_add_fd(compiler, std::get<HANDLE>(rules_component), nullptr, nullptr);
		}
	}

	// go through all rules files in rules directories
	for (auto &&rules_dir : rules_dirs_) {
		for (auto &&rules_file_dir_entry : std::filesystem::directory_iterator(rules_dir)) {
			// check that files is .yar
			static auto yar_re = std::regex(R"(.+\.yar)", std::regex::icase);
			if (std::regex_match(rules_file_dir_entry.path().string(), yar_re)) {
				add_rules_file(rules_file_dir_entry.path());
			}
		}
	}

	// compile rules
	YR_RULES *compiled_rules = nullptr;
	yr_compiler_get_rules(compiler, &compiled_rules);
	yr_compiler_destroy(compiler); // destroy compiler

	return rules(compiled_rules); // return instance of support class
}
