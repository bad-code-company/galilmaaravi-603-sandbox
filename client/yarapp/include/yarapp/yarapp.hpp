#ifndef YARAPP_HPP
#define YARAPP_HPP

#include <yara.h>
#include <filesystem>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

namespace sandbox {
	class yarapp {
		/**
		 * @brief Support class for compiled yara rules (it destroys rules automaticly)
		 */
		class rules {
			YR_RULES *rules_ = nullptr; /// compiled rules pointer

		public:
			/**
			 * @brief Constructor, sets a rules pointer
			 * @param rules Compiled rules pointer
			 */
			explicit rules(YR_RULES *rules) {
				set_rules(rules);
			}

			/**
			 * @brief Destructor, destroys a rules pointer
			 */
			virtual ~rules() {
				set_rules(nullptr);
			}

			/**
			 * @brief Returns compiled rules pointer
			 * @return Compiled rules pointer
			 */
			inline YR_RULES *get_rules() const {
				return rules_;
			}

			/**
			 * @brief Destroys old rules and sets new
			 * @param rules New rules pointer
			 */
			inline void set_rules(YR_RULES *rules) {
				if (rules_)
					yr_rules_destroy(rules_);
				rules_ = rules;
			}
		};

		/// fields
		using rules_component = std::variant<std::filesystem::path, std::string, HANDLE>;
		std::vector<rules_component> rules_components_; /// rules components

		std::vector<std::filesystem::path> rules_dirs_; /// rules directories
	public:
		/**
		 * @brief Constructor, initiliazes an yara library
		 */
		explicit yarapp();
		/**
		 * @brief Destructor, finalizes an yara library
		 */
		virtual ~yarapp();

		/**
		 * @brief Adds file component to rules components vector
		 * @param rules_file_path Path of rules file
		 */
		inline void add_rules(const std::filesystem::path &rules_file_path) {
			rules_components_.push_back(rules_file_path);
		}

		/**
		 * @brief Adds string component to rules components vector
		 * @param rules_string Rules string
		 */
		inline void add_rules(std::string_view rules_string) {
			rules_components_.push_back(rules_string);
		}

		/**
		 * @brief Adds file descriptor component to rules components vector
		 * @param rules_fd Rules file descriptor
		 */
		inline void add_rules(const HANDLE rules_fd) {
			rules_components_.push_back(rules_fd);
		}

		/**
		 * @brief Adds rules directory to rules directories vector
		 * @param rules_folder Rules directory to add
		 */
		inline void add_rules_dir(const std::filesystem::path &rules_dir) {
			rules_dirs_.push_back(rules_dir);
		}

		/**
		 * @brief Scans files and returns is one of the rules match
		 * @param file_path Path of file to basic_scan
		 * @return Is one of the rules match
		 */
		bool scan_file(const std::filesystem::path &file_path) const;

	private:
		/**
		 * @brief Compiles current rules in the vector
		 * @return Compiled rules
		 */
		rules compile_rules() const;
	};
} // namespace sandbox

#endif // YARAPP_HPP
