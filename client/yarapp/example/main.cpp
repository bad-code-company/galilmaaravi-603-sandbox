#include "yarapp/yarapp.hpp"
#include <iostream>

int main() {
	// test files pathes
	const std::filesystem::path rules_path	   = "pua_cryptocoin_miner.yar";
	const std::filesystem::path rules_dir_path = "rules";

	const std::filesystem::path miner_path	   = "miner.exe";
	const std::filesystem::path not_miner_path = "test.exe";

	sandbox::yarapp yara; // wraper class
	// yara.add_rules( rules_path ); // add rules to class
	yara.add_rules_dir(rules_dir_path);

	try {
		// perform some test
		std::cout << "DETECT MINER: " << (yara.scan_file(miner_path) ? "DETECTED" : "NOT DETECTED") << std::endl;
		std::cout << "DETECT NOT MINER: " << (yara.scan_file(not_miner_path) ? "DETECTED" : "NOT DETECTED") << std::endl;
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
	} catch (...) {
		std::cerr << "Some error occured" << std::endl;
	}
}