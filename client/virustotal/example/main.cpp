#include "virustotal/virustotal.hpp"
#include <iostream>

int main() {
	static const std::filesystem::path file_path = R"(test.exe)";
	try {
		auto [positives, total] = sandbox::virustotal::scan_file(file_path);
		std::cout << "Result: " << positives << '/' << total << std::endl;
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
	} catch (...) {
		std::cerr << "Unknown error occurred" << std::endl;
	}
}
