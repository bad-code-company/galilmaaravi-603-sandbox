#include "virustotal.hpp"
#include <chrono>
#include <string>
#include <thread>
#include <cpr/cpr.h>
#include <nlohmann/json.hpp>

using namespace sandbox;

using namespace std::literals::chrono_literals;

std::tuple<int, int> sandbox::virustotal::scan_file(const std::filesystem::path &file_path) {
	auto resource = perform_scan(file_path);
	return perform_report(resource);
}

std::string virustotal::perform_scan(const std::filesystem::path &file_path) {
	auto params = cpr::Multipart{
		{"apikey",					   api_key},
		{  "file", cpr::File(file_path.string())}
	};

	auto resp = cpr::Post(cpr::Url(scan_url), params);
	if (resp.status_code != 200)
		throw virustotal_exception("Can't perform request");
	auto resp_json = nlohmann::json::parse(resp.text);
	return resp_json["resource"].get<std::string>();
}

std::tuple<int, int> virustotal::perform_report(const std::string &resource) {
	auto params = cpr::Multipart{
		{  "apikey",	api_key},
		{"resource", resource}
	};

	while (true) {
		std::this_thread::sleep_for(1500ms);

		try {
			auto resp = cpr::Post(cpr::Url(report_url), params);
			if (resp.status_code != 200)
				continue;
			auto resp_json = nlohmann::json::parse(resp.text);
			if (resp_json["response_code"].get<int>() != 1)
				continue;
			return std::make_tuple(resp_json["positives"].get<int>(), resp_json["total"].get<int>());
		} catch (...) {
		}
	}
}
