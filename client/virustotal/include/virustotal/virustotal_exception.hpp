#ifndef VIRUSTOTAL_EXCEPTION_HPP
#define VIRUSTOTAL_EXCEPTION_HPP

#include <exception>
#include <string>
#include <string_view>

namespace sandbox {
	class virustotal_exception : public std::exception {
	public:
		/**
		 * @brief Constructs an exception
		 * @param message Exception message
		 */
		explicit virustotal_exception(std::string_view message = "") {
			if (!message.empty()) {
				message_.append(": ");
				message_.append(message);
			}
		}

		/**
		 * @brief Return exception message
		 * @return Exception message
		 */
		[[nodiscard]] const char *what() const override {
			return message_.c_str();
		};

	protected:
		std::string message_ = "VT";
	};
} // namespace sandbox

#endif // VIRUSTOTAL_EXCEPTION_HPP