#ifndef VIRUSTOTAL_HPP
#define VIRUSTOTAL_HPP

#include "virustotal_exception.hpp"
#include <filesystem>
#include <string_view>
#include <tuple>

namespace sandbox {
	class virustotal {
		/// vt api urls
		static constexpr auto scan_url	 = "https://www.virustotal.com/vtapi/v2/file/scan";
		static constexpr auto report_url = "https://www.virustotal.com/vtapi/v2/file/report";
		/// vt api key
		static constexpr auto api_key = "28d733ed9499607185b16bc711574f746f4ddcd2dbaa09a4750501c49d6094cc";

	public:
		/**
		 * @brief A function that scans a given file with VirusTotal
		 * @param file_path The path of the file to basic_scan
		 */
		[[nodiscard]] static std::tuple<int, int> scan_file(const std::filesystem::path &file_path);

	private:
		/**
		 * @brief Performs a scan request
		 * @param file_path File path to upload
		 * @return Resource (var in order to access to results)
		 */
		[[nodiscard]] static inline std::string perform_scan(const std::filesystem::path &file_path);
		/**
		 * @brief Performs a report request
		 * @param resource Resource (var in order to access to results)
		 * @return Tuple of positives and total detections
		 */
		[[nodiscard]] static inline std::tuple<int, int> perform_report(const std::string &resource);
	};
} // namespace sandbox

#endif // VIRUSTOTAL_HPP