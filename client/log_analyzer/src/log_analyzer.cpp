#include "log_analyzer.hpp"
#include <fstream>
#include <regex>
#include <stdexcept>
#include <nlohmann/json.hpp>
#include "syscalls.hpp"

using namespace sandbox;

log_analyzer::log_analyzer(const std::filesystem::path &lua_dir_path, const std::filesystem::path &log_path)
	: lua_dir_path_(lua_dir_path) {
	lua_.open_libraries(sol::lib::base, sol::lib::string, sol::lib::os, sol::lib::math, sol::lib::bit32);
	lua_.new_usertype<basic_syscall>("base", "pid", &basic_syscall::pid, "name", &basic_syscall::name, "return_value", &basic_syscall::return_value);

	lua_.new_usertype<ntcreatefile>("ntcreatefile", sol::base_classes, sol::bases<basic_syscall>(), "file_name", &ntcreatefile::file_name, "share_access", &ntcreatefile::share_access, "create_disposition", &ntcreatefile::create_disposition, "create_options", &ntcreatefile::create_options);
	lua_.new_usertype<ntcreateprocess>("ntcreateprocess", sol::base_classes, sol::bases<basic_syscall>(), "new_pid", &ntcreateprocess::new_pid, "process_path", &ntcreateprocess::process_path, "desired_access", &ntcreateprocess::desired_access);
	lua_.new_usertype<ntcreateprocessex>("ntcreateprocessex", sol::base_classes, sol::bases<basic_syscall>(), "new_pid", &ntcreateprocessex::new_pid, "process_path", &ntcreateprocessex::process_path, "desired_access", &ntcreateprocessex::desired_access, "flags", &ntcreateprocessex::flags);

	syscalls_ = parse(lua_, log_path);
}

void log_analyzer::run() {
	oss_.clear();

	for (auto &&lua_dir_entry : std::filesystem::directory_iterator(lua_dir_path_)) {
		// check that files is .lua
		static auto lua_re = std::regex(R"(.+\.lua)", std::regex::icase);
		if (std::regex_match(lua_dir_entry.path().string(), lua_re)) {
			lua_["analyze_log"] = []() {
				throw std::runtime_error("Function is missing");
			};
			// add function
			lua_.script_file(lua_dir_entry.path().string());
			oss_ << "'" << lua_dir_entry.path().stem().string() << "'";
			try {
				bool res = lua_["analyze_log"](syscalls_);
				oss_ << " DETECTED: " << (res ? "TRUE" : "FALSE") << "\n";
			} catch (const std::exception &e) {
				oss_ << " ERROR: " << e.what() << "\n";
			}
		}
	}

	if (!oss_.rdbuf()->in_avail()) {
		// remove last newline
		oss_.seekp(-1, std::ios_base::end);
		oss_ << '\0';
	} else {
		throw std::runtime_error("Haven't found any lua script");
	}
}

std::string log_analyzer::results() const {
	return oss_.str();
}

std::vector<sol::object> log_analyzer::parse(sol::state &lua, const std::filesystem::path &log_path) {
	std::ifstream log_file(log_path);
	if (!log_file.is_open())
		throw std::runtime_error("Can't open log file");

	std::vector<sol::object> syscalls;

	// read line by line
	std::string line;
	while (std::getline(log_file, line)) {
		try {
			line	  = std::regex_replace(line, std::regex(R"(\\)"), R"(\\)");
			auto json = nlohmann::json::parse(line);

			auto name		  = json["name"].get<std::string>();
			auto pid		  = json["pid"].get<std::uintmax_t>();
			auto return_value = json["return_value"].get<std::uint32_t>();
			if (name == "NtCreateFile") {

				auto file_name			= json["parameters"]["file_name"].get<std::string>();
				auto share_access		= json["parameters"]["share_access"].get<std::uint32_t>();
				auto create_disposition = json["parameters"]["create_disposition"].get<std::uint32_t>();
				auto create_options		= json["parameters"]["create_options"].get<std::uint32_t>();

				auto syscall = sol::make_object<ntcreatefile>(lua, pid, return_value, file_name, share_access, create_disposition, create_options);
				syscalls.push_back(syscall);
			} else if (name == "NtCreateProcess" || name == "NtCreateProcessEx") {
				auto new_pid		= json["parameters"]["new_pid"].get<std::uintmax_t>();
				auto process_path	= json["parameters"]["process_path"].get<std::string>();
				auto desired_access = json["parameters"]["desired_access"].get<std::uint32_t>();

				sol::object syscall;
				if (name != "NtCreateProcessEx") {
					syscall = sol::make_object<ntcreateprocess>(lua, pid, return_value, new_pid, process_path, desired_access);
				} else {
					auto flags = json["parameters"]["flags"].get<std::uint32_t>();
					syscall	   = sol::make_object<ntcreateprocessex>(lua, pid, return_value, new_pid, process_path, desired_access, flags);
				}
				syscalls.push_back(syscall);
			}
		} catch (...) {}
	}

	return syscalls;
}