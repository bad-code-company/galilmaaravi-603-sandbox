#include <iostream>
#include <log_analyzer/log_analyzer.hpp>

int main() {
	auto lua_dir_path = R"(lua)";
	auto log_path	  = R"(logs\test.exe_2022-03-25_18-15-24.log)";

	sandbox::log_analyzer la(lua_dir_path, log_path);
	try {
		la.run();
		auto results = la.results();
		std::cout << results << std::endl;
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
	}
}