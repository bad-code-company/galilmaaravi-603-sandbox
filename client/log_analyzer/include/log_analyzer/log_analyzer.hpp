#ifndef LOG_ANALYZER_HPP
#define LOG_ANALYZER_HPP

#include <filesystem>
#include <sstream>
#include <string>
#include <sol/sol.hpp>

namespace sandbox {
	struct basic_syscall;

	class log_analyzer {
	public:
		/**
		 * @brief Constructs a log analyzer object
		 * @param lua_dir_path Path of directory with lua rules
		 * @param log_path Log file path
		 */
		log_analyzer(const std::filesystem::path &lua_dir_path, const std::filesystem::path &log_path);

		/**
		 * @brief Runs a log analyzer
		 */
		void run();

		/**
		 * @return Log analyzer results
		 */
		[[nodiscard]] std::string results() const;

	private:
		std::filesystem::path	 lua_dir_path_;
		sol::state				 lua_;
		std::vector<sol::object> syscalls_;

		std::ostringstream oss_;

		/**
		 * @brief Parses syscalls list from log file
		 * @param lua Lua state
		 * @param log_path Log file path
		 * @return Syscalls list
		 */
		static std::vector<sol::object> parse(sol::state &lua, const std::filesystem::path &log_path);
	};
} // namespace sandbox

#endif // LOG_ANALYZER_HPP