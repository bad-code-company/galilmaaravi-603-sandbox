#ifndef SYSCALLS_HPP
#define SYSCALLS_HPP

#include <cstdint>
#include <string>
#include <string_view>

namespace sandbox {
	struct basic_syscall {
		std::uintmax_t pid;
		std::string	   name;
		std::uint32_t  return_value;

		basic_syscall(std::uintmax_t pid, std::string_view name, std::uint32_t return_value) {
			this->pid		   = pid;
			this->name		   = name;
			this->return_value = return_value;
		}

		virtual ~basic_syscall() = default;
	};

	/// NtCreateFile
	struct ntcreatefile : public basic_syscall {
		std::string	  file_name;
		std::uint32_t share_access;
		std::uint32_t create_disposition;
		std::uint32_t create_options;

		ntcreatefile(std::uintmax_t pid, std::uint32_t return_value, std::string_view file_name, std::uint32_t share_access, std::uint32_t create_disposition, std::uint32_t create_options)
			: basic_syscall(pid, "ntcreatefile", return_value) {
			this->file_name			 = file_name;
			this->share_access		 = share_access;
			this->create_disposition = create_disposition;
			this->create_options	 = create_options;
		}
	};

	/// NtCreateProcess
	struct ntcreateprocess : public basic_syscall {
		std::uintmax_t new_pid;
		std::string	   process_path;
		std::uint32_t  desired_access;

		ntcreateprocess(std::uintmax_t pid, std::uint32_t return_value, std::uintmax_t new_pid, std::string_view process_path, std::uint32_t desired_access)
			: basic_syscall(pid, "ntcreateprocess", return_value) {
			this->new_pid		 = new_pid;
			this->process_path	 = process_path;
			this->desired_access = desired_access;
		}
	};

	/// NtCreateProcessEx
	struct ntcreateprocessex : public basic_syscall {
		std::uintmax_t new_pid;
		std::string	   process_path;
		std::uint32_t  desired_access;
		std::uint32_t  flags;

		ntcreateprocessex(std::uintmax_t pid, std::uint32_t return_value, std::uintmax_t new_pid, std::string_view process_path, std::uint32_t desired_access, std::uint32_t flags)
			: basic_syscall(pid, "ntcreateprocessex", return_value) {
			this->new_pid		 = new_pid;
			this->process_path	 = process_path;
			this->desired_access = desired_access;
			this->flags			 = flags;
		}
	};
} // namespace sandbox

#endif // SYSCALLS_HPP