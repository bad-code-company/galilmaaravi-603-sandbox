#include <vm/vmware_vm.hpp>
#include <iostream>

int main() {
	constexpr auto vmx_path = "Windows 10 x64.vmx";
	constexpr auto username = "User";
	constexpr auto password = "";

	sandbox::vmware_virtual_machine vm(vmx_path);
	try {
		vm.connect();
		vm.boot();
		vm.login(username, password);
		vm.transfer_file_to_guest("C:\\Users\\User\\Desktop\\test_host.txt", "C:\\Users\\User\\Desktop\\test_host.txt");
		vm.transfer_file_to_host("C:\\Users\\User\\Desktop\\test_guest.txt", "C:\\Users\\User\\Desktop\\test_guest.txt");
		vm.run_cmd("notepad");
		vm.run_exe("C:\\Windows\\System32\\notepad.exe");
	} catch (const sandbox::vm_exception &e) {
		std::cerr << e.what() << std::endl;
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
	} catch (...) {
		std::cerr << "Occured unknown error" << std::endl;
	}
}