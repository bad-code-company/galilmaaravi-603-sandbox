# Set vix api path
if (DEFINED ENV{VIXContentRoot})
    set(VIX_API_PATH ENV{VIXContentRoot})
else ()
    set(VIX_API_PATH "C:/Program Files (x86)/VMware/VMware VIX")
endif ()

# Check that VIX folder exists
if (EXISTS ${VIX_API_PATH})
    set(VIX_ROOT ${VIX_API_PATH})
endif ()
unset(VIX_API_PATH)

# Check that VIX is found
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(VIX REQUIRED_VARS VIX_ROOT)

message(STATUS "VIX_ROOT: " ${VIX_ROOT})

# Check architecture
if (CMAKE_SIZEOF_VOID_P EQUAL 4)
    set(VIX_PLATFORM "") # For x86 it's empty string
elseif (CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(VIX_PLATFORM "64")
else ()
    message(FATAL_ERROR "Unsupported architecture")
endif ()

# Add VIX library
set(VIX_LIB_NAME "VIX::vix")
set(VIX_LIB_BIN "${VIX_ROOT}/Vix${VIX_PLATFORM}AllProductsDyn.lib")

add_library(${VIX_LIB_NAME} INTERFACE IMPORTED)
target_link_libraries(${VIX_LIB_NAME} INTERFACE ${VIX_LIB_BIN})
target_include_directories(${VIX_LIB_NAME} INTERFACE ${VIX_ROOT})

unset(VIX_LIB_BIN)
unset(VIX_LIB_NAME)

# Set VIX DLL path (needed for run-time)
set(VIX_DLL_BIN "${VIX_ROOT}/Vix${VIX_PLATFORM}AllProductsDyn.dll")