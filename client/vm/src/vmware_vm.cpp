#include "vmware_vm.hpp"

using namespace sandbox;

vmware_vm::vmware_vm(const std::filesystem::path &vmx_path)
	: vmx_path_(vmx_path) {}

vmware_vm::~vmware_vm() {
	// disconnect();
}

void vmware_vm::connect() {
	if (is_connected())
		return;

	// create connect job and wait until it ends
	auto job   = VixHost_Connect(VIX_API_VERSION, VIX_SERVICEPROVIDER_VMWARE_WORKSTATION, nullptr, 0, nullptr, nullptr, 0, VIX_INVALID_HANDLE, nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_JOB_RESULT_HANDLE, &host_, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job); // release job handler
	if (VIX_FAILED(error)) {
		VixHost_Disconnect(host_);
		throw vm_exception(Vix_GetErrorText(error, nullptr));
	}
}

void vmware_vm::disconnect() {
	VixHost_Disconnect(host_);
}

bool vmware_vm::is_connected() const {
	return host_ != VIX_INVALID_HANDLE;
}

bool vmware_vm::is_booted() const {
	return vm_ != VIX_INVALID_HANDLE;
}

bool vmware_vm::is_logged_in() const {
	return is_logged_in_;
}

void vmware_vm::boot() {
	// connect to vmware if its not connected
	if (!is_connected())
		connect();

	// release vm handler if its exists
	if (vm_ != VIX_INVALID_HANDLE)
		shutdown();

	// open vm
	auto job   = VixVM_Open(host_, vmx_path_.string().c_str(), nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_JOB_RESULT_HANDLE, &vm_, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error)) {
		Vix_ReleaseHandle(vm_);
		throw vm_exception(Vix_GetErrorText(error, nullptr));
	}

	// power on
	job	  = VixVM_PowerOn(vm_, VIX_VMPOWEROP_LAUNCH_GUI, VIX_INVALID_HANDLE, nullptr, nullptr);
	error = VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error)) {
		Vix_ReleaseHandle(vm_);
		throw vm_exception(Vix_GetErrorText(error, nullptr));
	}

	// wait for full windows loaded
	job	  = VixVM_WaitForToolsInGuest(vm_, /* 5 minutes */ 300, nullptr, nullptr);
	error = VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error)) {
		shutdown();
		throw vm_exception(Vix_GetErrorText(error, nullptr));
	}
}

void vmware_vm::shutdown() {
	VixHandle job;
	VixError  error;
	do {
		job	  = VixVM_PowerOff(vm_, VIX_VMPOWEROP_FROM_GUEST, nullptr, nullptr);
		error = VixJob_Wait(job, VIX_PROPERTY_NONE);
		Vix_ReleaseHandle(job);
	} while (VIX_FAILED(error));

	Vix_ReleaseHandle(vm_);
}

VixHandle vmware_vm::create_snapshot(std::string_view name, bool include_memory) const {
	if (!is_booted())
		throw vm_exception("Machine is not booted");

	VixHandle snapshot_handle = VIX_INVALID_HANDLE;

	const auto flags = include_memory ? VIX_SNAPSHOT_INCLUDE_MEMORY : 0;

	auto job   = VixVM_CreateSnapshot(vm_, name.data(), nullptr, flags, VIX_INVALID_HANDLE, nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_JOB_RESULT_HANDLE, &snapshot_handle, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error))
		throw vm_exception(Vix_GetErrorText(error, nullptr));
	if (snapshot_handle == VIX_INVALID_HANDLE)
		throw vm_exception("Snapshot wasn't saved");

	return snapshot_handle;
}

void vmware_vm::remove_snapshot(VixHandle &snapshot) const {
	if (snapshot == VIX_INVALID_HANDLE)
		return;

	if (!is_booted())
		throw vm_exception("Machine is not booted");

	auto job = VixVM_RemoveSnapshot(vm_, snapshot, 0, nullptr, nullptr);
	VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
}

void vmware_vm::revert_to_snapshot(VixHandle snapshot) const {
	if (snapshot == VIX_INVALID_HANDLE)
		return;

	if (!is_booted())
		throw vm_exception("Machine is not booted");

	auto job   = VixVM_RevertToSnapshot(vm_, snapshot, 0, VIX_INVALID_HANDLE, nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error))
		throw vm_exception(Vix_GetErrorText(error, nullptr));
}

void vmware_vm::login(std::string_view username, std::string_view password, bool gui) {
	if (!is_booted())
		boot();

	const auto flags = gui ? VIX_LOGIN_IN_GUEST_REQUIRE_INTERACTIVE_ENVIRONMENT : 0;

	while (!is_logged_in()) {
		auto job   = VixVM_LoginInGuest(vm_, username.data(), password.data(), flags, nullptr, nullptr);
		auto error = VixJob_Wait(job, VIX_PROPERTY_NONE);
		Vix_ReleaseHandle(job);
		if (!VIX_FAILED(error)) {
			is_logged_in_ = true;
		}
	}
}

void vmware_vm::logout() {
	auto job = VixVM_LogoutFromGuest(vm_, nullptr, nullptr);
	VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);

	is_logged_in_ = false;
}

void vmware_vm::create_directory(const std::filesystem::path &dir) const {
	if (!is_logged_in())
		throw vm_exception("There is not logged user");

	auto job   = VixVM_CreateDirectoryInGuest(vm_, dir.string().c_str(), VIX_INVALID_HANDLE, nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error) && std::string_view(Vix_GetErrorText(error, nullptr)) != "The file already exists")
		throw vm_exception(Vix_GetErrorText(error, nullptr));
}

void vmware_vm::remove_directory(const std::filesystem::path &dir) const {
	if (!is_logged_in())
		throw vm_exception("There is not logged user");

	auto job = VixVM_DeleteDirectoryInGuest(vm_, dir.string().c_str(), 0, nullptr, nullptr);
	VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
}

void vmware_vm::transfer_file_to_guest(const std::filesystem::path &src, const std::filesystem::path &dst) const {
	if (!is_logged_in())
		throw vm_exception("There is not logged user");

	auto job   = VixVM_CopyFileFromHostToGuest(vm_, src.string().c_str(), dst.string().c_str(), 0, VIX_INVALID_HANDLE, nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error))
		throw vm_exception(Vix_GetErrorText(error, nullptr));
}

void vmware_vm::transfer_file_to_host(const std::filesystem::path &src, const std::filesystem::path &dst) const {
	if (!is_logged_in())
		throw vm_exception("There is not logged user");

	auto job   = VixVM_CopyFileFromGuestToHost(vm_, src.string().c_str(), dst.string().c_str(), 0, VIX_INVALID_HANDLE, nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error))
		throw vm_exception(Vix_GetErrorText(error, nullptr));
}

void vmware_vm::remove_file(const std::filesystem::path &file) const {
	if (!is_logged_in())
		throw vm_exception("There is not logged user");

	auto job   = VixVM_DeleteFileInGuest(vm_, file.string().c_str(), nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(error);
}

bool vmware_vm::does_process_exist(std::string_view process_name) const {
	if (!is_logged_in())
		throw vm_exception("There is not logged user");

	static auto constexpr icompare = [](std::string_view str1, std::string_view str2) -> bool {
		return _strcmpi(str1.data(), str2.data()) == 0;
	};

	auto job   = VixVM_ListProcessesInGuest(vm_, 0, nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_NONE);
	if (VIX_FAILED(error))
		throw vm_exception(Vix_GetErrorText(error, nullptr));

	auto found		   = false;
	auto process_count = VixJob_GetNumProperties(job, VIX_PROPERTY_JOB_RESULT_ITEM_NAME);
	for (decltype(process_count) i = 0; i < process_count && !found; ++i) {
		char *curr_proc_name = nullptr;
		error				 = VixJob_GetNthProperties(job, i, VIX_PROPERTY_JOB_RESULT_ITEM_NAME, &curr_proc_name, VIX_PROPERTY_NONE);
		if (!VIX_FAILED(error)) {
			if (curr_proc_name && icompare(curr_proc_name, process_name)) {
				found = true;
			}

			Vix_FreeBuffer(curr_proc_name);
		}
	}

	Vix_ReleaseHandle(job);
	return found;
}

void vmware_vm::run_cmd(std::string_view command) const {
	if (!is_logged_in())
		throw vm_exception("There is not logged user");

	auto job   = VixVM_RunScriptInGuest(vm_, nullptr, command.data(), 0, VIX_INVALID_HANDLE, nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error))
		throw vm_exception(Vix_GetErrorText(error, nullptr));
}

void vmware_vm::run_exe(const std::filesystem::path &exe_path, std::string_view params) const {
	if (!is_logged_in())
		throw vm_exception("There is not logged user");

	auto job		 = VixVM_RunProgramInGuest(vm_, exe_path.string().c_str(), params.data(), 0, VIX_INVALID_HANDLE, nullptr, nullptr);
	auto error		 = VixJob_Wait(job, VIX_PROPERTY_NONE);
	auto return_code = VixJob_GetNumProperties(job, VIX_PROPERTY_JOB_RESULT_GUEST_PROGRAM_EXIT_CODE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error))
		throw vm_exception(Vix_GetErrorText(error, nullptr));
	if (return_code != 0)
		throw vm_exception("Return code: " + std::to_string(return_code));
}
