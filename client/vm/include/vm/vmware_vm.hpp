#ifndef VMWARE_VIRTUAL_MACHINE_HPP
#define VMWARE_VIRTUAL_MACHINE_HPP

#include "vm.hpp"
#include "vm_exception.hpp"
#include <vix.h> // VIX API

namespace sandbox {
	class vmware_vm : public vm {
		std::filesystem::path vmx_path_; /// vmx config file path

		/// host and vm handles
		VixHandle host_ = VIX_INVALID_HANDLE;
		VixHandle vm_	= VIX_INVALID_HANDLE;

		bool is_logged_in_ = false; /// is logged into user

	public:
		/**
		 * @brief Constructs object
		 * @param vmx_path Path to vmx-config file
		 */
		explicit vmware_vm(const std::filesystem::path &vmx_path);
		/**
		 * @brief Clears memory
		 */
		virtual ~vmware_vm();

		/**
		 * @brief Return is there connection between api and vmware
		 * @return Is there connection between api and vmware
		 */
		[[nodiscard]] bool is_connected() const override;
		/**
		 * @brief Returns is vm booted
		 * @return Is vm booted
		 */
		[[nodiscard]] bool is_booted() const override;
		/**
		 * @brief Returns is there logged user
		 * @return Is there logged user
		 */
		[[nodiscard]] bool is_logged_in() const override;

		/**
		 * @brief Connects to the VMWare
		 */
		void connect() override;
		/**
		 * @brief Disconnects from the VMWare
		 */
		void disconnect() override;

		/**
		 * @brief Boots the vm image
		 */
		void boot() override;
		/**
		 * @brief Shutdowns the vm image
		 */
		void shutdown() override;

		/**
		 * @brief Creates snapshot with name
		 * @param name Name of snapshot
		 * @param include_memory Include memory in snapshot
		 * @return Snapshot handle
		 */
		[[nodiscard]] VixHandle create_snapshot(std::string_view name, bool include_memory = true) const;
		/**
		 * @brief Removes snapshot
		 * @param snapshot Snapshot reference
		 */
		void remove_snapshot(VixHandle &snapshot) const;
		/**
		 * @brief Reverts vm to snapshot
		 * @param snapshot Snapshot to revert
		 */
		void revert_to_snapshot(VixHandle snapshot) const;

		/**
		 * @brief Signs Into account
		 * @param username Account's username
		 * @param password Account's password
		 */
		void login(std::string_view username, std::string_view password, bool gui = true) override;
		/**
		 * @brief Signs Out from the account
		 */
		void logout() override;

		/**
		 * @brief Creates directory on a guest
		 * @param dir Dir path to create
		 */
		void create_directory(const std::filesystem::path &dir) const override;
		/**
		 * @brief Removes directory on a guest OS
		 * @param dir Directory path to remove
		 */
		void remove_directory(const std::filesystem::path &dir) const override;

		/**
		 * @brief Transfers files from host system to guest system
		 * @param src File path on host system
		 * @param dst File path on guest system
		 */
		void transfer_file_to_guest(const std::filesystem::path &src, const std::filesystem::path &dst) const override;
		/**
		 * @brief Transfers files from guest system to host system
		 * @param src File path on guest system
		 * @param dst File path on host system
		 */
		void transfer_file_to_host(const std::filesystem::path &src, const std::filesystem::path &dst) const override;
		/**
		 * @brief Removes file on a guest OS
		 * @param file File path to remove
		 */
		void remove_file(const std::filesystem::path &file) const override;

		/**
		 * @brief Checks and returns does process exist in process list
		 * @param process_name Process name
		 * @return Does process exist in process list
		 */
		[[nodiscard]] bool does_process_exist(std::string_view process_name) const override;
		/**
		 * @brief Runs command on guest system
		 * @param command Command to perform
		 */
		void run_cmd(std::string_view command) const override;
		/**
		 * @brief Runs exe on guest system
		 * @param exe_path Executable path
		 * @param params Run parameters
		 */
		void run_exe(const std::filesystem::path &exe_path, std::string_view params = "") const override;
	};
} // namespace sandbox

#endif // VMWARE_VIRTUAL_MACHINE_HPP