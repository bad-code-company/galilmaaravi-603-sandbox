#ifndef VIRTUAL_MACHINE_EXCEPTION_HPP
#define VIRTUAL_MACHINE_EXCEPTION_HPP

#include <exception>
#include <string>
#include <string_view>

namespace sandbox {
	class vm_exception : public std::exception {
	public:
		/**
		 * @brief Constructs an exception
		 * @param message Exception message
		 */
		explicit vm_exception(std::string_view message = "") {
			if (!message.empty()) {
				message_.append(": ");
				message_.append(message);
			}
		}

		/**
		 * @brief Return exception message
		 * @return Exception message
		 */
		[[nodiscard]] const char *what() const override {
			return message_.c_str();
		}

	protected:
		std::string message_ = "VM"; /// exception message
	};
} // namespace sandbox

#endif // VIRTUAL_MACHINE_EXCEPTION_HPP