#ifndef VIRTUAL_MACHINE_HPP
#define VIRTUAL_MACHINE_HPP

#include <filesystem>
#include <string_view>

namespace sandbox {
	class vm {
	public:
		/**
		 * @brief Return is there connection between api and hypervisor
		 * @return Is there connection between api and hypervisor
		 */
		[[nodiscard]] virtual bool is_connected() const = 0;
		/**
		 * @brief Returns is vm booted
		 * @return Is vm booted
		 */
		[[nodiscard]] virtual bool is_booted() const = 0;
		/**
		 * @brief Returns is there logged user
		 * @return Is there logged user
		 */
		[[nodiscard]] virtual bool is_logged_in() const = 0;

		/**
		 * @brief Connects to the VM hypervisor
		 */
		virtual void connect() = 0;
		/**
		 * @brief Disconnects from the VM hypervisor
		 */
		virtual void disconnect() = 0;

		/**
		 * @brief Boots the vm image
		 */
		virtual void boot() = 0;
		/**
		 * @brief Shutdowns the vm image
		 */
		virtual void shutdown() = 0;

		/**
		 * @brief Signs Into account
		 * @param username Account's username
		 * @param password Account's password
		 */
		virtual void login(std::string_view username, std::string_view password, bool gui = true) = 0;
		/**
		 * @brief Signs Out from the account
		 */
		virtual void logout() = 0;

		/**
		 * @brief Creates directory on a guest OS
		 * @param dir Dir path to create
		 */
		virtual void create_directory(const std::filesystem::path &dir) const = 0;
		/**
		 * @brief Removes directory on a guest OS
		 * @param dir Directory path to remove
		 */
		virtual void remove_directory(const std::filesystem::path &dir) const = 0;

		/**
		 * @brief Transfers files from host system to guest system
		 * @param src File path on host system
		 * @param dst File path on guest system
		 */
		virtual void transfer_file_to_guest(const std::filesystem::path &src, const std::filesystem::path &dst) const = 0;
		/**
		 * @brief Transfers files from guest system to host system
		 * @param src File path on guest system
		 * @param dst File path on host system
		 */
		virtual void transfer_file_to_host(const std::filesystem::path &src, const std::filesystem::path &dst) const = 0;
		/**
		 * @brief Removes file on a guest OS
		 * @param file File path to remove
		 */
		virtual void remove_file(const std::filesystem::path &file) const = 0;

		/**
		 * @brief Checks and returns does process exist in process list
		 * @param process_name Process name
		 * @return Does process exist in process list
		 */
		[[nodiscard]] virtual bool does_process_exist(std::string_view process_name) const = 0;
		/**
		 * @brief Runs command on guest system
		 * @param command Command to perform
		 */
		virtual void run_cmd(std::string_view command) const = 0;
		/**
		 * @brief Runs exe on guest system
		 * @param exe_path Executable path
		 * @param params Run parameters
		 */
		virtual void run_exe(const std::filesystem::path &exe_path, std::string_view params = "") const = 0;
	};
} // namespace sandbox

#endif // VIRTUAL_MACHINE_HPP