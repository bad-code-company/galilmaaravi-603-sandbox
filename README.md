# Sandbox ![](https://sloc.xyz/gitlab/bad-code-company/GalilMaaravi-603-Sandbox)

Sandbox is a dynamic/static malware analysis tool using System Service Dispatcher
Table hooking, YARA Rules and VirusTotal.

* [Features](#features)
    * [Lua rules-scripts](#lua-rules-scripts)
* [Requirements](#requirements)
    * [Run](#run)
    * [Build dependencies](#build-dependencies)
        * [Client](#client)
        * [Driver](#driver)
* [Installation](#installation)
* [Credits](#credits)

## Features

* **Dynamic scan**
    * Running a System Calls logging using VMware Workstation.
    * System Calls Log analysis via [Lua rules-scripts](#lua-rules-scripts).
* **Static scan**
    * File analysis via YARA Rules.
    * File analysis via VirusTotal.
* Scans history saving

### Lua rules-scripts

You may want to write your own Lua scripts in order to analyze a System Calls Log file.
In order to use your scripts, you should put it into **dynamic_scan/lua** directory.

In order to write analysis scripts, you should make a script that contains one callback function named `analyze_log`.
Its parameter is an array of System Calls Structs, which you can go throw and perform different checks.

There's an example of simple script that checks if log contains create file operation on **test.txt** file:

```lua
function analyze_log(syscalls)
    for i = 1, #syscalls do
        if syscalls[i].name == "ntcreatefile" then
            if syscalls[i].file_name == "test.txt" then
                return true
            end
        end
    end
    return false
end
```

Full System Calls Struct list you can find in [**
client/log_analyzer/include/log_analyzer/syscalls.hpp**](client/log_analyzer/include/log_analyzer/syscalls.hpp) file.

## Requirements

### Run

* [VMWare Workstation Pro](https://www.vmware.com/products/workstation-pro.html) with Windows 10 installed and
  test-mode. [LTSC](https://isofiles.bd581e55.workers.dev/Windows%2010/Windows%2010%20Enterprise%20LTSC%202021/) is
  preferred.\
  _In order to enable test-mode, run **Command Prompt** as administrator and execute next commands_:
    * bcdedit /set testsigning on
    * bcdedit /set nointegritychecks on

### Build dependencies

* [**(C++ 17
  required)** Build tools for Visual Studio](https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2022)
  .
  Only **Desktop development with C++** is required.
* [vcpkg](https://github.com/microsoft/vcpkg)
* [CMake](https://cmake.org/)

##### Client

* GUI
    * [fmt](https://github.com/fmtlib/fmt)
    * [imgui](https://github.com/ocornut/imgui)
    * [imgui-filebrowser](https://github.com/sccoouut/imgui-filebrowser) (added as Git submodule)
    * [imgui-notify](https://github.com/sccoouut/imgui-notify) (added as Git submodule)
* Virtual Machine
    * [VMWare Workstation Pro](https://www.vmware.com/products/workstation-pro.html)
    * [VMWare VIX API](https://customerconnect.vmware.com/downloads/details?downloadGroup=PLAYER-1400-VIX1170&productId=687)
* Log analyzer
    * [sol2](https://github.com/ThePhD/sol2)
    * [nlohmann-json](https://github.com/nlohmann/json)
* YARA Rules
    * [libyara](https://github.com/VirusTotal/yara/tree/master/libyara)
* VirusTotal
    * [cpr](https://github.com/libcpr/cpr)
    * [nlohmann-json](https://github.com/nlohmann/json)

##### Driver

* [WDK](https://docs.microsoft.com/en-us/windows-hardware/drivers/download-the-wdk)
* [FindWDK](https://github.com/sccoouut/FindWDK) (added as Git submodule)

## Installation

Install dependencies via [vcpkg](https://github.com/microsoft/vcpkg)

```cmd
vcpkg install cpr:x64-windows-static
vcpkg install fmt:x64-windows-static
vcpkg install imgui[core,dx11-binding,win32-binding]:x64-windows-static
vcpkg install nlohmann-json:x64-windows-static
vcpkg install sol2:x64-windows-static
vcpkg install yara:x64-windows-static
```

Build and install via [CMake](https://cmake.org/)

```cmd
mkdir build && cd build
cmake -A x64 -DCMAKE_TOOLCHAIN_FILE=/path/to/vcpkg/scripts/buildsystems/vcpkg.cmake ..
cmake --build . --config MinSizeRel
cmake --install . --config MinSizeRel --prefix=/installation/path
```

## Credits

* [yarGen](https://github.com/Neo23x0/yarGen)
* [VirusTotal API](https://developers.virustotal.com/v2.0/reference/getting-started)
* [Shark](https://github.com/9176324/Shark)
* [TitanHide](https://github.com/mrexodia/TitanHide)
* [SSDT Hook](https://github.com/moukayz/Notebook/blob/master/mouka/windowsinternal/ssdt-hook.md)
* [GetKernelBase](https://www.unknowncheats.me/forum/general-programming-and-reversing/427419-getkernelbase.html)
* [MSDN](https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/)
* [Icon](https://www.flaticon.com/free-icon/sandbox_6843458?term=sandbox&page=1&position=32&page=1&position=32&related_id=6843458&origin=search#)