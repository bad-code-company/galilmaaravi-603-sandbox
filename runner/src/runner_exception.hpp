#ifndef RUNNER_EXCEPTION_H
#define RUNNER_EXCEPTION_H

#include <exception>
#include <string>
#include <string_view>

namespace sandbox {
	class runner_exception : public std::exception {
	public:
		/**
		 * @brief Constructs an exception
		 * @param message Exception message
		 */
		runner_exception(std::string_view message = "")
			: message_(message) {}

		/**
		 * @brief Return exception message
		 * @return Exception message
		 */
		virtual const char *what() const {
			return message_.c_str();
		}

	protected:
		std::string message_; /// exception message
	};
} // namespace sandbox

#endif // RUNNER_EXCEPTION_H
