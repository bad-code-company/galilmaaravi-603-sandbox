#ifndef RUNNER_H
#define RUNNER_H

#include <filesystem>
#include <tuple>
#include "service/service.hpp"
#include "ioctl/ioctl.hpp"
#include "runner_exception.hpp"

namespace sandbox {
	class runner {
	public:
		/**
		 * @brief Constructor, sets the app path
		 * @param app_path Application path
		 */
		runner(const std::filesystem::path &app_path);
		/**
		 * @brief Default destructor
		 */
		~runner() = default;

		/**
		 * @brief Starts the runner
		 * @details Starts the driver, starts the app, starts the logging, lets for app to run some time, and then stops everything.
		 */
		void run();

	private:
		/// Constants
		static constexpr auto sandbox_device_symlink_name = "SandboxLink";
		static constexpr auto driver_name				  = "sandbox_driver";

		/// Fields
		std::filesystem::path app_path_;

		/**
		 * @brief Runs suspended process
		 * @param app_path Application path
		 * @return Process handle and process id
		 */
		static PROCESS_INFORMATION run_suspended_process(const std::filesystem::path &app_path);

		/**
		 * @return Current path
		 */
		static std::filesystem::path current_path();
	};
} // namespace sandbox

#endif // RUNNER_H