#include <iostream>
#include <regex>
#include "runner.hpp"

int main(int argc, char *argv[]) {
	if (argc <= 1 || !std::regex_match(argv[1], std::regex(R"(.+\.exe)"))) {
		std::cerr << "Error! Use: " << argv[0] << " <app-path>.exe" << std::endl;
		return -1;
	}

	try {
		sandbox::runner runner(argv[1]);
		runner.run();
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
	}

	return 0;
}
