#include "runner.hpp"
#include <chrono>
#include <iostream>
#include <thread>

using namespace std::chrono_literals;
using namespace std::string_literals;
using namespace sandbox;

runner::runner(const std::filesystem::path &app_path) {
	// Get full path
	char buf[MAX_PATH] = { 0 };
	GetFullPathNameA(app_path.generic_string().c_str(), MAX_PATH, buf, nullptr);
	app_path_ = std::filesystem::path(buf);
}

void runner::run() {
	auto	drv_path = current_path() / (driver_name + ".sys"s);
	service service(drv_path, driver_name, driver_name);
	ioctl	ioctl(sandbox_device_symlink_name);

	service.create();
	service.start();

	auto proc_info = run_suspended_process(app_path_);

	// Open ioctl
	ioctl.open();
	ioctl.send(ioctl::request_id::start_logging, proc_info.dwProcessId);

	// Unsuspend process
	ResumeThread(proc_info.hThread);

	// Wait for 3 minutes until log is creating
	std::this_thread::sleep_for(5s);

	// WIP: Terminate process (in driver)
	// TerminateProcess( proc_info.hProcess, 0 );
	CloseHandle(proc_info.hProcess);

	// Close ioctl
	ioctl.send(ioctl::request_id::end_logging);
	ioctl.close();

	service.stop();
	service.destroy();
}

PROCESS_INFORMATION runner::run_suspended_process(const std::filesystem::path &app_path) {
	STARTUPINFO			startup_info = { sizeof(STARTUPINFO) };
	PROCESS_INFORMATION proc_info;
	// Create process
	auto status = CreateProcessA(app_path.generic_string().c_str(), nullptr, nullptr, nullptr, FALSE, CREATE_SUSPENDED, nullptr, nullptr, &startup_info, &proc_info);
	if (!status)
		throw runner_exception("Can't run the process");

	return proc_info;
}

std::filesystem::path runner::current_path() {
	char path[MAX_PATH + 1] = { 0 };
	GetModuleFileNameA(nullptr, path, MAX_PATH);
	std::string str_path(path);
	return str_path.substr(0, str_path.find_last_of("\\/"));
}