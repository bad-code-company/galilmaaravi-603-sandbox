#include <functional>
#include <iostream>
#include <map>
#include <string>
#include "ioctl/ioctl.hpp"

constexpr auto device_symlink_name = "SandboxLink";
sandbox::ioctl g_ioctl(device_symlink_name);

namespace commands {
	bool exit() {
		return false;
	}

	bool open() {
		g_ioctl.open();
		std::cout << "Device was successfuly opened" << std::endl;
		return true;
	}

	bool close() {
		g_ioctl.close();
		std::cout << "Device was successfuly closed" << std::endl;
		return true;
	}

	bool send() {
		static bool					started = false;
		sandbox::ioctl::response_id resp;
		if (!started)
			resp = g_ioctl.send(sandbox::ioctl::request_id::start_logging, 66666);
		else
			resp = g_ioctl.send(sandbox::ioctl::request_id::end_logging);
		started = resp == sandbox::ioctl::response_id::start_logging;
		std::cout << "Received from the driver: " << static_cast<int>(resp) << std::endl;
		return true;
	}

	bool recv() {
		auto resp = g_ioctl.recv();
		std::cout << "Received from the driver: " << static_cast<int>(resp) << std::endl;
		return true;
	}

} // namespace commands

int main() {
	static const std::map<std::string, std::function<bool()>> commands{
		{ "exit",  commands::exit},
		{ "open",  commands::open},
		{"close", commands::close},
		{ "send",  commands::send},
		{ "recv",  commands::recv}
	};

	bool		continue_running = true;
	std::string command;

	while (continue_running) {
		std::cout << ">> ";
		std::cin >> command;
		try {
			continue_running = commands.at(command)();
		} catch (const sandbox::ioctl_exception &e) {
			std::cerr << e.what() << std::endl;
		} catch (const std::exception &e) {
			std::cerr << e.what() << std::endl;
		} catch (...) {
			std::cerr << "Occured unknown error" << std::endl;
		}
	}
}
