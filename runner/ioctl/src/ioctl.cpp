#include "ioctl.hpp"
#include "ioctl_exception.hpp"

#define DEVICE_SEND CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_WRITE_DATA)
#define DEVICE_RECV CTL_CODE(FILE_DEVICE_UNKNOWN, 0x802, METHOD_BUFFERED, FILE_READ_DATA)

using namespace sandbox;

ioctl::ioctl(std::string_view device_symlink_name)
	: device_symlink_name_(device_symlink_name) {}

ioctl::~ioctl() {
	if (is_opened())
		close();
}

void ioctl::open() {
	auto symlink = "\\\\.\\" + device_symlink_name_;
	device_		 = CreateFileA(symlink.c_str(), GENERIC_ALL, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_SYSTEM, 0);
	if (device_ == nullptr || device_ == INVALID_HANDLE_VALUE) {
		device_ = nullptr;
		throw ioctl_exception("Can't open device: " + std::to_string(GetLastError()));
	}
}

void ioctl::close() {
	if (is_opened()) {
		CloseHandle(device_);
		device_ = nullptr;
	}
}

ioctl::response_id ioctl::recv() const {
	if (!is_opened())
		throw ioctl_exception("Device is closed");

	// Read from driver
	response resp;

	auto status = DeviceIoControl(device_, DEVICE_RECV, nullptr, 0, &resp, sizeof(response), nullptr, 0);
	if (!status)
		throw ioctl_exception("Error during receiving the answer");

	return resp.response_id;
}

ioctl::response_id ioctl::send(request_id id, long pid) const {
	if (!is_opened())
		throw ioctl_exception("Device is closed");

	// send the message
	request req;
	req.request_id = id;
	req.pid		   = pid;

	auto status = DeviceIoControl(device_, DEVICE_SEND, &req, sizeof(request), nullptr, 0, nullptr, 0);
	if (!status)
		throw ioctl_exception("Error during sending the message");

	return recv();
}