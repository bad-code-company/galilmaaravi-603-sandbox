#ifndef IOCTL_EXCEPTION_H
#define IOCTL_EXCEPTION_H

#include <exception>
#include <string>
#include <string_view>

namespace sandbox {
	class ioctl_exception : public std::exception {
	public:
		/**
		 * @brief Constructs an exception
		 * @param message Exception message
		 */
		explicit ioctl_exception(std::string_view message = "") {
			if (!message.empty()) {
				message_.append(": ");
				message_.append(message);
			}
		}

		/**
		 * @brief Return exception message
		 * @return Exception message
		 */
		[[nodiscard]] const char *what() const override {
			return message_.c_str();
		}

	protected:
		std::string message_ = "IOCTL"; /// exception message
	};
} // namespace sandbox

#endif // IOCTL_EXCEPTION_H
