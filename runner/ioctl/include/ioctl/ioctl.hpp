#ifndef IOCTL_H
#define IOCTL_H

#include <string>
#include <variant>
#include <windows.h>
#include "ioctl_exception.hpp"

namespace sandbox {
	class ioctl {
	public:
		enum class request_id { start_logging = 10,
								end_logging	  = 20 };
		enum class response_id { start_logging = 11,
								 end_logging   = 21 };

		/*
		 * @brief Constructor, setups ioctl object fields
		 * param device_symlink_name Device's symlink name
		 */
		explicit ioctl(std::string_view device_symlink_name);
		/**
		 * @brief Destructor, cleans the memory and device
		 */
		~ioctl();

		/**
		 * @brief Returns is device opened
		 * @return Is device opened
		 */
		inline bool is_opened() const {
			return device_ != nullptr && device_ != INVALID_HANDLE_VALUE;
		}

		/**
		 * @brief Opens device with symlink
		 * @details Throws exception when can not open device
		 */
		void open();
		/**
		 * @brief Closes device if it's opened
		 */
		void close();

		/**
		 * @brief Reads text from the driver and returns it
		 * @return Read text
		 */
		response_id recv() const;

		/**
		 * @brief Function sends message to the driver and returns the answer from the driver
		 * @param id Request id to send
		 * @param pid Process id of process to log
		 * @return The answer from the driver
		 */
		response_id send(request_id id, long pid = -1) const;

	private:
		// Necessary constructions
		struct request {
			request_id request_id;
			long	   pid = -1;
		};

		struct response {
			response_id response_id;
		};

		/// Fields
		std::string device_symlink_name_;
		HANDLE		device_ = nullptr;
	};
} // namespace sandbox

#endif // IOCTL_H