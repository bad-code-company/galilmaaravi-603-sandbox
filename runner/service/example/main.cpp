#include <iostream>
#include <exception>
#include "service/service.hpp"

int main() {
	try {
		service s("driver.sys", "driver", "driver");
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
	}
}