#include "service.hpp"
#include "service_manager.hpp"

using namespace sandbox;

service::service(const std::filesystem::path &path, std::string_view name, std::string_view display_name, DWORD start_type)
	: name_(name), display_name_(display_name), start_type_(start_type) {
	// Get full path
	char buf[MAX_PATH] = { 0 };
	GetFullPathNameA(path.generic_string().c_str(), MAX_PATH, buf, nullptr);
	path_ = buf;
}

service::~service() {
	stop();
	destroy();
}

void service::create() {
	if (is_created())
		return;

	// Create service
	handle_ = CreateServiceA(sm.data(), name_.c_str(), display_name_.c_str(), SERVICE_ALL_ACCESS, SERVICE_KERNEL_DRIVER, start_type_, SERVICE_ERROR_NORMAL, path_.generic_string().c_str(), NULL, NULL, NULL, NULL, NULL);
	if (!handle_) {
		// May be service exists, then we'll open it
		handle_ = OpenServiceA(sm.data(), name_.c_str(), SERVICE_ALL_ACCESS);
		if (!handle_)
			throw service_exception("service::create - Service can't be created or opened");
	}
}

void service::destroy() {
	if (!is_created())
		return;

	if (is_started())
		throw service_exception("service::destroy - Service is already started");

	// Delete service
	DeleteService(handle_);
	handle_ = nullptr;
}

void service::start() {
	if (is_started())
		return;

	// Start service
	if (!StartServiceA(handle_, 0, nullptr))
		throw service_exception("service::start - Service can't be started");

	started_ = true;
}

void service::stop() {
	if (!is_started())
		return;

	// Stop service
	SERVICE_STATUS ss;
	if (!ControlService(handle_, SERVICE_CONTROL_STOP, &ss))
		throw service_exception("service::stop - Service can't be stopped");

	started_ = false;
}