#include "service_manager.hpp"
#include <iostream>

using namespace sandbox;

service_manager::service_manager(DWORD desired_access) {
	handle_ = OpenSCManagerA(nullptr, nullptr, desired_access);
	if (!handle_)
		throw service_exception("Service Manager is nullptr");
}

service_manager::~service_manager() {
	CloseServiceHandle(handle_);
}

SC_HANDLE &service_manager::data() {
	return handle_;
}

const SC_HANDLE &service_manager::data() const {
	return handle_;
}