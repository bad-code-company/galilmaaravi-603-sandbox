#ifndef SERVICE_MANAGER_H
#define SERVICE_MANAGER_H

#include <windows.h>
#include "service_exception.hpp"

namespace sandbox {
	class service_manager {
	public:
		/**
		 * @brief Constructor, creates service manager
		 */
		service_manager(DWORD desired_access = SC_MANAGER_ALL_ACCESS);
		/**
		 * @brief Destructor, deletes service manager
		 */
		~service_manager();

		/**
		 * @return Service manager handle
		 */
		SC_HANDLE &data();
		/**
		 * @return Service manager handle (not mutable)
		 */
		const SC_HANDLE &data() const;

	private:
		/// Fields
		SC_HANDLE handle_ = nullptr;
	};
} // namespace sandbox

#endif // SERVICE_MANAGER_H