#ifndef SERVICE_EXCEPTION_H
#define SERVICE_EXCEPTION_H

#include <exception>
#include <string>
#include <string_view>

namespace sandbox {
	class service_exception : public std::exception {
	public:
		/**
		 * @brief Constructs an exception
		 * @param message Exception message
		 */
		explicit service_exception(std::string_view message = "") {
			if (!message.empty()) {
				message_.append(": ");
				message_.append(message);
			}
		}

		/**
		 * @brief Return exception message
		 * @return Exception message
		 */
		[[nodiscard]] const char *what() const override {
			return message_.c_str();
		}

	protected:
		std::string message_ = "Service"; /// exception message
	};
} // namespace sandbox

#endif // SERVICE_EXCEPTION_H