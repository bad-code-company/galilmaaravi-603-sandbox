#ifndef SERVICE_H
#define SERVICE_H

#include <filesystem>
#include <string_view>
#include <windows.h>
#include "service_manager.hpp"
#include "service_exception.hpp"

namespace sandbox {
	class service {
	public:
		/**
		 * @brief Initializes the service object
		 * @param path Path of the driver
		 * @param name Name of the driver
		 * @param display_name Display name of the driver
		 * @param start_type Start type of the driver (Default is demanded)
		 */
		service(const std::filesystem::path &path, std::string_view name, std::string_view display_name, DWORD start_type = SERVICE_DEMAND_START);
		/**
		 * @brief Deinitializes the service (destroys, stops)
		 */
		~service();

		/**
		 * @return Returns is server created
		 */
		inline bool is_created() const {
			return handle_ != nullptr;
		}

		/**
		 * @return Returns is server started
		 */
		inline bool is_started() const {
			return started_;
		}

		/**
		 * @brief Creates service
		 */
		void create();
		/**
		 * @brief Destroys service
		 */
		void destroy();

		/**
		 * @brief Starts service
		 */
		void start();
		/**
		 * @brief Stops the service
		 */
		void stop();

	private:
		/// Service manager
		service_manager sm;

		/// Fields
		std::filesystem::path path_;
		std::string			  name_;
		std::string			  display_name_;
		DWORD				  start_type_ = SERVICE_DEMAND_START;

		SC_HANDLE handle_  = nullptr;
		bool	  started_ = false;
	};
} // namespace sandbox

#endif // SERVICE_H
