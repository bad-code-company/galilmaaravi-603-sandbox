#ifndef LOCK_GUARD_HPP
#define LOCK_GUARD_HPP

#include "core.hpp"

namespace sandbox::ktl {
	/**
	 * @brief Implementation of lock guard (RAII mutex lock)
	 * @tparam TLock Mutex type
	 */
	template<typename TLock>
	class lock_guard {
	public:
		/**
		 * @brief Sets lock, locks it
		 * @param lock Mutex lock
		 */
		lock_guard(TLock &lock)
			: lock_(lock) {
			lock_.lock();
		}

		/**
		 * @brief Unlocks lock
		 */
		~lock_guard() {
			lock_.unlock();
		}

	private:
		TLock &lock_; /// mutex itself
	};
} // namespace sandbox::ktl

#endif // LOCK_GUARD_HPP