#ifndef VECTOR_HPP
#define VECTOR_HPP

#include "core.hpp"
#include "utility.hpp"

namespace sandbox::ktl {
	template<typename T, ULONG Tag = DRIVER_TAG, POOL_TYPE Pool = DEFAULT_POOL>
	class vector {
		static_assert(Tag != 0);

		/// Necessary stuff
		using iterator		  = T *;
		using const_iterator  = const T *;
		using reference		  = T &;
		using const_reference = const T &;

	public:
		/**
		 * @brief Constructor, allocates memory for array
		 * @param capacity Start capacity (default is 2)
		 */
		vector(size_t capacity = default_capacity) {
			if (capacity <= 0)
				capacity = default_capacity;

			elements_ = allocate(capacity_ = capacity);
		}

		/**
		 * @brief Copies data from other vector
		 * @param other Vector to copy from
		 */
		vector(const vector &other) {
			if (other.elements_) {
				if (other.size_ > capacity_) {
					if (elements_)
						deallocate(elements_, size_);
					elements_ = allocate(capacity_ = other.capacity_);
				}
				memcpy(elements_, other.elements_, sizeof(T) * other.size_);
			}
			size_ = other.size_;
		}

		/**
		 * @brief Copy '=' operator
		 * @param other Vector to copy from data
		 * @return This vector
		 */
		vector &operator=(const vector &other) {
			NT_ASSERT(this != &other);
			if (other.elements_) {
				if (other.size_ > capacity_) {
					if (elements_)
						deallocate(elements_, size_);
					elements_ = allocate(capacity_ = other.capacity_);
				}
				memcpy(elements_, other.elements_, sizeof(T) * other.size_);
			}
			size_ = other.size_;
			return *this;
		}

		/**
		 * @brief Moves data from other vector
		 * @param other Vector to move from
		 */
		vector(vector &&other) {
			capacity_		= other.capacity_;
			other.capacity_ = default_capacity;
			size_			= other.size_;
			other.size_		= 0;
			elements_		= other.elements_;
			other.elements_ = allocate(other.capacity_);
		}

		/**
		 * @brief Move '=' operator
		 * @param other Vector to move from data
		 * @return This vector
		 */
		vector &operator=(vector &&other) {
			NT_ASSERT(this != &other);
			capacity_		= other.capacity_;
			other.capacity_ = default_capacity;
			size_			= other.size_;
			other.size_		= 0;
			elements_		= other.elements_;
			other.elements_ = allocate(other.capacity_);
			return *this;
		}

		/*
		 * @brief Destructor, cleans memory
		 */
		~vector() {
			if (elements_)
				deallocate(elements_, size_);
		}

		/**
		 * @return Capacity of vector
		 */
		size_t capacity() const {
			return capacity_;
		}

		/**
		 * @return Size of vector
		 */
		size_t size() const {
			return size_;
		}

		/**
		 * @return Elements pointer
		 */
		T *data() {
			return elements_;
		}

		/**
		 * @return Elements pointer (not mutable)
		 */
		const T *data() const {
			return elements_;
		}

		/**
		 * @return Is vector empty
		 */
		bool empty() const {
			return size_ == 0;
		}

		/** Data access **/

		/**
		 * @brief Returns value at specific index
		 * @param index Index
		 * @return Value
		 */
		reference at(const size_t index) {
			NT_ASSERT(index < size_);
			return elements_[index];
		}

		/**
		 * @brief Returns value at specific index (not mutable)
		 * @param index Index
		 * @return Value
		 */
		const_reference at(const size_t index) const {
			NT_ASSERT(index < size_);
			return elements_[index];
		}

		/**
		 * @brief Returns value at specific index
		 * @param index Index
		 * @return Value
		 */
		reference operator[](const size_t index) {
			return at(index);
		}

		/**
		 * @brief Returns value at specific index (not mutable)
		 * @param index Index
		 * @return Value
		 */
		const_reference operator[](const size_t index) const {
			return at(index);
		}

		/**
		 * @return Returns first element of the vector
		 */
		reference front() {
			return elements_[0];
		}

		/**
		 * @return Returns first element of the vector (not mutable)
		 */
		const_reference front() const {
			return elements_[0];
		}

		/**
		 * @return Returns last element of the vector
		 */
		reference back() {
			if (size_ == 0)
				return elements_[0];
			return elements_[size_ - 1];
		}

		/**
		 * @return Returns last element of the vector (not mutable)
		 */
		const_reference back() const {
			if (size_ == 0)
				return elements_[0];
			return elements_[size_ - 1];
		}

		/**
		 * @return Begin ptr of vector
		 */
		iterator begin() {
			return elements_;
		}

		/**
		 * @return Begin ptr of vector (not mutable)
		 */
		const_iterator begin() const {
			return elements_;
		}

		/**
		 * @return End ptr of vector
		 */
		iterator end() {
			return elements_ + size_;
		}

		/**
		 * @return End ptr of vector (not mutable)
		 */
		const_iterator end() const {
			return elements_ + size_;
		}

		/** Allocation related **/

		/**
		 * @brief Reserves new memory for vector
		 * @param capacity New minimum capacity
		 */
		void reserve(const size_t capacity) {
			if (capacity_ < capacity) {
				// save old elements addr
				auto old_elements = elements_;
				// calc multiplicity factor
				auto multiplicity_factor = static_cast<size_t>((capacity - capacity_ + 1) / default_capacity);
				capacity_ += default_capacity * multiplicity_factor;
				// allocate new array
				elements_ = allocate(capacity_);
				memcpy(elements_, old_elements, sizeof(T) * size_);
				// Clear old memory
				for (auto i = 0u; i < size_; ++i)
					old_elements[i].~T();
				delete old_elements;
			}
		}

		/**
		 * @brief Resizes vector
		 * @param size New vector size
		 */
		void resize(const size_t size) {
			// reserve new memory if size is bigger than capacity
			if (capacity_ < size)
				reserve(size);
			size_ = size;
		}

		/**
		 * @brief Clears vector
		 * @detail Calls destructors and sets size 0
		 */
		void clear() {
			for (auto i = 0u; i < size_; ++i)
				elements_[i].~T();
			size_ = 0;
		}

		/** Data assigning **/

		/**
		 * @brief Pushes new value to vector
		 * @param value Value to push
		 */
		void push_back(const T &value) {
			NT_ASSERT(size_ <= capacity_);
			if (size_ == capacity_)
				reserve(capacity_ + default_capacity);
			elements_[size_++] = value;
		}

		/**
		 * @brief Pushes new value to vector
		 * @param value Value to push
		 */
		void push_back(T &&value) {
			NT_ASSERT(size_ <= capacity_);
			if (size_ == capacity_)
				reserve(capacity_ + default_capacity);
			elements_[size_++] = ktl::forward<T>(value);
		}

		/**
		 * @brief Removes last value of vector
		 */
		void pop_back() {
			NT_ASSERT(size_ > 0);
			elements_[--size_].~T();
		}

		/**
		 * @brief Resizes vector and values of vector to specific value
		 * @param count New vector size
		 * @param value Value to set
		 */
		void assign(const size_t count, const T &value) {
			resize(count);
			for (auto i = 0u; i < size_; ++i)
				elements_[i] = value;
		}

		/**
		 * @brief Emplaces back new element
		 * @tparam Args Args types
		 * @param args Args values
		 * @return Element reference
		 */
		template<typename... Args>
		vector &emplace_back(Args &&...args) {
			push_back(T(ktl::forward(args)));
			return elements_[size_ - 1];
		}

		/**
		 * @brief Erases element on spefic position
		 * @param pos Position to remove from
		 * @return Next element on vector
		 */
		iterator erase(const_iterator pos) {
			NT_ASSERT(!empty());
			// Calculate dist from the begin
			const size_t dist = pos - elements_;
			// Move elements
			for (auto i = dist; i < size_ - 1; ++i) {
				elements_[i] = elements_[i + 1];
			}
			--size_;
			// If it was not last elements return iterator at this place
			if (dist < size_)
				return &elements_[dist];
			return &elements_[size_ - 1];
		}

		/**
		 * @param value Value to check if vector contains
		 * @return Does vector contain the given value
		 */
		bool contain(const T &value) const {
			for (int i = 0; i < size_; ++i)
				if (elements_[i] == value)
					return true;
			return false;
		}

	protected:
		/// Fields
		T	  *elements_ = nullptr;
		size_t capacity_ = 2;
		size_t size_	 = 0;

	private:
		/**
		 * @brief Allocates memory for vector
		 * @param size Size of memory to allocate
		 * @return Memory ptr
		 */
		static T *allocate(const size_t size) {
			NT_ASSERT(size >= 0);
			auto buffer = operator new(sizeof(T) * size, Pool, Tag);
			if (!buffer)
				return nullptr;

			memset(buffer, 0, sizeof(T) * size);
			return static_cast<T *>(buffer);
		}

		/**
		 * @brief Deallocates the memory of the buffer
		 * @param buffer Buffer pointer
		 */
		static void deallocate(T *buffer, const size_t size) {
			NT_ASSERT(buffer != nullptr);
			// Call destructors
			for (auto i = 0u; i < size; ++i)
				buffer[i].~T();
			// Clear the memory
			delete buffer;
		}

		static constexpr auto default_capacity = 2u;
	};
} // namespace sandbox::ktl

#endif // VECTOR_HPP