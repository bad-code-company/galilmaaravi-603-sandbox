#ifndef FUNCTIONAL_HPP
#define FUNCTIONAL_HPP

#include "core.hpp"
#include "unique_ptr.hpp"

namespace sandbox::ktl {
	template<typename>
	class function;

	template<typename R, typename... Args>
	class function<R(Args...)> {
	public:
		/**
		 * @brief Default constructor, leaves function null
		 */
		function() = default;

		/**
		 * @brief Copies function from other
		 * @param other Function to copy from
		 */
		function(const function &other) {
			callable_ = move(other.callable_->copy());
		}

		function &operator=(const function &other) {
			callable_ = move(other.callable_->copy());
			return *this;
		}

		/**
		 * @brief Moves function from other
		 * @param other Function to move from
		 */
		function(function &&other) noexcept {
			callable_ = move(other.callable_);
		}

		function &operator=(function &&other) noexcept {
			callable_ = move(other.callable_);
			return *this;
		}

		/**
		 * @brief Constructor, sets callable (function itself)
		 * @tparam T Function type
		 * @param t Function addr
		 */
		template<typename T>
		explicit function(T &&t)
			: callable_(ktl::make_unique<callable<T>>(t)) {}

		/**
		 * @brief Set operator
		 * @tparam T Function type
		 * @param t Function addr
		 * @return Class instance
		 */
		template<typename T>
		function &operator=(T &&t) {
			callable_ = ktl::make_unique<callable<T>>(t);
			return *this;
		}

		/**
		 * @brief Calls the function
		 * @param args Arguments of the function
		 * @return Return value of call
		 */
		R operator()(Args... args) const {
			NT_ASSERT(callable_);
			return callable_->invoke(ktl::forward<Args>(args)...);
		}

		/**
		 * @return Does callable exist
		 */
		explicit operator bool() const {
			return callable_;
		}

	private:
		/// interface of callable
		class basic_callable {
		public:
			/**
			 * @brief Default virtual destructor
			 */
			virtual ~basic_callable() = default;
			/**
			 * @brief Calls the function
			 * @param args Arguments of the function
			 * @return Return value of call
			 */
			virtual R invoke(Args &&...args) const = 0;
			/**
			 * @return Returns unique ptr copy of entire object
			 */
			virtual unique_ptr<basic_callable> copy() const = 0;
		};

		/// callable class
		template<typename T>
		class callable : public basic_callable {
			T t_;

		public:
			/**
			 * @brief Constructor, sets the function
			 * @param t Callable function
			 */
			explicit callable(const T &t)
				: t_(t) {}

			/**
			 * @brief Default virtual destructor
			 */
			~callable() = default;

			/**
			 * @brief Calls the function
			 * @param args Arguments of the function
			 * @return Return value of call
			 */
			R invoke(Args &&...args) const {
				return t_(forward<Args>(args)...);
			}

			/**
			 * @return Returns unique ptr copy of entire object
			 */
			virtual unique_ptr<basic_callable> copy() const {
				return make_unique<callable<T>>(t_);
			}
		};

	protected:
		ktl::unique_ptr<basic_callable> callable_; /// callable ptr
	};
} // namespace sandbox::ktl

#endif // FUNCTIONAL_HPP
