#ifndef UTILITY_HPP
#define UTILITY_HPP

#include "core.hpp"

namespace sandbox::ktl {
	/**
	 * @brief Checks is class devired from base
	 * @tparam Base Base class
	 * @tparam Derived Check class
	 */
	template<typename Base, typename Derived>
	class is_base_of {
		constexpr static bool check(Base *) {
			return true;
		}

		constexpr static bool check(...) {
			return false;
		}

	public:
		enum { value = check(static_cast<Derived *>(0)) };
	};

	/** Move implementation **/
	template<typename T>
	struct remove_reference { typedef T type; };

	template<typename T>
	struct remove_reference<T &> { typedef T type; };

	template<typename T>
	struct remove_reference<T &&> { typedef T type; };

	template<typename T>
	constexpr typename remove_reference<T>::type &&move(T &&arg) noexcept {
		return static_cast<typename remove_reference<T>::type &&>(arg);
	}

	/** Forward implementation **/
	template<typename T>
	struct is_lvalue_reference { static constexpr bool value = false; };

	template<typename T>
	struct is_lvalue_reference<T &> { static constexpr bool value = true; };

	template<typename T>
	constexpr T &&forward(typename remove_reference<T>::type &arg) noexcept {
		return static_cast<T &&>(arg);
	}

	template<typename T>
	constexpr T &&forward(typename remove_reference<T>::type &&arg) noexcept {
		static_assert(!is_lvalue_reference<T>::value, "invalid rvalue to lvalue conversion");
		return static_cast<T &&>(arg);
	}
} // namespace sandbox::ktl

#endif // UTILITY_HPP