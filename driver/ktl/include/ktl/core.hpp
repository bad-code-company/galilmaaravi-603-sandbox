#ifndef CORE_HPP
#define CORE_HPP

#include <ntddk.h>
#include "crt/core.hpp"

namespace sandbox::ktl {
	class core {
	public:
		/**
		 * @brief Inits the KTL module
		 */
		static void init() {
			crt::core::init();
		}

		/**
		 * @brief Deinits the KTL module
		 */
		static void deinit() {
			crt::core::deinit();
		}
	};
} // namespace sandbox::ktl

#endif // CORE_HPP