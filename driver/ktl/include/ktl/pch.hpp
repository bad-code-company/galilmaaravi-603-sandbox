#ifndef PCH_HPP
#define PCH_HPP

#define SbPrint(_x_, ...) DbgPrint("[SANDBOX]: " _x_, __VA_ARGS__)

#endif // PCH_HPP