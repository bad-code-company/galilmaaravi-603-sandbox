#ifndef MUTEX_HPP
#define MUTEX_HPP

#include "core.hpp"

namespace sandbox::ktl {
	/**
	 * @brief Mutex implementation with KMUTEX
	 */
	class mutex {
	public:
		/**
		 * @brief Inits the mutex
		 */
		void init();

		/**
		 * @brief Locks mutex
		 */
		void lock();
		/**
		 * @brief Unlocks mutex
		 */
		void unlock();

	private:
		KMUTEX mutex_;
	};
} // namespace sandbox::ktl

#endif // MUTEX_HPP