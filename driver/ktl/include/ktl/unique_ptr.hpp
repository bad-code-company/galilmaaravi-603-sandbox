#ifndef UNIQUE_PTR_HPP
#define UNIQUE_PTR_HPP

#include "core.hpp"
#include "utility.hpp"

namespace sandbox::ktl {
	template<typename T>
	class unique_ptr {
	public:
		/**
		 * @brief Constructor of unique lock, sets the ptr
		 * @param ptr Pointer itself
		 */
		unique_ptr(T *ptr = nullptr)
			: ptr_(ptr) {}

		/*
		 * @brief Destructor, cleans the memory
		 */
		~unique_ptr() {
			reset();
		}

		/// Deleted copy constructor, because ptr is unique
		unique_ptr(const unique_ptr &) = delete;
		/// Deleted copy = operator, because ptr is unique
		unique_ptr &operator=(const unique_ptr &) = delete;

		/**
		 * @brief Move constructor
		 * @param other Unique ptr to move from
		 */
		unique_ptr(unique_ptr &&other) {
			ptr_ = ktl::move(other.release());
		}

		/**
		 * @brief Move constructor
		 * @tparam U Should be subclass of T
		 * @param other Unique ptr to move from
		 */
		template<typename U>
		unique_ptr(unique_ptr<U> &&other) {
			static_assert(is_base_of<T, U>::value);

			ptr_ = ktl::move(other.release());
		}

		/**
		 * @brief Move = operator
		 * @param other Unique ptr to move from
		 * @return Class instance
		 */
		unique_ptr &operator=(unique_ptr &&other) {
			if (this == &other)
				return *this;

			reset();
			ptr_ = ktl::move(other.release());
			return *this;
		}

		/**
		 * @brief Move = operator
		 * @tparam U Should be subclass of T
		 * @param other Unique ptr to move from
		 * @return Class instance
		 */
		template<typename U>
		unique_ptr &operator=(unique_ptr<U> &&other) {
			static_assert(is_base_of<T, U>::value);

			reset();
			ptr_ = ktl::move(other.release());
			return *this;
		}

		T *release() {
			auto tmp = ptr_;
			ptr_	 = nullptr;
			return tmp;
		}

		void reset(T *ptr = nullptr) {
			if (ptr_)
				delete ptr_;
			ptr_ = ptr;
		}

		/**
		 * @brief Returns pointer
		 * @return Pointer
		 */
		T *get() const {
			return ptr_;
		}

		/**
		 * @brief Returns pointer value
		 * @return Pointer value
		 */
		T &operator*() const {
			return *ptr_;
		}

		/**
		 * @brief Returns pointer
		 * @return Pointer
		 */
		T *operator->() const {
			return ptr_;
		}

		/**
		 * @brief Returns is ptr nullptr (for ifs, etc)
		 */
		explicit operator bool() const {
			return ptr_;
		}

	protected:
		/// pointer itself
		T *ptr_ = nullptr;
	};

	/**
	 * @brief Returns unique ptr instance
	 * @tparam T Type of unique ptr
	 * @tparam Args Args types for constructor
	 * @param args Args for constructor
	 * @return Unique ptr instance
	 */
	template<typename T, typename... Args>
	unique_ptr<T> make_unique(Args &&...args) {
		return unique_ptr<T>(new T(forward<Args>(args)...));
	}
} // namespace sandbox::ktl

#endif // UNIQUE_PTR_HPP