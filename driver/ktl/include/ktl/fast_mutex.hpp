#ifndef FAST_MUTEX_HPP
#define FAST_MUTEX_HPP

#include "core.hpp"

namespace sandbox::ktl {
	/**
	 * @brief Mutex implementation with FAST_MUTEX
	 */
	class fast_mutex {
	public:
		/**
		 * @brief Inits the mutex
		 */
		void init();

		/**
		 * @brief Locks mutex
		 */
		void lock();
		/**
		 * @brief Unlocks mutex
		 */
		void unlock();

	private:
		FAST_MUTEX mutex_;
	};
} // namespace sandbox::ktl

#endif // FAST_MUTEX_HPP