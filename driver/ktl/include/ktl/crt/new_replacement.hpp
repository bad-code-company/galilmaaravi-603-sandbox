#ifndef CRT_NEW_REPLACEMENT_HPP
#define CRT_NEW_REPLACEMENT_HPP

#ifndef DEFAULT_POOL
#	define DEFAULT_POOL NonPagedPool
#endif
#ifndef DRIVER_TAG
#	define DRIVER_TAG 'KTL'
#endif

#include <ntddk.h>

/// Custom new and new[]
void *__cdecl operator new(size_t size, POOL_TYPE pool, ULONG tag = DRIVER_TAG);
void *__cdecl operator new[](size_t size, POOL_TYPE pool, ULONG tag = DRIVER_TAG);
/// Overriding original new and new[]
void *__cdecl operator new(size_t size);
void *__cdecl operator new[](size_t size);
/// Overriding original delete and delete[]
void __cdecl operator delete(void *p, size_t size);
void __cdecl operator delete[](void *p);

#endif // CRT_NEW_REPLACEMENT_HPP