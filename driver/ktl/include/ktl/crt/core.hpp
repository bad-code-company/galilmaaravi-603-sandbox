#ifndef CRT_CORE_HPP
#define CRT_CORE_HPP

/// Credits: https://wiki.osdev.org/Visual_C%2B%2B_Runtime

#include "new_replacement.hpp"

namespace sandbox::ktl::crt {
	class core {
	public:
		/**
		 * @brief Inits C/C++ runtime
		 */
		static void init();
		/**
		 * @brief Deinits C/C++ runtime
		 */
		static void deinit();
	};
} // namespace sandbox::ktl::crt

#endif // CRT_CORE_HPP
