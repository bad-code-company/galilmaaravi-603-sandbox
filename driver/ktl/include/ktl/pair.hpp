#ifndef PAIR_HPP
#define PAIR_HPP

#include "utility.hpp"

namespace sandbox::ktl {
	template<typename T1, typename T2>
	class pair {
	public:
		/**
		 * @brief Default constructor
		 */
		pair() = default;

		/**
		 * @brief Constructor, sets values
		 * @param first First value
		 * @param second Second value
		 */
		pair(const T1 &first, const T2 &second)
			: first_(first), second_(second) {}

		/**
		 * @brief Copies pair from other
		 * @param other Pair to copy from
		 */
		pair(const pair &other)
			: first_(other.first_), second_(other.second_) {}

		/**
		 * @brief Moves pair from other
		 * @param other Pair to move from
		 */
		pair(pair &&other)
			: first_(ktl::move(other.first_)), second_(ktl::move(other.second_)) {}

		/**
		 * @brief Copies pair from other
		 * @param other Pair to copy from
		 * @return This pair object
		 */
		pair &operator=(const pair &other) {
			first_	= other.first_;
			second_ = other.second_;
			return *this;
		}

		/**
		 * @brief Moves pair from other
		 * @param other Pair to move from
		 * @return This pair object
		 */
		pair &operator=(pair &&other) {
			first_	= ktl::move(other.first_);
			second_ = ktl::move(other.second_);
			return *this;
		}

		/// Fields
		T1 first_;
		T2 second_;
	};

	/**
	 * @brief Makes pair and returns it
	 * @tparam T1 First type
	 * @tparam T2 Second type
	 * @param first First value
	 * @param second Second value
	 * @return Pair of T1 and T2
	 */
	template<typename T1, typename T2>
	pair<T1, T2> make_pair(T1 &&first, T2 &&second) {
		return pair<T1, T2>(ktl::forward<T1>(first), ktl::forward<T2>(second));
	}
} // namespace sandbox::ktl

#endif // PAIR_HPP