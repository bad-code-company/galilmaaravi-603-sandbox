#ifndef STRING_HPP
#define STRING_HPP

#include "core.hpp"

#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)

namespace sandbox::ktl {
	template<typename T, ULONG Tag = DRIVER_TAG, POOL_TYPE Pool = DEFAULT_POOL>
	class basic_string {
		static_assert(sizeof(T) <= sizeof(wchar_t));

		/// Necessary stuff
		using iterator		  = T *;
		using const_iterator  = const T *;
		using reference		  = T &;
		using const_reference = const T &;

	public:
		/**
		 * @brief Construct class from existing string
		 * @param data Existring string
		 */
		basic_string(const T *data = nullptr) {
			if (data) {
				length_	  = string_length(data);
				capacity_ = MAX(length_ + 1, capacity_);
				data_	  = new T[capacity_];
				memcpy(data_, data, sizeof(T) * length_);
			} else {
				data_ = new T[capacity_];
			}
			data_[length_] = static_cast<T>(0);
		}

		/**
		 * @brief Fill constructor
		 * @param ch Char to fill with the string
		 * @param count Count of filling "ch"
		 */
		basic_string(const T &ch, const size_t count)
			: capacity_(count + 1), length_(count) {
			NT_ASSERT(count >= 0);
			data_ = new T[capacity_];
			for (auto i = 0u; i < length_; ++i)
				data_[i] = ch;
			data_[length_] = static_cast<T>(0);
		}

		/**
		 * @brief Constructs string from unicode string
		 * @param us Unicode string
		 */
		explicit basic_string(PUNICODE_STRING us) {
			static_assert(sizeof(T) == sizeof(wchar_t));
			if (us->Length > 0) {
				NT_ASSERT(us->Buffer);
				length_ = us->Length / sizeof(T);
				data_	= new T[length_];
				memcpy(data_, us->Buffer, us->Length);
			}
		}

		/**
		 * @brief Constructs string from unicode string
		 * @param us Unicode string
		 */
		explicit basic_string(const UNICODE_STRING us)
			: basic_string(&us) {}

		/**
		 * @brief Copy constructor
		 * @param other String to copy from
		 */
		basic_string(const basic_string &other)
			: basic_string() {
			assign(other);
		}

		/**
		 * @brief Move constructor
		 * @param other String to move from
		 */
		basic_string(basic_string &&other)
			: basic_string() {
			assign(ktl::forward<basic_string<T>>(other));
		}

		/*
		 * @brief Cleans the memory
		 */
		~basic_string() {

			if (data_)
				delete[] data_;
		}

		/*
		 * @return String capacity
		 */
		size_t capacity() const {
			return capacity_;
		}

		/*
		 * @return String length
		 */
		size_t length() const {
			return length_;
		}

		/*
		 * @return String size (length * size of type)
		 */
		size_t size() const {
			return sizeof(T) * (length_ + 1);
		}

		/*
		 * @return C string pointer
		 */
		T *data() {
			return data_;
		}

		/*
		 * @return C string pointer (not mutable)
		 */
		const T *c_str() const {
			return data_;
		}

		/**
		 * @return Unicode string (wchar_t only)
		 */
		const UNICODE_STRING unicode_string() const {
			static_assert(sizeof(T) == sizeof(wchar_t));
			UNICODE_STRING us;
			RtlInitUnicodeString(&us, reinterpret_cast<wchar_t *>(data_));
			return us;
		}

		/**
		 * @return Is string empty
		 */
		bool empty() const {
			return length_ == 0;
		}

		/** Data access **/

		/**
		 * @brief Returns value at specific index
		 * @param index Index
		 * @return Value
		 */
		reference at(const size_t index) {
			NT_ASSERT(index < length_);
			return data_[index];
		}

		/**
		 * @brief Returns value at specific index (not mutable)
		 * @param index Index
		 * @return Value
		 */
		const_reference at(const size_t index) const {
			NT_ASSERT(index < length_);
			return data_[index];
		}

		/**
		 * @brief Returns value at specific index
		 * @param index Index
		 * @return Value
		 */
		reference operator[](const size_t index) {
			return at(index);
		}

		/**
		 * @brief Returns value at specific index (not mutable)
		 * @param index Index
		 * @return Value
		 */
		const_reference operator[](const size_t index) const {
			return at(index);
		}

		/**
		 * @return First element of the string
		 */
		reference front() {
			return data_[0];
		}

		/**
		 * @return First element of the string (not mutable)
		 */
		const_reference front() const {
			return data_[0];
		}

		/**
		 * @return Last element of the string
		 */
		reference back() {
			if (length_ == 0)
				return data_[0];
			return data_[length_ - 1];
		}

		/**
		 * @return Last element of the string (not mutable)
		 */
		const_reference back() const {
			if (length_ == 0)
				return data_[0];
			return data_[length_ - 1];
		}

		/**
		 * @return Begin of string pointer
		 */
		iterator begin() {
			return data_;
		}

		/**
		 * @return Begin of string pointer (not mutable)
		 */
		const_iterator begin() const {
			return data_;
		}

		/**
		 * @return End of string pointer
		 */
		iterator end() {
			return data_ + length_;
		}

		/**
		 * @return End of string pointer (not mutable)
		 */
		const_iterator end() const {
			return data_ + length_;
		}

		/** Allocation related **/

		/**
		 * @brief Reserves new memory for vector
		 * @param capacity New minimum capacity
		 */
		void reserve(const size_t capacity) {
			if (capacity_ < capacity) {
				// save old elements addr
				auto old_elements = data_;
				// calc multiplicity factor
				while (capacity_ < capacity) capacity_ += default_capacity / 4;
				// allocate new array
				data_ = new T[capacity_];
				memcpy(data_, old_elements, sizeof(T) * (length_ + 1));
				// Clear old memory
				delete[] old_elements;
			}
		}

		/**
		 * @brief Resizes vector
		 * @param size New vector size
		 */
		void resize(const size_t length) {
			// reserve new memory if size is bigger than capacity
			if (capacity_ < length)
				reserve(length);
			length_ = length;
		}

		/**
		 * @brief Reinitializes String
		 */
		void clear() {
			delete[] data_;
			// Allocate new memory
			data_	 = new T[default_capacity];
			data_[0] = static_cast<T>(0);
			length_	 = 0;
		}

		/** Data assigning **/

		/**
		 * @brief Pushes new char to string
		 * @param ch Char to push
		 */
		void push_back(const T &ch) {
			NT_ASSERT(length_ <= capacity_);
			if (capacity_ <= ++length_)
				reserve(length_);
			data_[length_ - 1] = ch;
			data_[length_]	   = 0;
		}

		/**
		 * @brief Pushes new c string to string
		 * @param c_str C string to push
		 */
		void push_back(const T *c_str) {
			NT_ASSERT(length_ <= capacity_);
			NT_ASSERT(c_str != nullptr);
			auto c_str_length = string_length(c_str);
			if (capacity_ <= length_ + c_str_length) {
				reserve(length_ + c_str_length);
			}
			memcpy(data_ + length_, c_str, sizeof(T) * c_str_length);
			data_[length_ += c_str_length] = 0;
		}

		/**
		 * @brief Pushes new string to string
		 * @param other String to push
		 */
		void push_back(const basic_string &other) {
			push_back(other.data_);
		}

		/**
		 * @brief Pushes new string to string
		 * @param other String to push
		 */
		void push_back(basic_string &&other) {
			push_back(other.data_);
			other.clear();
		}

		/**
		 * @brief Removes last value of string
		 */
		void pop_back() {
			NT_ASSERT(length_ > 0);
			data_[--length_].~T();
		}

		/**
		 * @brief Assigns string to char 'ch' count times
		 * @param ch Char to assign as
		 * @param count Count of times to set char
		 */
		void assign(const T &ch, const size_t count) {
			resize(count);
			for (auto i = 0u; i < length_; ++i)
				data_[i] = ch;
		}

		/**
		 * @brief Assigns string to specific string
		 * @param c_str C string
		 */
		void assign(const T *c_str) {
			NT_ASSERT(c_str != nullptr);
			auto c_str_length = string_length(c_str);
			if (capacity_ <= c_str_length)
				resize(c_str_length);
			memcpy(data_, c_str, sizeof(T) * c_str_length);
			data_[length_ = c_str_length] = 0;
		}

		/**
		 * @brief Assigns string to specific string
		 * @param other String
		 */
		void assign(const basic_string &other) {
			if (this != &other)
				assign(other.data_);
		}

		/**
		 * @brief Assigns string to specific string
		 * @param other String
		 */
		void assign(basic_string &&other) {
			if (this != &other) {
				if (data_)
					delete[] data_;
				capacity_ = other.capacity_;
				length_	  = other.length_;
				data_	  = other.data_;

				other.capacity_ = default_capacity;
				other.length_	= 0;
				other.data_		= new T[other.capacity_];
			}
		}

		/**
		 * @brief Erases element on spefic position
		 * @param pos Position to remove from
		 * @return Next element on string
		 */
		iterator erase(const_iterator pos) {
			NT_ASSERT(!empty());
			// Calculate dist from the begin
			const size_t dist = pos - data_;
			// Move elements
			for (auto i = dist; i < length_ - 1; ++i) {
				data_[i] = data_[i + 1];
			}
			--length_;
			// If it was not last elements return iterator at this place
			if (dist < length_)
				return &data_[dist];
			return &data_[length_ - 1];
		}

		/** Compare methods **/

		/**
		 * @brief Compares string with another
		 * @param c_str C string to compare with
		 * @return 0 when strings are same
		 */
		int compare(const T *c_str) {
			NT_ASSERT(c_str != nullptr);
			auto i = 0u;
			while (data_[i] && c_str[i] && data_[i] == c_str[i])
				i++;
			return data_[i] - c_str[i];
		}

		/**
		 * @brief Compares string with another
		 * @param other String to compare with
		 * @return 0 when strings are same
		 */
		int compare(const basic_string &other) {
			return compare(other.data_);
		}

		/**
		 * @brief Compares string with another
		 * @param other String to compare with
		 * @return 0 when strings are same
		 */
		int compare(basic_string &&other) {
			return compare(other.data_);
		}

		/** Operators **/

		/// Assign operator
		basic_string &operator=(const T &ch) {
			assign(ch, 1);
			return *this;
		}

		basic_string &operator=(const T *c_str) {
			assign(c_str);
			return *this;
		}

		basic_string &operator=(const basic_string &other) {
			assign(other);
			return *this;
		}

		basic_string &operator=(basic_string &&other) {
			assign(ktl::forward<basic_string<T>>(other));
			return *this;
		}

		/// Add operator
		basic_string &operator+=(const T &ch) {
			push_back(ch);
			return *this;
		}

		basic_string &operator+=(const T *c_str) {
			push_back(c_str);
			return *this;
		}

		basic_string &operator+=(const basic_string &other) {
			push_back(other);
			return *this;
		}

		basic_string &operator+=(basic_string &&other) {
			push_back(ktl::forward<basic_string<T>>(other));
			return *this;
		}

		/// Compare operators
		bool operator==(const T *c_str) const {
			return compare(c_str) == 0;
		}

		bool operator==(const basic_string &other) const {
			return compare(other) == 0;
		}

		bool operator==(basic_string &&other) const {
			return compare(ktl::forward<basic_string<T>>(other)) == 0;
		}

		bool operator!=(const T *c_str) const {
			return compare(c_str) != 0;
		}

		bool operator!=(const basic_string &other) const {
			return compare(other) != 0;
		}

		bool operator!=(basic_string &&other) const {
			return compare(ktl::forward<basic_string<T>>(other)) != 0;
		}

		bool operator>(const T *c_str) {
			return compare(c_str) > 0;
		}

		bool operator<(const T *c_str) {
			return compare(c_str) < 0;
		}

		bool operator>=(const T *c_str) {
			return compare(c_str) >= 0;
		}

		bool operator<=(const T *c_str) {
			return compare(c_str) <= 0;
		}

		bool operator>(const basic_string &other) {
			return compare(other) > 0;
		}

		bool operator<(const basic_string &other) {
			return compare(other) < 0;
		}

		bool operator>=(const basic_string &other) {
			return compare(other) >= 0;
		}

		bool operator<=(const basic_string &other) {
			return compare(other) <= 0;
		}

		bool operator>(basic_string &&other) {
			return compare(ktl::forward<basic_string<T>>(other)) > 0;
		}

		bool operator<(basic_string && other) {
			return compare(ktl::forward<basic_string<T>>(other)) < 0;
		}

		bool operator>=(basic_string &&other) {
			return compare(ktl::forward<basic_string<T>>(other)) >= 0;
		}

		bool operator<=(basic_string &&other) {
			return compare(ktl::forward<basic_string<T>>(other)) <= 0;
		}

	protected:
		/// Fields
		T	  *data_	 = nullptr;
		size_t capacity_ = default_capacity;
		size_t length_	 = 0;

	private:
		/**
		 * @brief Returns length of given c string
		 * @param c_str C string
		 * @return C string length
		 */
		static size_t string_length(const T *c_str) {
			if (sizeof(T) == sizeof(wchar_t))
				return wcslen(reinterpret_cast<const wchar_t *>(c_str));
			return strlen(reinterpret_cast<const char *>(c_str));
		}

		static constexpr auto default_capacity = 32u;
	};

	using string  = basic_string<char>;
	using wstring = basic_string<wchar_t>;
} // namespace sandbox::ktl

#undef MAX
#undef MIN

#endif // STRING_HPP
