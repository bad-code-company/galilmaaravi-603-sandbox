#ifndef MEMORY_HPP
#define MEMORY_HPP

#include "core.hpp"

namespace sandbox::ktl {
	class memory {
	public:
		/**
		 * @brief Copies memory from source to destination in special memory area
		 * @param dst Destination
		 * @param src Source
		 * @param size Size of memory to copy
		 * @return Status of memory copying
		 */
		static NTSTATUS super_memcpy(void *dst, const void *src, size_t size);
	};
} // namespace sandbox::ktl

#endif // MEMORY_HPP