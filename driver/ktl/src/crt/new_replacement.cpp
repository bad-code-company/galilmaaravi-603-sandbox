#include "crt/new_replacement.hpp"

void *__cdecl operator new(size_t size, POOL_TYPE pool, ULONG tag) {
	auto p = ExAllocatePoolWithTag(pool, size, tag);
	SbPrint("Allocating %u bytes from pool %d with tag 0x%X: 0x%p\n", size, pool, tag, p);
	return p;
}

void __cdecl operator delete(void *p, size_t size) {
	NT_ASSERT(p);
	ExFreePool(p);
	SbPrint("Freeing: 0x%p\n", p);
}

void *__cdecl operator new(size_t size) {
	return operator new(size, DEFAULT_POOL);
}

void *__cdecl operator new[](size_t size) {
	return operator new[](size, DEFAULT_POOL);
}

void *__cdecl operator new[](size_t size, POOL_TYPE pool, ULONG tag) {
	return operator new(size, pool, tag);
}

void __cdecl operator delete[](void *p) {
	delete p;
}