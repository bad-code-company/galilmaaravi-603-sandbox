#include "crt/core.hpp"

using _PVFV = void(__cdecl *)();
using _PIFV = int(__cdecl *)();

// Linker puts constructors/terminators between these sections,
// and we use them to locate constructor pointers.
#pragma section(".CRT$XIA", long, read) // C Initializers
#pragma section(".CRT$XIZ", long, read)

#pragma section(".CRT$XCA", long, read) // C++ Initializers
#pragma section(".CRT$XCZ", long, read)

#pragma section(".CRT$XPA", long, read) // C pre-terminators
#pragma section(".CRT$XPZ", long, read)

#pragma section(".CRT$XTA", long, read) // C terminators
#pragma section(".CRT$XTZ", long, read)

// Put .CRT data into .rdata section
#pragma comment(linker, "/merge:.CRT=.rdata")

// Pointers surrounding constructors
__declspec(allocate(".CRT$XIA")) _PIFV __xi_a[] = { nullptr }; // C initializers
__declspec(allocate(".CRT$XIZ")) _PIFV __xi_z[] = { nullptr };

__declspec(allocate(".CRT$XCA")) _PVFV __xc_a[] = { nullptr }; // C++ initializers
__declspec(allocate(".CRT$XCZ")) _PVFV __xc_z[] = { nullptr };

__declspec(allocate(".CRT$XPA")) _PVFV __xp_a[] = { nullptr }; // C pre-terminators
__declspec(allocate(".CRT$XPZ")) _PVFV __xp_z[] = { nullptr };

__declspec(allocate(".CRT$XTA")) _PVFV __xt_a[] = { nullptr }; // C terminators
__declspec(allocate(".CRT$XTZ")) _PVFV __xt_z[] = { nullptr };

// Call C constructors
int _initterm_e(const _PIFV *begin, const _PIFV *end) {
	int ret = 0;

	while (begin < end && ret == 0) {
		if (*begin != 0)
			ret = (**begin)();
		++begin;
	}

	return ret;
}

// Call C++ constructors
void _initterm(const _PVFV *begin, const _PVFV *end) {
	while (begin < end) {
		if (*begin != 0)
			(**begin)();
		++begin;
	}
}

// Class destructors linked list
typedef struct destructor_entry {
	_PVFV			  destructor_ = nullptr;
	destructor_entry *next_		  = nullptr;

	destructor_entry(destructor_entry *next, _PVFV destructor)
		: next_(next), destructor_(destructor) {}

	~destructor_entry() {
		destructor_();
	}
} * destructor_table;

destructor_table dt = nullptr;

// Adds destructor to the list
int __cdecl atexit(const _PVFV function) {
	auto entry = new destructor_entry(dt, function);
	if (entry == nullptr)
		return -1;
	dt = entry;
	return 0;
}

using namespace sandbox::ktl;

void crt::core::init() {
	// Call C constructors
	auto initret = _initterm_e(__xi_a, __xi_z);
	if (initret != 0)
		return;

	// Call C++ constructors
	_initterm(__xc_a, __xc_z);
}

void crt::core::deinit() {
	// Call classes destructors
	for (auto entry = dt; entry;) {
		auto next = entry->next_;
		delete entry;
		entry = next;
	}

	// Call C pre-terminators
	_initterm(__xp_a, __xp_z);

	// Call C terminators
	_initterm(__xt_a, __xt_z);
}