#include "mutex.hpp"

using namespace sandbox::ktl;

void mutex::init() {
	KeInitializeMutex(&mutex_, 0);
}

void mutex::lock() {
	KeWaitForSingleObject(&mutex_, Executive, KernelMode, FALSE, nullptr);
}

void mutex::unlock() {
	KeReleaseMutex(&mutex_, FALSE);
}
