#include "fast_mutex.hpp"

using namespace sandbox::ktl;

void fast_mutex::init() {
	ExInitializeFastMutex(&mutex_);
}

void fast_mutex::lock() {
	ExAcquireFastMutex(&mutex_);
}

void fast_mutex::unlock() {
	ExReleaseFastMutex(&mutex_);
}
