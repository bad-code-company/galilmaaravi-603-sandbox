#include "memory.hpp"

using namespace sandbox::ktl;

NTSTATUS memory::super_memcpy(void *dst, const void *src, size_t size) {
	auto irql = KeRaiseIrqlToDpcLevel();

	auto mdl = IoAllocateMdl(dst, size, 0, 0, nullptr);
	if (mdl == nullptr) {
		KeLowerIrql(irql);
		return STATUS_NO_MEMORY;
	}

	MmBuildMdlForNonPagedPool(mdl);

	// Hack: prevent bugcheck from Driver Verifier and possible future versions of Windows
#pragma prefast(push)
#pragma prefast(disable \
				: __WARNING_MODIFYING_MDL, "Trust me I'm a scientist")
	const auto OriginalMdlFlags = mdl->MdlFlags;
	mdl->MdlFlags |= MDL_PAGES_LOCKED;
	mdl->MdlFlags &= ~MDL_SOURCE_IS_NONPAGED_POOL;

	// Map pages and do the copy
	const auto mapped = MmMapLockedPagesSpecifyCache(mdl, KernelMode, MmCached, nullptr, FALSE, HighPagePriority);
	if (mapped == nullptr) {
		mdl->MdlFlags = OriginalMdlFlags;
		IoFreeMdl(mdl);
		KeLowerIrql(irql);
		return STATUS_NONE_MAPPED;
	}

	RtlCopyMemory(mapped, src, size);

	MmUnmapLockedPages(mapped, mdl);
	mdl->MdlFlags = OriginalMdlFlags;
#pragma prefast(pop)
	IoFreeMdl(mdl);
	KeLowerIrql(irql);

	return STATUS_SUCCESS;
}