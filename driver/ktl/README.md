# Kernel Template Library

Implementation of some parts of STL needed for using in Sandbox project.

### Implemented stuff:

* C Runtime (need to be initialized and deinitialized in DriverEntry and DriverUnload).
* Overridden global operators: new, delete, new[], delete[]. Default pool is **Non Paged Pool**.
* Vector.
* Mutex (using KMUTEX and FAST_MUTEX).
* Lock guard.
* Unique Pointer.
* Function.

### Credits

[Developing Kernel Drivers with Modern C++ - Pavel Yosifovich](https://www.youtube.com/watch?v=AsSMKL5vaXw)