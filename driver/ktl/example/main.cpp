#include <ntddk.h>
#include <ktl/core.hpp>
#include <ktl/unique_ptr.hpp>
#include <ktl/vector.hpp>
#include <ktl/functional.hpp>
#include <ktl/string.hpp>

ktl::unique_ptr<ktl::vector<int>> vector;

DRIVER_UNLOAD DriverUnload;

void DriverUnload(PDRIVER_OBJECT) {
	ktl::core::deinit();

	SbPrint("Driver unloaded.\r\n");
}

extern "C" DRIVER_INITIALIZE DriverEntry;

extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT driver_object, PUNICODE_STRING) {
	driver_object->DriverUnload = DriverUnload;
	ktl::core::init();

	vector = ktl::make_unique<ktl::vector<int>>();

	ktl::function<void(const ktl::vector<int> &)> print = [](const ktl::vector<int> &vector) {
		SbPrint("");
		for (auto &&i : vector) {
			DbgPrint("%d ", i);
		}
		DbgPrint("\r\n");
	};

	vector->push_back(1);
	vector->push_back(2);
	vector->push_back(3);

	print(*vector);

	vector->assign(10, 4);

	print(*vector);

	ktl::vector<int> other_vector = ktl::move(*vector);
	print(*vector);
	print(other_vector);
	other_vector.erase(other_vector.begin());
	print(other_vector);

	ktl::string string = "yes";
	SbPrint("%d %s\r\n", string.length(), string.c_str());
	string += "what?";
	SbPrint("%d %s\r\n", string.length(), string.c_str());
	string = "not what";
	SbPrint("%d %s\r\n", string.length(), string.c_str());
	ktl::string another_string(string);
	SbPrint("%d %s\r\n", string.length(), string.c_str());
	SbPrint("%d %s\r\n", another_string.length(), another_string.c_str());
	ktl::string one_more = ktl::move(string);
	SbPrint("%d %s\r\n", string.length(), string.c_str());
	SbPrint("%d %s\r\n", one_more.length(), one_more.c_str());

	SbPrint("Driver loaded.\r\n");

	return STATUS_SUCCESS;
}