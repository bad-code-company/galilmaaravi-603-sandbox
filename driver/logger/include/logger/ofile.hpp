#ifndef FILE_HPP
#define FILE_HPP

#include "pch.hpp"

namespace sandbox {
	/// output file
	class ofile {
	public:
		/**
		 * @brief Inits output file path and opens the file
		 * @param path Path of file to write
		 */
		explicit ofile(const wchar_t *path) noexcept;
		/**
		 * @brief Closes file
		 */
		~ofile() noexcept;

		/**
		 * @return Is file opened
		 */
		[[nodiscard]] bool is_opened() const;

		/**
		 * @brief Removes the file
		 */
		void remove();

		/**
		 * @brief Opens file
		 * @return Is file opened
		 */
		bool open();
		/**
		 * @brief Closes file
		 */
		void close();

		/**
		 * @brief Writes to file
		 * @param fmt Format of writing
		 * @param ... Writing args
		 * @return Is data written
		 */
		bool write(const char *fmt, ...);

	private:
		/// Fields
		UNICODE_STRING path_;

		HANDLE			file_handle_ = nullptr;
		IO_STATUS_BLOCK io_status_block_;
	};
} // namespace sandbox

#endif // FILE_HPP