#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <ktl/unique_ptr.hpp>
#include <ktl/mutex.hpp>
#include "ofile.hpp"

namespace sandbox {
	class logger {
	public:
		/// Path of file to write to
		static constexpr auto log_file_path = L"\\??\\C:\\sandbox.log";

		/**
		 * @brief Initializes logger (native create and write file function)
		 * @return Was initialization successful
		 */
		static bool init();
		/**
		 * @brief Deinitliazes the logger class, closes the file
		 */
		static void deinit();

		/**
		 * @brief Logs function call to C:\sandbox.log
		 * @param pid Process id of process that calls syscall
		 * @param function_name Name of function to log
		 * @param return_value Return value of function
		 * @param fmt Arguments format
		 * @param ... Arguments of function
		 * @return Was log line added
		 */
		static bool log(ULONGLONG pid, const char *function_name, NTSTATUS return_value, const char *fmt, ...);

	private:
		/// Fields
		static ofile	  log_file_;
		static ktl::mutex mtx_;
	};
} // namespace sandbox

#endif // LOGGER_HPP
