#include "ofile.hpp"
#include <ntstrsafe.h>

using namespace sandbox;

ofile::ofile(const wchar_t *path) noexcept {
	RtlInitUnicodeString(&path_, path);
	RtlZeroMemory(&io_status_block_, sizeof(IO_STATUS_BLOCK));
}

ofile::~ofile() noexcept {
	close();
}

bool ofile::is_opened() const {
	return file_handle_ != nullptr;
}

void ofile::remove() {
	if (is_opened())
		close();

	// init file name
	OBJECT_ATTRIBUTES obj_attributes;
	InitializeObjectAttributes(&obj_attributes, &path_, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, nullptr, nullptr);
	ZwDeleteFile(&obj_attributes);
}

bool ofile::open() {
	// init file name
	OBJECT_ATTRIBUTES obj_attributes;
	InitializeObjectAttributes(&obj_attributes, &path_, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, nullptr, nullptr);

	if (KeGetCurrentIrql() != PASSIVE_LEVEL) {
		SbPrint("KeGetCurrentIrql != PASSIVE_LEVEL!\r\n");
		return false;
	}

	auto status = ZwCreateFile(&file_handle_, FILE_APPEND_DATA, &obj_attributes, &io_status_block_, nullptr, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_WRITE | FILE_SHARE_READ, FILE_OPEN_IF, FILE_SYNCHRONOUS_IO_NONALERT, nullptr, 0);
	if (!NT_SUCCESS(status)) {
		SbPrint("0x%X Failed during openning file %wZ.\r\n", status, path_);
	}
	return NT_SUCCESS(status);
}

void ofile::close() {
	if (is_opened()) {
		ZwClose(file_handle_);
		file_handle_ = nullptr;
	}
	RtlZeroMemory(&io_status_block_, sizeof(IO_STATUS_BLOCK));
}

bool ofile::write(const char *fmt, ...) {
	if (!is_opened()) {
		SbPrint("%wZ is not opened.\r\n", path_);
		return false;
	}

	// Format message
	char	args_string[0x1000] = "";
	va_list args;
	va_start(args, fmt);
	auto args_length		 = _vsnprintf(args_string, sizeof(args_string) / sizeof(char), fmt, args);
	args_string[args_length] = '\0';
	va_end(args);

	auto status = ZwWriteFile(file_handle_, nullptr, nullptr, nullptr, &io_status_block_, const_cast<char *>(args_string), args_length, nullptr, nullptr);
	if (NT_SUCCESS(status)) {
		return true;
	}

	SbPrint("0x%X Failed during writing to file %wZ.\r\n", status, path_);
	return false;
}
