#include "logger.hpp"
#include <cstdarg>
#include <ntstrsafe.h>
#include <ktl/lock_guard.hpp>
#include "ofile.hpp"

using namespace sandbox;

ofile	   logger::log_file_(log_file_path);
ktl::mutex logger::mtx_;

bool logger::init() {
	mtx_.init();

	if (log_file_.is_opened()) {
		return true;
	} else {
		log_file_.remove();
	}

	// open file
	return log_file_.open();
}

void logger::deinit() {
	if (!log_file_.is_opened())
		return;

	ktl::lock_guard guard(mtx_);
	log_file_.close();
}

bool logger::log(ULONGLONG pid, const char *function_name, NTSTATUS return_value, const char *fmt, ...) {
	ktl::lock_guard guard(mtx_);

	static const auto size = 0x500;

	// Format begin string
	char begin_str[size]	 = { 0 };
	auto begin_str_len		 = _snprintf(begin_str, size, R"({"pid":%llu,"name":"%s","return_value":%lu,"parameters":)", pid, function_name, return_value);
	begin_str[begin_str_len] = '\0';

	// Format args
	char	args_str[size] = { 0 };
	va_list args;
	va_start(args, fmt);
	auto args_str_len	   = _vsnprintf(args_str, size, fmt, args);
	args_str[args_str_len] = '\0';
	va_end(args);

	// Write new line
	auto ret_val = log_file_.write("%s%s}\r\n", begin_str, args_str);

	return ret_val;
}