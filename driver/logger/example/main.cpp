#include <ntddk.h>
#include <logger/logger.hpp>
#include <ktl/unique_ptr.hpp>

DRIVER_UNLOAD DriverUnload;

void DriverUnload(PDRIVER_OBJECT) {
	SbPrint("Driver unloaded.\r\n");
}

extern "C" DRIVER_INITIALIZE DriverEntry;

extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT driver_object, PUNICODE_STRING) {
	driver_object->DriverUnload = DriverUnload;

	sandbox::logger::instance()->log(0, "FUNCTION_NAME", STATUS_SUCCESS, "%s %s %s", "arg1", "arg2", "arg3");

	SbPrint("Driver loaded.\n");
	return STATUS_SUCCESS;
}
