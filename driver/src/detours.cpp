#include "detours.hpp"
#include <ntstrsafe.h>
#include <logger/logger.hpp>
#include <ktl/string.hpp>

using namespace sandbox;

ktl::vector<ULONGLONG> detours::pids_;
bool				   detours::on_log_ = true;

NTCREATEFILE detours::pNtCreateFile = nullptr;

NTCREATEPROCESS	  detours::pNtCreateProcess = nullptr;
NTCREATEPROCESSEX detours::pNtCreateProcessEx;

void detours::init() {
	logger::init();
}

void detours::deinit() {
	// Terminate all process
	HANDLE			  proc_handle = nullptr;
	OBJECT_ATTRIBUTES obj_attr;
	CLIENT_ID		  cid;
	for (auto &&pid : pids_) {
		InitializeObjectAttributes(&obj_attr, nullptr, 0, nullptr, nullptr);
		cid.UniqueProcess = reinterpret_cast<HANDLE>(pid);
		cid.UniqueThread  = static_cast<HANDLE>(nullptr);
		auto status		  = ZwOpenProcess(&proc_handle, GENERIC_ALL, &obj_attr, &cid);
		if (NT_SUCCESS(status) && proc_handle != nullptr) {
			ZwTerminateProcess(proc_handle, 0);
			proc_handle = nullptr;
			SbPrint("[PID: %d] Terminated.\r\n", pid);
		}
	}
	// Reset pid
	pids_.clear();
	// Init logger
	logger::deinit();
}

void detours::add_pid(ULONGLONG process_id) {
	pids_.push_back(process_id);
}

template<typename... Args>
void detours::log(ULONGLONG pid, const char *function_name, NTSTATUS status, const char *fmt, Args &&...args) {
	if (on_log_) {
		on_log_ = false;
		logger::log(pid, function_name, status, fmt, ktl::forward<Args>(args)...);
		on_log_ = true;
	}
}

NTSTATUS NTAPI detours::HookedNtCreateFile(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes, ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer, ULONG EaLength) {
	auto status = pNtCreateFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, AllocationSize, FileAttributes, ShareAccess, CreateDisposition, CreateOptions, EaBuffer, EaLength);
	auto pid	= reinterpret_cast<ULONGLONG>(PsGetCurrentProcessId());

	if (pids_.empty() || !pids_.contain(pid)) return status;

	if (!ObjectAttributes || !ObjectAttributes->ObjectName) return status;
	SbPrint("HOOKED NtCreateFile %wZ 0x%X 0x%X 0x%X\r\n", ObjectAttributes->ObjectName, ShareAccess, CreateDisposition, CreateOptions);
	log(pid, "NtCreateFile", status, R"({"file_name":"%wZ","share_access":%lu,"create_disposition":%lu,"create_options":%lu})", ObjectAttributes->ObjectName, ShareAccess, CreateDisposition, CreateOptions);

	return status;
}

NTSTATUS NTAPI detours::HookedNtCreateProcess(PHANDLE ProcessHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, HANDLE ParentProcess, BOOLEAN InheritObjectTable, HANDLE SectionHandle, HANDLE DebugPort, HANDLE ExceptionPort) {
	auto status = pNtCreateProcess(ProcessHandle, DesiredAccess, ObjectAttributes, ParentProcess, InheritObjectTable, SectionHandle, DebugPort, ExceptionPort);
	auto pid	= reinterpret_cast<ULONGLONG>(PsGetCurrentProcessId());

	if (pids_.empty() || !pids_.contain(pid)) return status;

	if (!ProcessHandle) return status;
	PEPROCESS proc;
	auto	  proc_status = ObReferenceObjectByHandle(*ProcessHandle, DesiredAccess, *PsProcessType, KernelMode, reinterpret_cast<void **>(&proc), nullptr);
	if (!NT_SUCCESS(proc_status)) return status;

	auto new_pid = reinterpret_cast<ULONGLONG>(PsGetProcessId(proc));
	add_pid(new_pid);

	if (!ObjectAttributes || !ObjectAttributes->ObjectName) return status;
	SbPrint("HOOKED NtCreateProcessEx %llu->%llu %wZ 0x%X\r\n", pid, new_pid, ObjectAttributes->ObjectName, DesiredAccess);
	log(pid, "NtCreateProcessEx", status, R"({"new_pid":%llu,"process_path":"%wZ","desired_access":%lu})", new_pid, ObjectAttributes->ObjectName, DesiredAccess);

	return status;
}

NTSTATUS NTAPI detours::HookedNtCreateProcessEx(PHANDLE ProcessHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, HANDLE ParentProcess, ULONG Flags, HANDLE SectionHandle, HANDLE DebugPort, HANDLE ExceptionPort, BOOLEAN InJob) {
	auto status = pNtCreateProcessEx(ProcessHandle, DesiredAccess, ObjectAttributes, ParentProcess, Flags, SectionHandle, DebugPort, ExceptionPort, InJob);
	auto pid	= reinterpret_cast<ULONGLONG>(PsGetCurrentProcessId());

	if (pids_.empty() || !pids_.contain(pid)) return status;

	if (!ProcessHandle) return status;
	PEPROCESS proc;
	auto	  proc_status = ObReferenceObjectByHandle(*ProcessHandle, DesiredAccess, *PsProcessType, KernelMode, reinterpret_cast<void **>(&proc), nullptr);
	if (!NT_SUCCESS(proc_status)) return status;

	auto new_pid = reinterpret_cast<ULONGLONG>(PsGetProcessId(proc));
	add_pid(new_pid);

	if (!ObjectAttributes || !ObjectAttributes->ObjectName) return status;
	SbPrint("HOOKED NtCreateProcessEx %llu->%llu %wZ 0x%X 0x%X\r\n", pid, new_pid, ObjectAttributes->ObjectName, DesiredAccess, Flags);
	log(pid, "NtCreateProcessEx", status, R"({"new_pid":%llu,"process_path":"%wZ","desired_access":%lu,"flags"%lu:})", new_pid, ObjectAttributes->ObjectName, DesiredAccess, Flags);

	return status;
}