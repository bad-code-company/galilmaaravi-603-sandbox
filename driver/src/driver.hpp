#ifndef DRIVER_HPP
#define DRIVER_HPP

#include <ntddk.h>
#include <ioctl/ioctl.hpp>
#include "hooks.hpp"

namespace sandbox {
	class driver {
	public:
		/**
		 * @brief Initializes the driver
		 * @param driver_object Driver object ptr
		 */
		driver(PDRIVER_OBJECT driver_object);

	private:
		/// Fields
		PDRIVER_OBJECT driver_object_ = nullptr;

		hooks hooks_;
		ioctl ioctl_;

		/**
		 * @brief Callback of starting logging of ioctl
		 * @param start Is to start logging
		 * @param pid PID of app
		 */
		void ioctl_callback(bool start, long pid);
	};
} // namespace sandbox

#endif // DRIVER_HPP