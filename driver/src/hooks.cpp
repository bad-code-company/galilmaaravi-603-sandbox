#include "hooks.hpp"
#include "detours.hpp"

using namespace sandbox;

hooks::hooks(PDRIVER_OBJECT driver_object)
	: driver_object_(driver_object), ssdt_(driver_object_) {
	init();
}

hooks::~hooks() {
	deinit();
}

void hooks::init() {
	// Init detours
	detours::init();

	// Hook all functions

	// NtCreateFile
	auto hook_ptr = ssdt_.hook("NtCreateFile", reinterpret_cast<void *>(&detours::HookedNtCreateFile), reinterpret_cast<void **>(&detours::pNtCreateFile));
	hooks_.push_back(ktl::move(hook_ptr));

	// NtCreateProcess
	hook_ptr = ssdt_.hook("NtCreateProcess", reinterpret_cast<void *>(&detours::HookedNtCreateProcess), reinterpret_cast<void **>(&detours::pNtCreateProcess));
	hooks_.push_back(ktl::move(hook_ptr));

	// NtCreateProcessEx
	hook_ptr = ssdt_.hook("NtCreateProcessEx", reinterpret_cast<void *>(&detours::HookedNtCreateProcessEx), reinterpret_cast<void **>(&detours::pNtCreateProcessEx));
	hooks_.push_back(ktl::move(hook_ptr));
}

void hooks::deinit() {
	detours::deinit();
	// Unhook all entries
	for (auto &&p : hooks_) {
		ssdt_.unhook(p);
	}
	hooks_.clear();
}

void hooks::start_logging(const long pid) const {
	detours::add_pid(pid);
}

void hooks::stop_logging() const {
	detours::deinit();
}
