#ifndef HOOKS_HPP
#define HOOKS_HPP

#include <ktl/vector.hpp>
#include <ssdt/ssdt.hpp>

namespace sandbox {
	class hooks {
	public:
		/**
		 * @brief Inits the hooks library, sets hooks
		 * @param driver_object Driver itself obj
		 */
		hooks(PDRIVER_OBJECT driver_object);
		/**
		 * @brief Deinits hooks library, remove hooks
		 */
		~hooks();

		/**
		 * @brief Inits the hooks library, sets hooks
		 */
		void init();
		/**
		 * @brief Deinits hooks library, remove hooks
		 */
		void deinit();

		/**
		 * @brief Start logging on specific pid
		 * @param pid Process id
		 */
		void start_logging(const long pid) const;
		/**
		 * @brief Stops logging
		 */
		void stop_logging() const;

	private:
		/// Fields
		PDRIVER_OBJECT driver_object_ = nullptr;

		ssdt					   ssdt_;
		ktl::vector<details::hook> hooks_;
	};
} // namespace sandbox

#endif // HOOKS_HPP