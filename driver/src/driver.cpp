#include "driver.hpp"
#include <logger/logger.hpp>

using namespace sandbox;

driver::driver(PDRIVER_OBJECT driver_object)
	: driver_object_(driver_object), hooks_(driver_object_), ioctl_(driver_object_, ktl::make_pair(this, &driver::ioctl_callback)) {}

void driver::ioctl_callback(bool start, long pid) {
	if (start) {
		SbPrint("Starting logging on [PID: %d].\r\n", pid);
		hooks_.start_logging(pid);
	} else {
		SbPrint("Finishing logging.\r\n");
		hooks_.stop_logging();
	}
}