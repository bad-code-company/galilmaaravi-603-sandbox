#ifndef DETOURS_HPP
#define DETOURS_HPP

#include <ntddk.h>
#include <ktl/vector.hpp>

using NTCREATEFILE = NTSTATUS(NTAPI *)(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes, ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer, ULONG EaLength);

using NTCREATEPROCESS	= NTSTATUS(NTAPI *)(PHANDLE ProcessHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, HANDLE ParentProcess, BOOLEAN InheritObjectTable, HANDLE SectionHandle, HANDLE DebugPort, HANDLE ExceptionPort);
using NTCREATEPROCESSEX = NTSTATUS(NTAPI *)(PHANDLE ProcessHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, HANDLE ParentProcess, ULONG Flags, HANDLE SectionHandle, HANDLE DebugPort, HANDLE ExceptionPort, BOOLEAN InJob);

namespace sandbox {
	class detours {
	public:
		/**
		 * @brief Inits detours
		 */
		static void init();
		/**
		 * @brief Deinits detours obj (logger and reset PID)
		 */
		static void deinit();

		/**
		 * @brief Adds process id to logging
		 * @param process_id
		 */
		static void add_pid(ULONGLONG process_id);

		/// NtCreateFile
		static NTCREATEFILE	  pNtCreateFile;
		static NTSTATUS NTAPI HookedNtCreateFile(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes, ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer, ULONG EaLength);

		// NtCreateProcess
		static NTCREATEPROCESS pNtCreateProcess;
		static NTSTATUS NTAPI  HookedNtCreateProcess(PHANDLE ProcessHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, HANDLE ParentProcess, BOOLEAN InheritObjectTable, HANDLE SectionHandle, HANDLE DebugPort, HANDLE ExceptionPort);

		// NtCreateProcessEx
		static NTCREATEPROCESSEX pNtCreateProcessEx;
		static NTSTATUS NTAPI HookedNtCreateProcessEx(PHANDLE ProcessHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, HANDLE ParentProcess, ULONG Flags, HANDLE SectionHandle, HANDLE DebugPort, HANDLE ExceptionPort, BOOLEAN InJob);

	private:
		static ktl::vector<ULONGLONG> pids_;

		static bool on_log_;

		template<typename... Args>
		static void log(ULONGLONG pid, const char *function_name, NTSTATUS status, const char *fmt, Args &&...args);
	};
} // namespace sandbox

#endif // DETOURS_HPP