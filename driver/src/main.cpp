#include <ntddk.h>
#include <ktl/core.hpp>
#include "driver.hpp"

sandbox::ktl::unique_ptr<sandbox::driver> driver;

DRIVER_UNLOAD DriverUnload;

void DriverUnload(PDRIVER_OBJECT) {
	sandbox::ktl::core::deinit();
	SbPrint("driver unloaded.\r\n");
}

extern "C" DRIVER_INITIALIZE DriverEntry;

extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT driver_object, PUNICODE_STRING) {
	driver_object->DriverUnload = DriverUnload;
	sandbox::ktl::core::init();

	driver = sandbox::ktl::make_unique<sandbox::driver>(driver_object);

	SbPrint("driver loaded.\r\n");
	return STATUS_SUCCESS;
}