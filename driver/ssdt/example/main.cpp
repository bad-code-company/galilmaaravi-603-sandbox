#include <ntddk.h>
#include <ktl/core.hpp>
#include <ssdt/ssdt.hpp>

typedef struct _PS_ATTRIBUTE {
	ULONG_PTR Attribute;
	SIZE_T	  Size;

	union {
		ULONG_PTR Value;
		PVOID	  ValuePtr;
	};

	PSIZE_T ReturnLength;
} PS_ATTRIBUTE, *PPS_ATTRIBUTE;

typedef struct _PS_ATTRIBUTE_LIST {
	SIZE_T		 TotalLength;
	PS_ATTRIBUTE Attributes[1];
} PS_ATTRIBUTE_LIST, *PPS_ATTRIBUTE_LIST;

using PUSER_THREAD_START_ROUTINE = NTSTATUS(NTAPI *)(PVOID ThreadParameter);

extern "C" char *PsGetProcessImageFileName(PEPROCESS p);

using NTCREATETHREADEX = NTSTATUS(NTAPI *)(PHANDLE ThreadHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, HANDLE ProcessHandle, PUSER_THREAD_START_ROUTINE StartRoutine, PVOID Argument, ULONG CreateFlags, SIZE_T ZeroBits, SIZE_T StackSize, SIZE_T MaximumStackSize, PPS_ATTRIBUTE_LIST AttributeList);

ktl::unique_ptr<sandbox::ssdt> ssdt;

NTCREATETHREADEX	   pNtCreateThreadEx	 = nullptr;
sandbox::details::hook create_thread_ex_hook = nullptr;

NTSTATUS NTAPI hooked_create_thread_ex(PHANDLE ThreadHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, HANDLE ProcessHandle, PUSER_THREAD_START_ROUTINE StartRoutine, PVOID Argument, ULONG CreateFlags, SIZE_T ZeroBits, SIZE_T StackSize, SIZE_T MaximumStackSize, PPS_ATTRIBUTE_LIST AttributeList) {
	static constexpr auto loadlibrarya_signature = "\x48\xFF\x25\xD1\x9C\x05\x00";
	static constexpr auto loadlibraryw_signature = "\x48\xFF\x25\x91\xA5\x05\x00";
	static constexpr auto signature_size		 = sizeof(loadlibrarya_signature);

	PEPROCESS target_proc;
	ObReferenceObjectByHandle(ProcessHandle, 0, *PsProcessType, KernelMode, reinterpret_cast<void **>(&target_proc), nullptr);

	auto is_remote = PsGetCurrentProcess() != target_proc;
	if (is_remote) {
		auto from_name = PsGetProcessImageFileName(PsGetCurrentProcess());
		auto from_id   = PsGetCurrentProcessId();
		auto to_name   = PsGetProcessImageFileName(target_proc);
		auto to_id	   = PsGetProcessId(target_proc);

		if (StartRoutine != nullptr) {
			if (!memcmp(StartRoutine, loadlibrarya_signature, signature_size))
				DbgPrintEx(0, 0, "[LoadLibraryA SUSPICION] ");
			else if (!memcmp(StartRoutine, loadlibraryw_signature, signature_size))
				DbgPrintEx(0, 0, "[LoadLibraryW SUSPICION] ");
		}

		DbgPrintEx(0, 0, "CreateRemoteThreadEx - %s [PID: %d] -> %s [PID: %d]\r\n", from_name, from_id, to_name, to_id);
	}

	return pNtCreateThreadEx(ThreadHandle, DesiredAccess, ObjectAttributes, ProcessHandle, StartRoutine, Argument, CreateFlags, ZeroBits, StackSize, MaximumStackSize, AttributeList);
}

DRIVER_UNLOAD DriverUnload;

void DriverUnload(PDRIVER_OBJECT) {
	if (create_thread_ex_hook)
		ssdt->unhook(create_thread_ex_hook);

	ktl::core::deinit();
	SbPrint("driver unloaded.\r\n");
}

extern "C" DRIVER_INITIALIZE DriverEntry;

extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT driver_object, PUNICODE_STRING) {
	driver_object->DriverUnload = DriverUnload;
	ktl::core::init();

	// init ssdt
	ssdt = ktl::make_unique<sandbox::ssdt>(driver_object);

	// Hook file creation function
	create_thread_ex_hook = ssdt->hook("NtCreateThreadEx", hooked_create_thread_ex, reinterpret_cast<void **>(&pNtCreateThreadEx));
	if (!create_thread_ex_hook || !pNtCreateThreadEx) {
		SbPrint("Error during hooking NtCreateThreadEx...\r\n");
	}

	SbPrint("driver successfully loaded.\n");

	return STATUS_SUCCESS;
}
