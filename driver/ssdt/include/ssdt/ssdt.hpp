#ifndef SSDT_HPP
#define SSDT_HPP

#include <ntddk.h>
#include <ktl/unique_ptr.hpp>
#include "kernel.hpp"
#include "ntdll.hpp"
#include "hooklib.hpp"

namespace sandbox {
	namespace details {
		/// SSDT Struct
		struct ssdt_struct {
			LONG *pServiceTable;
			PVOID pCounterTable;
#ifdef _WIN64
			ULONGLONG NumberOfServices;
#else
			ULONG NumberOfServices;
#endif // _WIN64
			PCHAR pArgumentTable;
		};
	} // namespace details

	class ssdt {
	public:
		/**
		 * @brief Initializes the ssdt class, finds ssdt itself
		 * @param driver_object Driver object (for kernel init)
		 */
		ssdt(PDRIVER_OBJECT driver_object);

		/**
		 * @brief Returns ssdt struct
		 * @return ssdt struct
		 */
		inline details::ssdt_struct *ssdt_struct() {
			return ssdt_struct_;
		}

		/**
		 * @brief Initializes the ssdt class, finds ssdt itself
		 * @return Is class initialized
		 */
		bool init();

		/**
		 * @brief Returns SSDT function address by its name
		 * @param function_name Function's name
		 * @return Function's address
		 */
		PVOID get_function_address(const char *function_name) const;

		/**
		 * @brief Hooks native function and replaces it with detour
		 * @param function_name Name of function to hook (for example NtCreateFile)
		 * @param detour Function to replace with the original
		 * @param original Pointer to original function replacer
		 * @return Hook struct
		 */
		[[nodiscard]] details::hook hook(const char *function_name, void *detour, void **original = nullptr);
		/**
		 * @brief Unhook the function and clears the memory
		 * @param hook Hook struct of hook to unhook
		 */
		void unhook(details::hook hook);

	private:
		/**
		 * @brief Finds ssdt table
		 * @return Was table found
		 */
		bool find_ssdt();

		/// fields
		PDRIVER_OBJECT driver_object_ = nullptr;

		struct details::ssdt_struct *ssdt_struct_ = nullptr;

		kernel kernel_;
		ntdll  ntdll_;
	};
} // namespace sandbox

#endif // SSDT_HPP