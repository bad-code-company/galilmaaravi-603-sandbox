#ifndef PE_HPP
#define PE_HPP

#include <ntdef.h>

namespace sandbox {
	class pe {
	public:
		/// Error value for related functions
		static constexpr auto error_value = -1ul;

		/**
		 * @brief Finds and returns export offset by export name
		 * @param file_data File to find in it export offset
		 * @param file_size File's size
		 * @param export_name Name of export to find
		 * @return Returns export's offset
		 */
		static ULONG_PTR get_export_offset(const UINT8 *file_data, ULONG file_size, const char *export_name);

		/**
		 * @brief Returns the page base by header and ptr
		 * @param header Header of page base
		 * @param size Size (to return)
		 * @param ptr Pointer of page base
		 * @return Page base addr
		 */
		static PVOID get_page_base(UINT8 *header, PULONG size, UINT8 *ptr);
	};
} // namespace sandbox

#endif // PE_HPP