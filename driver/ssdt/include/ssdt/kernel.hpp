#ifndef KERNEL_HPP
#define KERNEL_HPP

#include <ntddk.h>

namespace sandbox {
	class kernel {
	public:
		/**
		 * @brief Initliazes the kernel class, finds base and size
		 * @param driver_object The running driver object
		 */
		kernel(PDRIVER_OBJECT driver_object);
		~kernel() = default;

		/**
		 * @brief Returns kernel base
		 * @return Kernel base
		 */
		inline ULONG_PTR base() {
			return base_;
		}

		/**
		 * @brief Returns kernel image size
		 * @return Kernel image size
		 */
		inline ULONG size() {
			return size_;
		}

		/**
		 * @brief Initializes the kernel class, finds base and size
		 * @param driver_object The running driver object
		 * @return Was class inited
		 */
		bool init();

	private:
		/// fields
		PDRIVER_OBJECT driver_object_ = nullptr;

		ULONG_PTR base_ = 0;
		ULONG	  size_ = 0;
	};
} // namespace sandbox

#endif // KERNEL_HPP