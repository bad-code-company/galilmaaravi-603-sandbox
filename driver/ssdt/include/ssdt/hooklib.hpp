#ifndef HOOKLIB_HPP
#define HOOKLIB_HPP

#include <ntddk.h>

namespace sandbox {
	namespace details {
#pragma pack(push, 1)

		struct hook_opcodes {
#ifdef _WIN64
			UINT16 mov;
#else
			UINT8 mov;
#endif // _WIN64
			ULONG_PTR addr;
			UINT8	  push;
			UINT8	  ret;
		};

#pragma pack(pop)

		using hook = struct hook_struct {
			ULONG_PTR	 addr;
			hook_opcodes hook_ops;
			UINT8		 orig[sizeof(hook_opcodes)];
			// SSDT extension
			int		  ssdt_index;
			LONG	  ssdt_old;
			LONG	  ssdt_new;
			ULONG_PTR ssdt_addr;
		} *;
	} // namespace details

	class hooklib {
	public:
		/**
		 * @brief Hooks original and replaces it with new function
		 * @param original Original addr to hook
		 * @param new_function Detour to replace with
		 * @return Hook struct
		 */
		[[nodiscard]] static details::hook hook(PVOID original, PVOID new_function);
		/**
		 * @brief Unhooks memory
		 * @param hook Hook struct to unhook
		 * @param free Free memory or not
		 */
		static void unhook(details::hook hook, bool free = true);
	};
} // namespace sandbox

#endif // HOOKLIB_HPP