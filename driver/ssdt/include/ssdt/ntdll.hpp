#ifndef NTDLL_HPP
#define NTDLL_HPP

#include <ntdef.h>

namespace sandbox {
	class ntdll {
	public:
		/**
		 * @brief Initializes the ntdll class, reads the ntdll.dll file
		 */
		ntdll();
		/**
		 * @brief Deinitliazes the ntdll class, clears the memory
		 */
		~ntdll();

		/**
		 * @return Is class initialized
		 */
		inline bool inited() const {
			return file_data_ != nullptr;
		}

		/**
		 * @brief Initializes the ntdll class, reads the ntdll.dll file
		 * @return Is class initialized
		 */
		bool init();
		/**
		 * @brief Deinitializes the ntdll class, clears the memory
		 */
		void deinit();

		/**
		 * @brief Returns function's index of SSDT
		 * @param function_name Name of the function
		 * @return Function's index of SSDT
		 */
		int get_function_ssdt_index(const char *function_name) const;

	private:
		/// fields
		UINT8 *file_data_ = nullptr;
		ULONG  file_size_ = 0;
	};
} // namespace sandbox

#endif // NTDLL_HPP