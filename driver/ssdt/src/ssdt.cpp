#include "ssdt.hpp"
#include <ntimage.h>
#include <ktl/memory.hpp>
#include "hooklib.hpp"
#include "kernel.hpp"
#include "ntdll.hpp"
#include "pe.hpp"

using namespace sandbox;

namespace sandbox::details {
	// Needed for finding .text section
	extern "C" __declspec(dllimport) PIMAGE_NT_HEADERS NTAPI RtlImageNtHeader(PVOID Base);

	// Needed for hooking itself
#ifdef _WIN64
	static PVOID find_cave_address(PVOID code_start, ULONG code_size, ULONG cave_size) {
		auto *code = reinterpret_cast<UINT8 *>(code_start);

		for (auto i = 0u, j = 0u; i < code_size; ++i) {
			if (code[i] == 0x90 || code[i] == 0xCC) { // NOP or INT3
				++j;
			} else {
				j = 0;
			}
			if (j == cave_size) {
				return reinterpret_cast<PVOID>(reinterpret_cast<ULONG_PTR>(code_start) + i - cave_size + 1);
			}
		}
		return nullptr;
	}
#endif //_WIN64
} // namespace sandbox::details

ssdt::ssdt(PDRIVER_OBJECT driver_object)
	: driver_object_(driver_object), kernel_(driver_object_) {
	init();
}

bool ssdt::init() {
	if (ssdt_struct_ != nullptr)
		return true;

	if (!kernel_.init()) {
		SbPrint("sandbox::kernel initialization failed...\r\n");
		return false;
	} else {
		SbPrint("sandbox::kernel initialization succeed...\r\n");
	}
	if (!ntdll_.init()) {
		SbPrint("sandbox::ntdll initialization failed...\r\n");
		return false;
	} else {
		SbPrint("sandbox::ntdll initialization succeed...\r\n");
	}

	return find_ssdt();
}

PVOID ssdt::get_function_address(const char *function_name) const {
	// Check SSDT initialization
	if (!ssdt_struct_) {
		SbPrint("Get function address - SSDT is not found.\r\n");
		return nullptr;
	}
	if (!ssdt_struct_->pServiceTable) {
		SbPrint("Get function address - SSDT Service Table is not found.\r\n");
		return nullptr;
	}

	// get export index
	auto export_index = ntdll_.get_function_ssdt_index(function_name);
	SbPrint("%s's export_index = 0x%X\r\n", function_name, export_index);
	if (export_index == pe::error_value || export_index >= ssdt_struct_->NumberOfServices) {
		SbPrint("Get function address - Invalid export index.\r\n");
		return nullptr;
	}

#ifdef _WIN64
	return reinterpret_cast<PVOID>((ssdt_struct_->pServiceTable[export_index] >> 4) + reinterpret_cast<ULONG_PTR>(ssdt_struct_->pServiceTable));
#else
	return reinterpret_cast<PVOID>(ssdt_struct_->pServiceTable[export_index]);
#endif
}

details::hook ssdt::hook(const char *function_name, void *detour, void **original) {
	if (!function_name || !detour) {
		SbPrint("SSDT Hook - function name or detour is nullptr.\r\n");
		return nullptr;
	}
	// Check SSDT initialization
	if (!ssdt_struct_) {
		SbPrint("SSDT Hook - SSDT is not found.\r\n");
		return nullptr;
	}
	if (!ssdt_struct_->pServiceTable) {
		SbPrint("SSDT Hook - SSDT Service Table is not found.\r\n");
		return nullptr;
	}
	// get export index
	auto export_index = ntdll_.get_function_ssdt_index(function_name);
	SbPrint("%s's export_index = 0x%X\r\n", function_name, export_index);
	if (export_index == pe::error_value || export_index >= ssdt_struct_->NumberOfServices) {
		SbPrint("SSDT Hook - Invalid export index.\r\n");
		return nullptr;
	}

	// save old function
	if (original)
		*original = get_function_address(function_name);

	details::hook hook		   = nullptr;
	auto		  old_function = ssdt_struct_->pServiceTable[export_index];
	auto		  new_function = 0l;

#ifdef _WIN64
	// find code start
	auto code_size	= 0ul;
	auto code_start = pe::get_page_base(reinterpret_cast<UINT8 *>(kernel_.base()), &code_size, reinterpret_cast<UINT8 *>((old_function >> 4) + reinterpret_cast<ULONG_PTR>(ssdt_struct_->pServiceTable)));
	if (!code_start || !code_size) {
		SbPrint("SSDT Hook %s - PeGetPageBase failed...\r\n", function_name);
		return nullptr;
	}
	// Get lowest and highest range of table
	auto lowest_range  = reinterpret_cast<ULONG_PTR>(ssdt_struct_->pServiceTable);
	auto highest_range = lowest_range + 0x0FFFFFFFul;
	// start of the page is out of range (impossible, but whatever)
	if (reinterpret_cast<ULONG_PTR>(code_start) < lowest_range) {
		code_size -= lowest_range - reinterpret_cast<ULONG_PTR>(code_start);
		code_start = reinterpret_cast<PVOID>(lowest_range);
	}
	// Find cave address to hook
	auto cave_address = details::find_cave_address(code_start, code_size, sizeof(details::hook_opcodes));
	if (!cave_address) {
		SbPrint("SSDT Hook %s - FindCaveAddress failed...\r\n", function_name);
		return nullptr;
	}
	SbPrint("SSDT Hook %s - CaveAddress: 0x%p\r\n", function_name, cave_address);

	// hook the cave address
	hook = hooklib::hook(cave_address, reinterpret_cast<void *>(detour));
	if (!hook)
		return nullptr;

	new_function = reinterpret_cast<ULONG_PTR>(cave_address) - reinterpret_cast<ULONG_PTR>(ssdt_struct_->pServiceTable);
	new_function = (new_function << 4) | old_function & 0xF;

	// update hook structure
	hook->ssdt_index = export_index;
	hook->ssdt_old	 = old_function;
	hook->ssdt_new	 = new_function;
	hook->ssdt_addr	 = (old_function >> 4) + reinterpret_cast<ULONG_PTR>(ssdt_struct_->pServiceTable);
#else
	new_function = reinterpret_cast<ULONG_PTR>(detour);

	hook = new details::hook_struct;

	hook->ssdt_index = export_index;
	hook->ssdt_old	 = old_function;
	hook->ssdt_new	 = new_function;
	hook->ssdt_addr	 = old_function;
#endif // _WIN64

	// Copy the memory to SSDT
	ktl::memory::super_memcpy(&ssdt_struct_->pServiceTable[export_index], &new_function, sizeof(new_function));

	return hook;
}

void ssdt::unhook(details::hook hook) {
	if (hook && ssdt_struct_ && ssdt_struct_->pServiceTable) {
		ktl::memory::super_memcpy(&ssdt_struct_->pServiceTable[hook->ssdt_index], &hook->ssdt_old, sizeof(hook->ssdt_old));
#ifdef _WIN64
		hooklib::unhook(hook, true);
#else
		 delete hook;
#endif
	}
}

// Used:
//	https://github.com/moukayz/Notebook/blob/master/mouka/windowsinternal/ssdt-hook.md
//	https://github.com/mrexodia/TitanHide/blob/master/TitanHide/ssdt.cpp
bool ssdt::find_ssdt() {
	if (!ssdt_struct_) {
#ifdef _WIN64
		auto kernel_base = kernel_.base();
		auto kernel_size = kernel_.size();
		if (!kernel_base || !kernel_size) {
			SbPrint("Can't find SSDT - Kernel is uninitialized.\r\n");
			return false;
		}

		// find .text
		PIMAGE_SECTION_HEADER text_section	  = nullptr;
		auto				  nt_headers	  = details::RtlImageNtHeader(reinterpret_cast<PVOID>(kernel_base));
		auto				  current_section = IMAGE_FIRST_SECTION(nt_headers);
		char				  section_name[IMAGE_SIZEOF_SHORT_NAME + 1]{ 0 };
		for (auto i = 0ul; i < nt_headers->FileHeader.NumberOfSections && !text_section; ++i) {
			RtlCopyMemory(section_name, current_section->Name, IMAGE_SIZEOF_SHORT_NAME);
			if (!strncmp(section_name, ".text", 5)) {
				text_section = current_section;
			}
			++current_section;
		}
		if (!text_section) {
			SbPrint("Can't find SSDT - .text section not found.\r\n");
			return false;
		}

		// find KiSystemServiceStart pattern in memory
		static constexpr UCHAR kiss_pattern[] = {
			// KiSystemServiceStart pattern
			0x8B, 0xF8,					 // mov edi,eax
			0xC1, 0xEF, 0x07,			 // shr edi,7
			0x83, 0xE7, 0x20,			 // and edi,20h
			0x25, 0xFF, 0x0F, 0x00, 0x00 // and eax,0fffh
		};
		static constexpr auto kiss_pattern_size = sizeof(kiss_pattern);

		bool found		 = false;
		auto kiss_offset = 0ul;
		for (; kiss_offset < text_section->Misc.VirtualSize - kiss_pattern_size && !found; ++kiss_offset) {
			if (RtlCompareMemory(reinterpret_cast<void *>(kernel_base + text_section->VirtualAddress + kiss_offset), kiss_pattern, kiss_pattern_size) == kiss_pattern_size) {
				found = true;
				--kiss_offset;
			}
		}
		if (!found) {
			SbPrint("Can't find SSDT - KiSystemServiceStart not found.\r\n");
			return false;
		}

		// lea r10, KeServiceDescriptorTable
		auto address		 = kernel_base + text_section->VirtualAddress + kiss_offset + kiss_pattern_size;
		auto relative_offset = 0ul;
		if (*reinterpret_cast<UINT8 *>(address) == 0x4C && *reinterpret_cast<UINT8 *>(address + 1) == 0x8D && *reinterpret_cast<UINT8 *>(address + 2) == 0x15) {
			relative_offset = *reinterpret_cast<ULONG *>(address + 3);
		} else {
			SbPrint("Can't find SSDT - Not lea opcode on address.\r\n");
			return false;
		}

		ssdt_struct_ = reinterpret_cast<details::ssdt_struct *>(address + relative_offset + 7);
#else
		 UNICODE_STRING routine_name;
		 RtlInitUnicodeString(&routine_name, L"KeServiceDescriptorTable");
		 ssdt_struct_ = reinterpret_cast<details::ssdt_struct *>(MmGetSystemRoutineAddress(&routine_name));
#endif
	}

	return true;
}