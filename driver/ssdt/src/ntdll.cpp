#include "ntdll.hpp"
#include <ktl/core.hpp>
#include "pe.hpp"

using namespace sandbox;

ntdll::ntdll() {
	init();
}

ntdll::~ntdll() {
	deinit();
}

bool ntdll::init() {
	if (file_data_)
		return true;

	// Check that irql is not passive level
	if (KeGetCurrentIrql() != PASSIVE_LEVEL) {
		SbPrint("Can't initialize ntdll - CurrentIrql != PASSIVE_LEVEL.\r\n");
		return false;
	}

	// Initialize ntdll attribute
	UNICODE_STRING file_path;
	RtlInitUnicodeString(&file_path, L"\\SystemRoot\\system32\\ntdll.dll");
	OBJECT_ATTRIBUTES object_attributes;
	InitializeObjectAttributes(&object_attributes, &file_path, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, NULL, NULL);

	// Open ntdll
	HANDLE			file_handle = NULL;
	IO_STATUS_BLOCK io_status_block;
	auto			status = ZwCreateFile(&file_handle, GENERIC_READ, &object_attributes, &io_status_block, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, FILE_OPEN, FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	// check that file opened successfully
	if (!NT_SUCCESS(status)) {
		SbPrint("Can't initialize ntdll - ZwCreateFile failed.\r\n");
		return false;
	}

	// Get file size
	FILE_STANDARD_INFORMATION standard_information = { 0 };
	status										   = ZwQueryInformationFile(file_handle, &io_status_block, &standard_information, sizeof(FILE_STANDARD_INFORMATION), FileStandardInformation);
	if (!NT_SUCCESS(status)) {
		SbPrint("Can't initialize ntdll - ZwQueryInformationFile failed.\r\n");
		return false;
	}
	file_size_ = standard_information.EndOfFile.LowPart;
	SbPrint("FileSize of ntdll.dll is 0x%X!\r\n", file_size_);
	file_data_ = new (NonPagedPool) UINT8[file_size_];

	// Read the file
	LARGE_INTEGER byte_offset;
	byte_offset.LowPart	 = 0;
	byte_offset.HighPart = 0;
	status				 = ZwReadFile(file_handle, nullptr, nullptr, nullptr, &io_status_block, file_data_, file_size_, &byte_offset, nullptr);
	if (!NT_SUCCESS(status)) {
		deinit();
		SbPrint("Can't initialize ntdll - ZwReadFile failed.\r\n");
		return false;
	}

	return true;
}

void ntdll::deinit() {
	if (file_data_) {
		file_size_ = 0;
		delete[] file_data_;
		file_data_ = nullptr;
	}
}

int ntdll::get_function_ssdt_index(const char *function_name) const {
	auto export_offset = pe::get_export_offset(file_data_, file_size_, function_name);
	if (export_offset == pe::error_value) {
		SbPrint("Get function SSDT index - Offset exporting failed.\r\n");
		return -1;
	}

	// Find ssdt offset in export data
	int	 ssdt_offset = -1;
	auto export_data = file_data_ + export_offset;
	for (int i = 0; i < 32 && export_offset + i < file_size_ && ssdt_offset == -1; ++i) {
		if (export_data[i] == 0xB8) { // mov eax, X
			ssdt_offset = *reinterpret_cast<int *>(export_data + i + 1);
		}
	}

	return ssdt_offset;
}