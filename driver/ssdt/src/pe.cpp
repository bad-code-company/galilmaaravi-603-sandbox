#include "pe.hpp"
#include <ntimage.h>

using namespace sandbox;

namespace sandbox::details {
	ULONG rva_to_offset(PIMAGE_NT_HEADERS image_nt_headers, ULONG rva_address, ULONG size) {
		auto image_section_header = IMAGE_FIRST_SECTION(image_nt_headers);
		auto section_number		  = image_nt_headers->FileHeader.NumberOfSections;
		for (auto i = 0; i < section_number; i++) {
			if (image_section_header->VirtualAddress <= rva_address) {
				if (image_section_header->VirtualAddress + image_section_header->Misc.VirtualSize > rva_address) {
					rva_address -= image_section_header->VirtualAddress;
					rva_address += image_section_header->PointerToRawData;
					return rva_address < size ? rva_address : pe::error_value;
				}
			}
			++image_section_header;
		}
		return pe::error_value;
	}

	ULONG rva_to_section(PIMAGE_NT_HEADERS image_nt_headers, ULONG rva) {
		auto sections			  = image_nt_headers->FileHeader.NumberOfSections;
		auto image_section_header = IMAGE_FIRST_SECTION(image_nt_headers);
		for (int i = 0; i < sections; ++i) {
			if (image_section_header[i].VirtualAddress <= rva && image_section_header[i].VirtualAddress + image_section_header[i].Misc.VirtualSize > rva)
				return i;
		}
		return pe::error_value;
	}
} // namespace sandbox::details

ULONG_PTR pe::get_export_offset(const UINT8 *file_data, ULONG file_size, const char *export_name) {
	// Verify DOS header
	auto image_dos_header = reinterpret_cast<PIMAGE_DOS_HEADER>(const_cast<UINT8 *>(file_data));
	if (image_dos_header->e_magic != IMAGE_DOS_SIGNATURE) {
		SbPrint("Export offset - DOS header veryfing failed.\r\n");
		return error_value;
	}
	// Verify PE header
	PIMAGE_NT_HEADERS image_nt_headers = reinterpret_cast<PIMAGE_NT_HEADERS>(const_cast<UINT8 *>(file_data) + image_dos_header->e_lfanew);
	if (image_nt_headers->Signature != IMAGE_NT_SIGNATURE) {
		SbPrint("Export offset - PE header veryfing failed.\r\n");
		return error_value;
	}
	// Verify data export
	PIMAGE_DATA_DIRECTORY image_data_directory = nullptr;
	if (image_nt_headers->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR64_MAGIC)
		image_data_directory = reinterpret_cast<PIMAGE_NT_HEADERS64>(image_nt_headers)->OptionalHeader.DataDirectory;
	else
		image_data_directory = reinterpret_cast<PIMAGE_NT_HEADERS32>(image_nt_headers)->OptionalHeader.DataDirectory;
	auto export_dir_rva	   = image_data_directory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
	auto export_dir_size   = image_data_directory[IMAGE_DIRECTORY_ENTRY_EXPORT].Size;
	auto export_dir_offset = details::rva_to_offset(image_nt_headers, export_dir_rva, file_size);
	if (export_dir_offset == error_value) {
		SbPrint("Export offset - Data export veryfing failed.\r\n");
		return error_value;
	}

	// Read export directory
	auto export_dir			  = reinterpret_cast<PIMAGE_EXPORT_DIRECTORY>(const_cast<UINT8 *>(file_data) + export_dir_offset);
	auto names_number		  = export_dir->NumberOfNames;
	auto functions_offset	  = details::rva_to_offset(image_nt_headers, export_dir->AddressOfFunctions, file_size);
	auto name_ordinals_offset = details::rva_to_offset(image_nt_headers, export_dir->AddressOfNameOrdinals, file_size);
	auto names_offset		  = details::rva_to_offset(image_nt_headers, export_dir->AddressOfNames, file_size);
	if (functions_offset == error_value || name_ordinals_offset == error_value || names_offset == error_value) {
		SbPrint("Export offset - Reading export directory failed.\r\n");
		return error_value;
	}
	// Get global address
	auto functions	   = reinterpret_cast<ULONG *>(const_cast<UINT8 *>(file_data) + functions_offset);
	auto name_ordinals = reinterpret_cast<USHORT *>(const_cast<UINT8 *>(file_data) + name_ordinals_offset);
	auto names		   = reinterpret_cast<ULONG *>(const_cast<UINT8 *>(file_data) + names_offset);

	// Find export
	auto export_offset = error_value;
	for (auto i = 0ul; i < names_number && export_offset == error_value; i++) {
		auto current_name_offset = details::rva_to_offset(image_nt_headers, names[i], file_size);
		if (current_name_offset != error_value) {
			const char *current_name		 = reinterpret_cast<const char *>(const_cast<UINT8 *>(file_data) + current_name_offset);
			auto		current_function_rva = functions[name_ordinals[i]];
			// check that export is not forwarded
			if (current_function_rva < export_dir_rva || current_function_rva >= export_dir_rva + export_dir_size) {
				if (!strcmp(current_name, export_name)) { // compare the export name to the requested export
					export_offset = details::rva_to_offset(image_nt_headers, current_function_rva, file_size);
				}
			}
		}
	}
	if (export_offset == error_value) {
		SbPrint("Export offset - Export %s finding failed.\r\n", export_name);
	}

	return export_offset;
}

PVOID pe::get_page_base(UINT8 *header, PULONG size, UINT8 *ptr) {
	if (ptr < header) {
		SbPrint("Get page base - Invalid params.\r\n");
		return nullptr;
	}
	// Verify DOS header
	auto image_dos_header = reinterpret_cast<PIMAGE_DOS_HEADER>(header);
	if (image_dos_header->e_magic != IMAGE_DOS_SIGNATURE) {
		SbPrint("Get page base - Failed verifing DOS header.\r\n");
		return nullptr;
	}
	// Verify PE header
	auto image_nt_header = reinterpret_cast<PIMAGE_NT_HEADERS>(header + image_dos_header->e_lfanew);
	if (image_nt_header->Signature != IMAGE_NT_SIGNATURE) {
		SbPrint("Get page base - Failed verifing PE header.\r\n");
		return nullptr;
	}
	// Convert RVA to section
	auto rva	 = static_cast<ULONG>(ptr - header);
	auto section = details::rva_to_section(image_nt_header, rva);
	if (section == error_value) {
		SbPrint("Get page base - Converting RVA to section.\r\n");
		return nullptr;
	}
	// Get section
	auto image_section_header = IMAGE_FIRST_SECTION(image_nt_header);
	// Set size
	if (size)
		*size = image_section_header[section].SizeOfRawData;
	return reinterpret_cast<PVOID>(header + image_section_header[section].VirtualAddress);
}