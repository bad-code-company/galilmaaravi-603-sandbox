#include "hooklib.hpp"
#include <ktl/memory.hpp>

using namespace sandbox;

details::hook hooklib::hook(PVOID original, PVOID new_function) {
	if (!original || !new_function) {
		SbPrint("hook - original or new function is nullptr.\r\n");
		return nullptr;
	}
	// Allocate memory for the struct
	auto hook = reinterpret_cast<details::hook>(operator new(sizeof(details::hook_struct)));
	// Set hooking address
	hook->addr = reinterpret_cast<ULONG_PTR>(original);
	// Set hooking opcodes
#ifdef _WIN64
	hook->hook_ops.mov = 0xB848;
#else
	hook->hook_ops.mov = 0xB8;
#endif // _WIN64
	hook->hook_ops.addr = reinterpret_cast<ULONG_PTR>(new_function);
	hook->hook_ops.push = 0x50;
	hook->hook_ops.ret	= 0xC3;
	// Copy original data
	ktl::memory::super_memcpy(&hook->orig, original, sizeof(details::hook_opcodes));
	if (!NT_SUCCESS(ktl::memory::super_memcpy(original, &hook->hook_ops, sizeof(details::hook_opcodes)))) {
		delete hook;
		return nullptr;
	}
	return hook;
}

void hooklib::unhook(details::hook hook, bool free) {
	// Check that hook is not nullptr
	if (hook && hook->addr) {
		ktl::memory::super_memcpy(reinterpret_cast<void *>(hook->addr), hook->orig, sizeof(details::hook_opcodes));
		if (free)
			delete hook;
	}
}