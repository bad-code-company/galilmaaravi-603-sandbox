#include "kernel.hpp"

using namespace sandbox;

namespace sandbox::details {
	typedef struct _LDR_DATA_TABLE_ENTRY {
		LIST_ENTRY	   InLoadOrderLinks;
		LIST_ENTRY	   InMemoryOrderLinks;
		LIST_ENTRY	   InInitializationOrderLinks;
		PVOID		   DllBase;
		PVOID		   EntryPoint;
		ULONG		   SizeOfImage;
		UNICODE_STRING FullDllName;
		UNICODE_STRING BaseDllName;
		ULONG		   Flags;
		USHORT		   LoadCount;
		USHORT		   TlsIndex;
		LIST_ENTRY	   HashLinks;
		ULONG		   TimeDateStamp;
	} LDR_DATA_TABLE_ENTRY, *PLDR_DATA_TABLE_ENTRY;

	inline wchar_t locase_w(wchar_t c) {
		if ((c >= 'A') && (c <= 'Z'))
			return c + 0x20;
		else
			return c;
	}

	int _strcmpi_w(const wchar_t *s1, const wchar_t *s2) {
		wchar_t c1;
		wchar_t c2;

		if (s1 == s2)
			return 0;

		if (s1 == 0)
			return -1;

		if (s2 == 0)
			return 1;

		do {
			c1 = locase_w(*s1);
			c2 = locase_w(*s2);
			s1++;
			s2++;
		} while (c1 != 0 && c1 == c2);

		return c1 - c2;
	}
} // namespace sandbox::details

kernel::kernel(PDRIVER_OBJECT driver_object)
	: driver_object_(driver_object) {
	init();
}

// Credits: https://www.unknowncheats.me/forum/general-programming-and-reversing/427419-getkernelbase.html
bool kernel::init() {
	if (base_ != 0 && size_ != 0)
		return true;

	auto entry = reinterpret_cast<details::PLDR_DATA_TABLE_ENTRY>(driver_object_->DriverSection);
	auto first = entry;
	while (reinterpret_cast<details::PLDR_DATA_TABLE_ENTRY>(entry->InLoadOrderLinks.Flink) != first) {
		if (details::_strcmpi_w(entry->BaseDllName.Buffer, L"ntoskrnl.exe") == 0) {
			base_ = reinterpret_cast<ULONG_PTR>(entry->DllBase);
			size_ = entry->SizeOfImage;
		}
		entry = reinterpret_cast<details::PLDR_DATA_TABLE_ENTRY>(entry->InLoadOrderLinks.Flink);
	}

	return base_ != 0 && size_ != 0;
}