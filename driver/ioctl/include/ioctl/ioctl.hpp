#ifndef IOCTL_HPP
#define IOCTL_HPP

#include <ntddk.h>
#include "callback.hpp"

namespace sandbox {
	class ioctl {
	public:
		enum class request_id { start_logging = 10,
								end_logging	  = 20 };
		enum class response_id { start_logging = 11,
								 end_logging   = 21 };

		/**
		 * @brief Initiliazes the ioctl class to work
		 * @param driver_object Driver's object
		 * @param callback Callback of starting/ending of logging
		 */
		ioctl(PDRIVER_OBJECT driver_object, const callback<void(bool, long)> &cb);
		/**
		 * @brief Deinitializes the ioctl
		 */
		~ioctl();

		/**
		 * @brief Initiliazes the ioctl class to work
		 * @return Initialization status
		 */
		bool init();
		/**
		 * @brief Deinitializes the ioctl
		 */
		void deinit();

	private:
		/**
		 * @brief Callback of all IOCTL requests
		 * @param driver_object IOCTL device object
		 * @param irp IRP structure's pointer
		 * @return Status of completing requets
		 */
		static NTSTATUS dispatch_pass_thru(PDEVICE_OBJECT device_object, PIRP irp);
		/**
		 * @brief Callback of read and write IOCTL requests
		 * @param driver_object IOCTL device object
		 * @param irp IRP structure's pointer
		 * @return Status of completing requets
		 */
		static NTSTATUS dispatch_device_control(PDEVICE_OBJECT device_object, PIRP irp);

		// Necessary constructions
	private:
		struct request {
			request_id request_id;
			long	   pid = -1;
		};

		struct response {
			response_id response_id;
		};

		/// Constants
		static inline UNICODE_STRING device_name  = RTL_CONSTANT_STRING(L"\\Device\\Sandbox");
		static inline UNICODE_STRING symlink_name = RTL_CONSTANT_STRING(L"\\??\\SandboxLink");

		/// Self instance
		static ioctl *self;

		/// Fields
		PDRIVER_OBJECT driver_object_ = nullptr;
		PDEVICE_OBJECT device_object_ = nullptr;

		callback<void(bool, long)> cb_;
	};
} // namespace sandbox

#endif // IOCTL_HPP