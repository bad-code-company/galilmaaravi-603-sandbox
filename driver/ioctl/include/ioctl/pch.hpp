#ifndef PCH_HPP
#define PCH_HPP

#include <ntddk.h>

// Print message with sandbox tag
#define SbPrint(_x_, ...) DbgPrint("[SANDBOX]: " _x_, __VA_ARGS__)

#endif // PCH_HPP
