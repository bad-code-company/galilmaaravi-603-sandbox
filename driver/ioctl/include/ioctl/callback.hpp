#ifndef CALLBACK_HPP
#define CALLBACK_HPP

#include <ktl/functional.hpp>
#include <ktl/mutex.hpp>
#include <ktl/lock_guard.hpp>
#include <ktl/utility.hpp>
#include <ktl/pair.hpp>

namespace sandbox {
	template<typename>
	class callback;

	template<typename R, typename... Args>
	class callback<R(Args...)> {
	public:
		/**
		 * @brief Default constructor
		 */
		callback() {
			mtx_.init();
		}

		/*
		 * @brief Sets the function callback
		 * @param callback Function
		 */
		callback(const ktl::function<R(Args...)> &callback)
			: callback() {
			set(callback);
		}

		/*
		 * @brief Sets the function callback as class method
		 * @param obj Class object
		 * @param func Func ptr
		 */
		template<class C>
		callback(C *obj, R (C::*func)(Args...))
			: callback() {
			set(obj, func);
		}

		/*
		 * @brief Sets the function callback as class method
		 * @param method Pair of class obj and func ptr
		 */
		template<class C>
		callback(const ktl::pair<C *, R (C::*)(Args...)> &method)
			: callback() {
			set(method);
		}

		/**
		 * @brief Copies callback from other
		 * @param other Callback to copy from
		 */
		callback(const callback &other)
			: callback() {
			callback_ = other.callback_;
		}

		/**
		 * @brief Moves callback from other
		 * @param other Callback to move from
		 */
		callback(callback &&other)
			: callback() {
			callback_ = ktl::move(other.callback_);
		}

		/*
		 * @brief Sets the function callback
		 * @param callback Function
		 * @return This callback object
		 */
		callback &operator=(const ktl::function<R(Args...)> &callback) {
			set(callback);
			return *this;
		}

		/*
		 * @brief Sets the function callback as class method
		 * @param method Pair of class obj and func ptr
		 * @param This callback object
		 */
		template<class C>
		callback &operator=(const ktl::pair<C *, R (C::*)(Args...)> &method) {
			set(method);
			return *this;
		}

		/*
		 * @brief Sets the function callback
		 * @param callback Function
		 */
		void set(const ktl::function<R(Args...)> &callback) {
			ktl::lock_guard guard(mtx_);
			callback_ = callback;
		}

		/*
		 * @brief Sets the function callback as class method
		 * @param obj Class object
		 * @param func Func ptr
		 */
		template<class C>
		void set(C *obj, R (C::*func)(Args...)) {
			ktl::lock_guard guard(mtx_);
			callback_ = [obj, func](Args &&...args) {
				(obj->*func)(args...);
			};
		}

		/*
		 * @brief Sets the function callback as class method
		 * @param method Pair of class obj and func ptr
		 */
		template<class C>
		void set(const ktl::pair<C *, R (C::*)(Args...)> &method) {
			auto [obj, func] = method;
			set(obj, func);
		}

		/**
		 * @brief Calls the callback
		 * @param args Args of the callback func
		 * @return Return value of the callback
		 */
		R operator()(Args... args) const {
			return callback_(ktl::forward<Args>(args)...);
		}

		/**
		 * @return Does callable exist
		 */
		explicit operator bool() const {
			return callback_;
		}

	private:
		/// Fields
		ktl::function<R(Args...)> callback_;
		ktl::mutex				  mtx_;
	};
} // namespace sandbox

#endif // CALLBACK_HPP