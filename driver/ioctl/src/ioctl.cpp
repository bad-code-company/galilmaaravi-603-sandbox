#include "ioctl.hpp"

#define DEVICE_RECV CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_WRITE_DATA)
#define DEVICE_SEND CTL_CODE(FILE_DEVICE_UNKNOWN, 0x802, METHOD_BUFFERED, FILE_READ_DATA)

using namespace sandbox;

ioctl *ioctl::self = nullptr;

ioctl::ioctl(PDRIVER_OBJECT driver_object, const callback<void(bool, long)> &cb)
	: driver_object_(driver_object), cb_(cb) {
	NT_ASSERT(self == nullptr);
	init();
	// save self instance
	self = this;
}

ioctl::~ioctl() {
	deinit();
	// remove self instance
	self = nullptr;
}

bool ioctl::init() {
	if (device_object_ != nullptr)
		return true;

	// Create IO device
	auto status = IoCreateDevice(driver_object_, 0, &device_name, FILE_DEVICE_UNKNOWN, FILE_DEVICE_SECURE_OPEN, FALSE, &device_object_);
	if (!NT_SUCCESS(status)) {
		SbPrint("Createing IO device failed.\r\n");
		return status;
	}

	// Create IO symlink
	status = IoCreateSymbolicLink(&symlink_name, &device_name);
	if (!NT_SUCCESS(status)) {
		SbPrint("Createing IO symlink failed.\r\n");
		IoDeleteDevice(device_object_);
		device_object_ = nullptr;
		return status;
	}

	// Change IRP function to mine function
	for (auto i = 0u; i < IRP_MJ_MAXIMUM_FUNCTION; ++i) {
		driver_object_->MajorFunction[i] = &dispatch_pass_thru;
	}
	driver_object_->MajorFunction[IRP_MJ_DEVICE_CONTROL] = &dispatch_device_control;

	return NT_SUCCESS(status);
}

void ioctl::deinit() {
	// Clear IO
	if (device_object_ != nullptr) {
		IoDeleteSymbolicLink(&symlink_name);
		IoDeleteDevice(device_object_);
		device_object_ = nullptr;
	}
}

NTSTATUS ioctl::dispatch_pass_thru(PDEVICE_OBJECT device_object, PIRP irp) {
	auto irp_stack_location = IoGetCurrentIrpStackLocation(irp);

	NTSTATUS status = STATUS_SUCCESS;

	switch (irp_stack_location->MajorFunction) {
	case IRP_MJ_CREATE:
		SbPrint("Create request.\r\n");
		break;
	case IRP_MJ_CLOSE:
		self->cb_(false, -1);
		SbPrint("Close request.\r\n");
		break;
	default:
		break;
	}

	irp->IoStatus.Information = 0;
	irp->IoStatus.Status	  = status;
	IoCompleteRequest(irp, IO_NO_INCREMENT);

	return status;
}

NTSTATUS ioctl::dispatch_device_control(PDEVICE_OBJECT device_object, PIRP irp) {
	static request req;

	auto irp_stack_location = IoGetCurrentIrpStackLocation(irp);

	NTSTATUS status = STATUS_SUCCESS;

	// get buffer
	auto buffer				= irp->AssociatedIrp.SystemBuffer;
	auto information_length = 0ul;

	switch (irp_stack_location->Parameters.DeviceIoControl.IoControlCode) {
	case DEVICE_RECV:
		if (buffer != nullptr) {
			// Set request data
			req.request_id = static_cast<request *>(buffer)->request_id;
			req.pid		   = static_cast<request *>(buffer)->pid;
			// Process request
			self->cb_(req.request_id == request_id::start_logging, req.pid);
			information_length = sizeof(request);
		} else {
			SbPrint("Error during recv function.\r\n");
			status = STATUS_UNSUCCESSFUL;
		}
		break;
	case DEVICE_SEND:
		response resp;
		if (req.request_id == request_id::start_logging)
			resp.response_id = response_id::start_logging;
		else
			resp.response_id = response_id::end_logging;
		memcpy(buffer, &resp, sizeof(response));
		information_length = sizeof(response);
		break;
	default:
		status = STATUS_INVALID_PARAMETER;
	}

	// Complete request
	irp->IoStatus.Status	  = status;
	irp->IoStatus.Information = information_length;
	IoCompleteRequest(irp, IO_NO_INCREMENT);

	return status;
}
