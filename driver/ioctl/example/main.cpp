#include <ntddk.h>
#include <ktl/core.hpp>
#include <ktl/unique_ptr.hpp>
#include <ioctl/ioctl.hpp>

ktl::unique_ptr<sandbox::ioctl> ioctl;

void start_logging_callback(bool start, long pid) {
	SbPrint("START LOGGING: %s [PID: %d]\r\n", start ? "TRUE" : "FALSE", pid);
}

DRIVER_UNLOAD DriverUnload;

void DriverUnload(PDRIVER_OBJECT) {
	ktl::core::deinit();
	SbPrint("Driver unloaded.\r\n");
}

extern "C" DRIVER_INITIALIZE DriverEntry;

extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT driver_object, PUNICODE_STRING) {
	driver_object->DriverUnload = DriverUnload;
	ktl::core::init();

	ioctl = ktl::make_unique<sandbox::ioctl>(driver_object, start_logging_callback);

	SbPrint("Driver loaded.\n");
	return STATUS_SUCCESS;
}